f = open('nist.dat', 'r')
lines = f.read().split('\n')

size = int(len(lines)/8) 

iso = [[] for i in range(119)]
name = ['' for i in range(119)]
for i in range(size):
	section = lines[i*8:i*8+7]
	Z = int(section[0].split(' = ')[1])
	M = section[2].split(' = ')[1]
	f = section[4].split(' = ')[1].split('(')[0]
	if f == '':
		f = 0
	iso[Z].append([M, f])
	if name[Z] == '':
		name[Z] = section[1].split(' = ')[1]


print('#Atomic Weights and Isotopic Compositions for All Elements\n#Description of Quantities and Notes')
print('#S\tZ\tM\t(m, f)')
for Z in range(1, 119):
	# element name
	print(name[Z], end='\t')
	# atomic number
	print(Z, end='\t')
	# mean mass
	M = 0
	for m in iso[Z]:
		M += float(m[0])*float(m[1])
	print(M, end='\t')
	# isotopes mass and abundancies
	for m in iso[Z]:
		if m[1] != 0:
			print(m[0], m[1], end=' ')
	print()

print('#NIST | Physical Measurement Laboratory | Physical Reference Data | Atomic Weights and Isotopic Compositions Main Page')
