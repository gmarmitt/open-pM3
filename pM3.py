# Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import configparser
import sys
import os

#local libs
if __name__ == 'pm3.pM3':
	from pm3.plib.structs 	import *
	from pm3.plib.read		import *
	from pm3.plib.calc 		import *
	from pm3.plib.run 		import *
	from pm3.plib.output	import *
else:
	from plib.structs 	import *
	from plib.read		import *
	from plib.calc 		import *
	from plib.run 		import *
	from plib.output	import *

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  CLASSES  - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

class term_input:		#input parameters
	prmt_FileName	= None		#parameters file name
	prmt_FileDir	= None		#directory of .cpm file
	outp_FileName	= None		#output file name

class paths:		#filesystem paths
	clib	= []		#c libraries
	binary	= ''		#binary folder
	atom	= ''		#atom_data file

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - PATHS - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
#get the correct c++ library path for current OS

def PATHS():
	paths.binary = os.path.dirname(os.path.realpath(__file__)) + '/'
	paths.clib = []
	if sys.maxsize == 2147483647:
		if os.name == 'posix':
			paths.clib.append(paths.binary + 'clib/pM3_clib-lin32.so')
			paths.clib.append(paths.binary + 'clib/SRIM_clib-lin32.so')
			paths.clib.append(paths.binary + 'clib/cross_clib-lin32.so')
			paths.clib.append(paths.binary + 'clib/CASP_clib-lin32.so')
			paths.clib.append(paths.binary + 'clib/dielectric_clib-lin32.so')
		if os.name == 'nt':
			paths.clib.append(paths.binary + 'clib/pM3_clib-win32.dll')
			paths.clib.append(paths.binary + 'clib/SRIM_clib-win32.dll')
			paths.clib.append(paths.binary + 'clib/cross_clib-win32.dll')
			paths.clib.append(paths.binary + 'clib/CASP_clib-win32.dll')
			paths.clib.append(paths.binary + 'clib/dielectric_clib-win32.dll')
	if sys.maxsize == 9223372036854775807:
		if os.name == 'posix':
			paths.clib.append(paths.binary + 'clib/pM3_clib-lin64.so')
			paths.clib.append(paths.binary + 'clib/SRIM_clib-lin64.so')
			paths.clib.append(paths.binary + 'clib/cross_clib-lin64.so')
			paths.clib.append(paths.binary + 'clib/CASP_clib-lin64.so')
			paths.clib.append(paths.binary + 'clib/dielectric_clib-lin64.so')
		if os.name == 'nt':
			paths.clib.append(paths.binary + 'clib/pM3_clib-win64.dll')
			paths.clib.append(paths.binary + 'clib/SRIM_clib-win64.dll')
			paths.clib.append(paths.binary + 'clib/cross_clib-win64.dll')
			paths.clib.append(paths.binary + 'clib/CASP_clib-win64.dll')
			paths.clib.append(paths.binary + 'clib/dielectric_clib-win64.dll')

	#get the path to the atom data file
	paths.isotopes = paths.binary + 'data/isotopes.dat'
	paths.cache = paths.binary + 'cache/'

	return paths

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  INPUT LINE - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def INPUT_VERIFICATION():
	if len(sys.argv) > 1:
		term_input.prmt_FileName = sys.argv[1]
		term_input.prmt_FileDir = os.path.dirname(term_input.prmt_FileName)
		if term_input.prmt_FileDir != '':
			term_input.prmt_FileDir += '/'
	else:
		term_input.prmt_FileName = "example.cpm"
		term_input.prmt_FileDir = os.path.dirname(term_input.prmt_FileName)
		print('To run simulations, use the following syntax:\npython pM3.py <configuration file> <output file>\n')
	if len(sys.argv) > 2:
		term_input.outp_FileName = sys.argv[2]
	else:
		term_input.outp_FileName = "output.dat"

	return term_input

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - MAIN  - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def MAIN(term_input, config, is_progbar_active, is_plot_active):

	paths = PATHS()

    #open the parameter file
	print('Opening CPM...')
	[prmt_c, matrix_c, config] = OPEN_PARAMETERS_FILE(term_input, paths.isotopes, config)
	logfilename=os.path.splitext(term_input.outp_FileName)[0]+'.log'
	with open(logfilename, 'w') as f:
		    config.write(f)                                  #this is the log file as MV likes it contains all input parameters, incl layer thicknesses etc, same filename  as output file, diff. extension

	#run matgen functions
	print('Generating matrix...')
	MATGEN(prmt_c, matrix_c)

	# For connected trajectory algorithm, sample must be a simgle layer defined as a matrix,
	# the following function creates such matrix based on the input layers.
	if prmt_c.simu.algorithm == 1:
		config = REFACTOR_PARAMETERS(prmt_c, matrix_c, config)
		[prmt_c, matrix_c, config] = OPEN_PARAMETERS_FILE(term_input, paths.isotopes, config) #this also creates the log file as Gabriel made it, after refactoring input parameters
		MATGEN(prmt_c, matrix_c)

	#open the matrix files
	GENERATE_MATRIX_C(paths, prmt_c, matrix_c)

	#manage parameters changes
	PREPARATION(prmt_c, matrix_c)

	#open the calc tables
	tables_c = [None]
	tables_c[0] = OPEN_TABLES(prmt_c)

	print('\nUsing '+str(prmt_c.nEl)+' elements and '+str(prmt_c.nCp)+' compounds')

	#read vegas cristal visibility files
	#READ_VEGAS_INPUT(prmt_c, tables_c[0])

	#kinematic factor and cross sections
	print('Kinematic factors...')
	K_CALCULATION(prmt_c, tables_c[0])
	print('Cross sections...')
	CS_CALCULATION_CM(paths, prmt_c, tables_c[0])

	#stopping power calculation
	print('Stopping power...')
	STOPPING_POWER_CALCULATION(paths, prmt_c, tables_c[0])
	#straggling calculation
	print('Straggling...')
	STRAGG_CALCULATION(prmt_c, tables_c[0])

	print('Charge fraction...')
	CHARGE_FRACTION_CALCULATION(paths, prmt_c, tables_c[0])

	print('Dielectric function...')
	DIELECTRIC_FUNCTION(paths, prmt_c, tables_c[0])

	#output a logfile with many calculated values
	LOGFILE(prmt_c, tables_c[0])

	#run the simulation
	print('\nRunning simulation:')
	if prmt_c.simu.algorithm == 1:
		histo_c = SIMU_TRAJ(paths, prmt_c, matrix_c, tables_c[0], None, None, is_progbar_active, is_plot_active)
	else:
		histo_c = SIMULA(paths, prmt_c, matrix_c, tables_c[0], None, None, is_progbar_active, is_plot_active)

	#write the output
	print('\nSaving output...')
	OUTPUT(term_input, prmt_c, histo_c)

if __name__ == '__main__':
	print('|==================== pM3 -- powerMEIS v3.0 -- C/python ====================|')
	print('By G.G. Marmitt\n')

	term_input 	= INPUT_VERIFICATION()

	#open a configparser instance and set an option to keep high-low case
	config = configparser.ConfigParser()
	config.optionxform=str
	#read the .cpm file
	config.read(term_input.prmt_FileName)

	MAIN(term_input, config, True, False)
