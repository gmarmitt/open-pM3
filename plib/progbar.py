# Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import time
import math

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  PROGRESSBAR  - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def print_sec_to_time(secs):
	hrs = int(secs / 3600)
	secs -= 3600*hrs

	mins = int(secs / 60)
	secs -= 60*mins

	if hrs != 0:
		print('{0:2.0f}h '.format(hrs) + '{0:2.0f}m '.format(mins) +  '{0:2.0f}s'.format(secs), end='         ')
	else:
		if(mins != 0):
			print('{0:2.0f}m '.format(mins) +  '{0:2.0f}s'.format(secs), end='         ')
		else:
			print('{0:2.0f}s'.format(secs), end='         ')

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

class progress_bar:
	onoff	 	= True
	delay 		= 0.2
	ticks_total	= 100
	ticks_bar	= 50

	def RUN(count_total, current_count):
		start_time = time.time()
		onoff = True
		count = 0

		if count_total == 0:
			count_total = 1

		while progress_bar.onoff and count < count_total:
			#wait a bit
			time.sleep(progress_bar.delay)

			#get the total count, summing all threads counts
			count = 0
			for iThread in range(0, len(current_count)):
				count += current_count[iThread]

			if count == 0:
				count = 1

			#get the time
			current_time = time.time()
			secs = int((current_time-start_time)/count*(count_total-count))

			#calculates the ticks
			step = int(progress_bar.ticks_total*count/count_total)
			tick_current = int(100*step/progress_bar.ticks_total)

			#prints the bar
			print(
			'\r[' +
			'#' * int(step*progress_bar.ticks_bar/progress_bar.ticks_total) +
			'-' * (progress_bar.ticks_bar - int(step*progress_bar.ticks_bar/progress_bar.ticks_total)) +
			']' +
			' {0:2.0f}%'.format(tick_current), end=''
			)
			if count != 1:
				print('- ETA: ', end='')
				print_sec_to_time(secs)

		stop_time = time.time()
		print('\nDone in {0:0.3f} sec.'.format(stop_time-start_time))

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

'''
def read_exp_file(prmt_c, window_min, window_max):
	energy_array = []
	counts_array = []

	exp_file = open (prmt_c.outp.exp_file.decode(),'r')
	exp_lines = exp_file.readlines ()
	for row in exp_lines:
		# split lines
		thisrow = row.split ()
		# get energy (first column)
		energy = float (thisrow [0])
		counts = float (thisrow [1]) * prmt_c.outp.exp_norm
		if (energy >= window_min and energy <= window_max):
			# store energy
			energy_array.append(energy)
			# store counts
			counts_array.append(counts)
	return [energy_array, counts_array]

class liveplot:
	onoff	 	= True
	delay 		= 0.1
	graph_type 	= 1
	checkbox	= []
	exp_x		= []
	exp_y		= []

	def __init__(prmt_c):
		import matplotlib.pyplot as plt

		liveplot.fig = plt.figure()
		if liveplot.graph_type == 1:
			from matplotlib.widgets import Slider, CheckButtons
			liveplot.axis = plt.subplot(111)
			plt.subplots_adjust(left=0.1, right=0.95, top=0.95, bottom=0.2)
			x = [(iEn*prmt_c.outp.dEnergy+prmt_c.outp.Emin)/1e3 for iEn in range(prmt_c.outp.nEn)]
			y = [0 for iEn in range(prmt_c.outp.nEn)]
			liveplot.data, = liveplot.axis.plot(x, y, '.')

			if prmt_c.outp.exp_file.decode() != 'None':
				[liveplot.exp_x,liveplot.exp_y] = read_exp_file(prmt_c, 0, 200)
				liveplot.data_exp, = liveplot.axis.plot(liveplot.exp_x, liveplot.exp_y, '.')

			ax_anglesum 	= plt.axes([0.2, 0.03, 0.65, 0.03], axisbg='w')
			ax_anglemean  	= plt.axes([0.2, 0.08, 0.65, 0.03], axisbg='w')
			ax_expmult  	= plt.axes([0.2, 0.13, 0.65, 0.03], axisbg='w')
			if prmt_c.outp.nAn == 1:
				anglemean_min = (prmt_c.dete.Y0-prmt_c.outp.dAngle)*180/math.pi
				anglemean_max = (prmt_c.dete.Y0+prmt_c.outp.dAngle)*180/math.pi
				anglemean = prmt_c.dete.Y0*180/math.pi
				anglesum_min = 0
				anglesum_max = 2*prmt_c.outp.dAngle*180/math.pi
				anglesum = prmt_c.outp.dAngle*180/math.pi
			else:
				anglemean_min = (prmt_c.dete.theta_min)*180/math.pi
				anglemean_max = (prmt_c.dete.theta_max)*180/math.pi
				anglemean = (prmt_c.dete.theta_max+prmt_c.dete.theta_min)/2*180/math.pi
				anglesum_min = prmt_c.outp.dAngle*180/math.pi
				anglesum_max = prmt_c.outp.nAn*prmt_c.outp.dAngle*180/math.pi
				anglesum = prmt_c.outp.dAngle*180/math.pi

			liveplot.sl_anglemean = Slider(ax_anglemean, 'Scatt. Angle', anglemean_min, anglemean_max, valinit=anglemean)
			liveplot.sl_anglesum = Slider(ax_anglesum, 'Angle Sum', anglesum_min, anglesum_max, valinit=anglesum)
			liveplot.sl_expmult = Slider(ax_expmult, 'Exp. mult.', 1, 1e6, valinit=1)

			def func_on_click_elem(label):
				for iEl in range(1, prmt_c.nEl+1):
					if label == prmt_c.elem[iEl].name.decode():
						if liveplot.checkbox[iEl] == True:
							liveplot.checkbox[iEl] = False
							break
						if liveplot.checkbox[iEl] == False:
							liveplot.checkbox[iEl] = True
							break

			def func_on_click_comp(label):
				for iCp in range(1, prmt_c.nCp+1):
					if label == prmt_c.comp[iCp].name.decode():
						if liveplot.checkbox[iCp] == True:
							liveplot.checkbox[iCp] = False
							break
						if liveplot.checkbox[iCp] == False:
							liveplot.checkbox[iCp] = True
							break

			if prmt_c.outp.histo_type == 1:
				selector = plt.axes([0.11, 0.72, 0.2, 0.2])
				names = tuple()
				onoff = tuple()
				for iEl in range(1, prmt_c.nEl+1):
					names += (prmt_c.elem[iEl].name.decode(),)
					onoff += (True,)
				checkbutton = CheckButtons(selector, names, onoff)
				checkbutton.on_clicked(func_on_click_elem)
				liveplot.checkbox = [True] * (prmt_c.nEl+1)
			if prmt_c.outp.histo_type == 2:
				selector = plt.axes([0.11, 0.72, 0.2, 0.2])
				names = tuple()
				onoff = tuple()
				for iCp in range(1, prmt_c.nCp+1):
					names += (prmt_c.comp[iCp].name.decode(),)
					onoff += (True,)
				checkbutton = CheckButtons(selector, names, onoff)
				checkbutton.on_clicked(func_on_click_comp)
				liveplot.checkbox = [True] * (prmt_c.nCp+1)


		if liveplot.graph_type == 2:
			z = [[0 for iAn in range(prmt_c.outp.nAn)] for iEn in range(prmt_c.outp.nEn)]
			liveplot.img = plt.imshow(z, extent=[prmt_c.dete.theta_min, prmt_c.dete.theta_max, prmt_c.outp.Emin/1e3, prmt_c.outp.Emax/1e3], aspect='auto')
			liveplot.fig = plt.gcf()
			plt.tight_layout()
			#plt.clim()

	def RUN(prmt_c, current_histo, current_count):
		import matplotlib.pyplot as plt
		onoff = True
		count = 0

		while liveplot.onoff and plt.get_fignums():
			#wait a bit
			time.sleep(progress_bar.delay)

			#get the total count, summing all threads counts
			count = 0
			for iThread in range(0, len(current_count)):
				count += current_count[iThread]

			if liveplot.graph_type == 1:
				xlim_tuple = liveplot.axis.get_xlim()
				xlim = [max(prmt_c.outp.Emin/1e3, xlim_tuple[0]),
						min(prmt_c.outp.Emax/1e3, xlim_tuple[1])]
				xlim_ind = [int((xlim[0]*1e3 - prmt_c.outp.Emin)/prmt_c.outp.dEnergy)-1,
							int((xlim[1]*1e3 - prmt_c.outp.Emin)/prmt_c.outp.dEnergy)+1]
				if prmt_c.outp.nAn == 1:
					ylim_ind = [0, 1]
				else:
					ylim = [(liveplot.sl_anglemean.val-liveplot.sl_anglesum.val/2),
							(liveplot.sl_anglemean.val+liveplot.sl_anglesum.val/2)]
					ylim_ind = [int((ylim[0]*math.pi/180-prmt_c.dete.theta_min)/prmt_c.outp.dAngle)-1,
								int((ylim[1]*math.pi/180-prmt_c.dete.theta_min)/prmt_c.outp.dAngle)+1]

				histo = [0 for iEn in range(prmt_c.outp.nEn)]
				count_lim = [1e20, 0]
				for iEn in range(max(0, xlim_ind[0]), min(xlim_ind[1], prmt_c.outp.nEn)):
					histo_sum = 0
					if prmt_c.outp.histo_type == 0:
						for iThread in range(len(current_count)):
							for iAn in range(max(0, ylim_ind[0]), min(ylim_ind[1], prmt_c.outp.nAn)):
								histo_sum += current_histo[iThread].simple_2D[iEn][iAn]
					if prmt_c.outp.histo_type == 1:
						for iThread in range(len(current_count)):
							for iLa in range(1, prmt_c.nLa+1):
								for iMt in range(0, prmt_c.nMt+1):
									for iCp in range(1, prmt_c.nCp+1):
										for iEl in range(1, prmt_c.nEl+1):
											if liveplot.checkbox[iEl]:
												for iAn in range(max(0, ylim_ind[0]), min(ylim_ind[1], prmt_c.outp.nAn)):
													histo_sum += current_histo[iThread].full_2D[iLa][iMt][iCp][iEl][iEn][iAn]
					if prmt_c.outp.histo_type == 2:
						for iThread in range(len(current_count)):
							for iLa in range(1, prmt_c.nLa+1):
								for iMt in range(0, prmt_c.nMt+1):
									for iCp in range(1, prmt_c.nCp+1):
										if liveplot.checkbox[iCp]:
											for iEl in range(1, prmt_c.nEl+1):
												for iAn in range(max(0, ylim_ind[0]), min(ylim_ind[1], prmt_c.outp.nAn)):
													histo_sum += current_histo[iThread].full_2D[iLa][iMt][iCp][iEl][iEn][iAn]
					histo_sum *= liveplot.sl_expmult.val/count
					histo[iEn] += histo_sum
					if histo_sum < count_lim[0]:
						count_lim[0] = histo_sum
					if histo_sum > count_lim[1]:
						count_lim[1] = histo_sum

				xlim_tuple = liveplot.axis.get_xlim()
				xlim = [max(prmt_c.outp.Emin/1e3, xlim_tuple[0]),
						min(prmt_c.outp.Emax/1e3, xlim_tuple[1])]
				liveplot.axis.set_xlim(xlim[0], xlim[1])

				ywindow_size = count_lim[1] - count_lim[0]
				if count_lim[0] - ywindow_size*0.05 != count_lim[1] + ywindow_size*0.05:
					liveplot.axis.set_ylim(count_lim[0] - ywindow_size*0.05, count_lim[1] + ywindow_size*0.05)

				liveplot.data.set_ydata(histo)

			if liveplot.graph_type == 2:
				histo = [[0 for iAn in range(prmt_c.outp.nAn)] for iEn in range(prmt_c.outp.nEn)]
				zlim = [0, 0]
				for iThread in range(0, len(current_count)):
					for iEn in range(0, prmt_c.outp.nEn):
						jEn = prmt_c.outp.nEn-1 - iEn
						for iAn in range(0, prmt_c.outp.nAn):
							histo[jEn][iAn] += current_histo[iThread].simple_2D[iEn][iAn]
							if histo[jEn][iAn] > zlim[1]:
								zlim[1] = histo[jEn][iAn]

				liveplot.img = plt.imshow(histo, extent=[prmt_c.dete.theta_min*180/math.pi, prmt_c.dete.theta_max*180/math.pi, prmt_c.outp.Emin/1e3, prmt_c.outp.Emax/1e3], aspect='auto')
				#liveplot.img.set_data(histo)
				plt.clim()

			liveplot.fig.canvas.draw()
'''
