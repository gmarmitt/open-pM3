# Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from tkinter import *
import tkinter as tk
import sys

#Creates and Initiates class 'App'
class App(Tk):
	def __init__(self):
		Tk.__init__(self)

		self.title("Periodic Table of the Elements")

		self.topLabel = Label(self, text = "Click the element you would like to choose", font=20)
		self.topLabel.grid(row=0,column=0,columnspan=18)

		#Names of buttons in column 1
		column1 = [
		('H', 'Hydrogen', '1', '1.01', 'Gas', 'Alkali Metals'),
		('Li', 'Lithium', '3', '6.94', 'Solid', 'Alkali Metals'),
		('Na', 'Sodium', '11', '22.99', 'Solid', 'Alkali Metals'),
		('K', 'Potassium', '19', '39.10', 'Solid', 'Alkali Metals'),
		('Rb', 'Rubidium', '37', '85.47', 'Solid', 'Alkali Metals'),
		('Cs', 'Cesium', '55', '132.91', 'Solid', 'Alkali Metals'),
		('Fr', 'Francium', '87', '223.00', 'Solid', 'Alkali Metals')]
		#create all buttons with a loop
		r = 1
		c = 0
		for b in column1:
			tk.Button(self,text=b[0],width=1,height=1, bg="grey",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column2 = [
		('Be', 'Beryllium', '4', '9.01', 'Solid', 'Alkaline Earth Metals'),
		('Mg', 'Magnesium', '12', '24.31', 'Solid', 'Alkaline Earth Metals'),
		('Ca', 'Calcium', '20', '40.08', 'Solid', 'Alkaline Earth Metals'),
		('Sr', 'Strontium', '38', '87.62', 'Solid', 'Alkaline Earth Metals'),
		('Ba', 'Barium', '56', '137.33', 'Solid', 'Alkaline Earth Metals'),
		('Ra', 'Radium', '88', '226.03', 'Solid', 'Alkaline Earth Metals')]
		r = 2
		c = 1
		for b in column2:
			tk.Button(self,text=b[0],width=1,height=1, bg="light green",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column3 = [
		('Sc', 'Scandium', '21', '44.96', 'Solid', 'Transitional Metals'),
		('Y', 'Yttrium', '39', '88.91', 'Solid', 'Transitional Metals'),
		('Lan', 'Lanthanum', '57', '138.91', 'Solid', 'Transitional Metals'),
		('Act', 'Actinium', '89', '227.03', 'Solid', 'Transitional Metals')]
		r = 4
		c = 2
		for b in column3:
			tk.Button(self,text=b[0],width=1,height=1, bg="light goldenrod",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column4 = [
		('Ti', 'Titanium', '22', '47.90', 'Solid', 'Transitional Metals'),
		('Zr', 'Zirconium', '40', '91.22', 'Solid', 'Transitional Metals'),
		('Hf', 'Hanium', '72', '178.49', 'Solid', 'Transitional Metals'),
		('Rf', 'Rutherfordium', '104', '261.00', 'Synthetic', 'Transitional Metals')]
		r = 4
		c = 3
		for b in column4:
			tk.Button(self,text=b[0],width=1,height=1, bg="light goldenrod",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 10:
				r = 1
				c += 1

		column5 = [
		('V', 'Vanadium', '23', '50.94', 'Solid', 'Transitional Metals'),
		('Nb', 'Niobium', '41', '92.91', 'Solid', 'Transitional Metals'),
		('Ta', 'Tantalum', '73', '180.95', 'Solid', 'Transitional Metals'),
		('Ha', 'Hahnium', '105', '262.00', 'Synthetic', 'Transitional Metals')]
		r = 4
		c = 4
		for b in column5:
			tk.Button(self,text=b[0],width=1,height=1, bg="light goldenrod",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 10:
				r = 1
				c += 1

		column6 = [
		('Cr', 'Chromium', '24', '51.99', 'Solid', 'Transitional Metals'),
		('Mo', 'Molybdenum', '42', '95.94', 'Solid', 'Transitional Metals'),
		('W', 'Tungsten', '74', '183.85', 'Solid', 'Transitional Metals'),
		('Sg', 'Seaborgium', '106', '266.00', 'Synthetic', 'Transitional Metals')]
		r = 4
		c = 5
		for b in column6:
			tk.Button(self,text=b[0],width=1,height=1, bg="light goldenrod",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column7 = [
		('Mn', 'Manganese', '25', '178.49', 'Solid', 'Transitional Metals'),
		('Tc', 'Technetium', '43', '178.49', 'Synthetic', 'Transitional Metals'),
		('Re', 'Rhenium', '75', '178.49', 'Solid', 'Transitional Metals'),
		('Bh', 'Bohrium', '107', '262.00', 'Synthetic', 'Transitional Metals')]
		r = 4
		c = 6
		for b in column7:
			tk.Button(self,text=b[0],width=1,height=1, bg="light goldenrod",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column8 = [
		('Fe', 'Iron', '26', '55.85', 'Solid', 'Transitional Metals'),
		('Ru', 'Ruthenium', '44', '101.07', 'Solid', 'Transitional Metals'),
		('Os', 'Osmium', '76', '190.20', 'Solid', 'Transitional Metals'),
		('Hs', 'Hassium', '108', '265.00', 'Synthetic', 'Transitional Metals')]
		r = 4
		c = 7
		for b in column8:
			tk.Button(self,text=b[0],width=1,height=1, bg="light goldenrod",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column9 = [
		('Co', 'Cobalt', '27', '58.93', 'Solid', 'Transitional Metals'),
		('Rh', 'Rhodium', '45', '102.91', 'Solid', 'Transitional Metals'),
		('Ir', 'Iridium', '77', '192.22', 'Solid', 'Transitional Metals'),
		('Mt', 'Meitnerium', '109', '266.00', 'Synthetic', 'Transitional Metals')]
		r = 4
		c = 8
		for b in column9:
			tk.Button(self,text=b[0],width=1,height=1, bg="light goldenrod",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column10 = [
		('Ni', 'Nickle', '28', '58.70', 'Solid', 'Transitional Metals'),
		('Pd', 'Palladium', '46', '106.40', 'Solid', 'Transitional Metals'),
		('Pt', 'Platinum', '78', '195.09', 'Solid', 'Transitional Metals')]
		r = 4
		c = 9
		for b in column10:
			tk.Button(self,text=b[0],width=1,height=1, bg="light goldenrod",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column11 = [
		('Cu', 'Copper', '29', '63.55', 'Solid', 'Transitional Metals'),
		('Ag', 'Silver', '47', '107.97', 'Solid', 'Transitional Metals'),
		('Au', 'Gold', '79', '196.97', 'Solid', 'Transitional Metals')]
		r = 4
		c = 10
		for b in column11:
			tk.Button(self,text=b[0],width=1,height=1, bg="light goldenrod",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column12 = [
		('Zn', 'Zinc', '30', '65.37', 'Solid', 'Transitional Metals'),
		('Cd', 'Cadmium', '48', '112.41', 'Solid', 'Transitional Metals'),
		('Hg', 'Mercury', '80', '200.59', 'Liquid', 'Transitional Metals')]
		r = 4
		c = 11
		for b in column12:
			tk.Button(self,text=b[0],width=1,height=1, bg="light goldenrod",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column13_1 = [
		('B', 'Boron', '5', '10.81', 'Solid', 'Nonmetals')]
		r = 2
		c = 12
		for b in column13_1:
			tk.Button(self,text=b[0],width=1,height=1, bg="Light Blue",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column13_2 = [
		('Al', 'Aluminum', '13', '26.98', 'Solid', 'Other Metals'),
		('Ga', 'Gallium', '31', '69.72', 'Solid', 'Other Metals'),
		('In', 'Indium', '49', '69.72', 'Solid', 'Other Metals'),
		('Ti', 'Thallium', '81', '204.37', 'Solid', 'Other Metals')]
		r = 3
		c = 12
		for b in column13_2:
			tk.Button(self,text=b[0],width=1,height=1, bg="Light Pink",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column14_1 = [
		('C', 'Carbon', '6', '12.01', 'Solid', 'Nonmetals'),
		('Si', 'Silicon', '14', '28.09', 'Solid', 'Nonmetals')]
		r = 2
		c = 13
		for b in column14_1:
			tk.Button(self,text=b[0],width=1,height=1, bg="Light Blue",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column14_2 = [
		('Ge', 'Germanium', '32', '72.59', 'Solid', 'Other Metals'),
		('Sn', 'Tin', '50', '118.69', 'Solid', 'Other Metals'),
		('Pb', 'Lead', '82', '207.20', 'Solid', 'Other Metals')]
		r = 4
		c = 13
		for b in column14_2:
			tk.Button(self,text=b[0],width=1,height=1, bg="Light Pink",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column15_1 = [
		('N', 'Nitrogen', '7', '14.01', 'Gas', 'Nonmetals'),
		('P', 'Phosphorus', '15', '30.97', 'Solid', 'Nonmetals'),
		('As', 'Arsenic', '33', '74.92', 'Solid', 'Nonmetals')]
		r = 2
		c = 14
		for b in column15_1:
			tk.Button(self,text=b[0],width=1,height=1, bg="Light Blue",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column15_2 = [
		('Sb', 'Antimony', '51', '121.75', 'Solid', 'Other Metals'),
		('Bi', 'Bismuth', '83', '208.98', 'Solid', 'Other Metals')]
		r = 5
		c = 14
		for b in column15_2:
			tk.Button(self,text=b[0],width=1,height=1, bg="Light Pink",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column16_1 = [
		('O', 'Oxygen', '8', '15.99', 'Gas', 'Nonmetals'),
		('S', 'Sulfur', '16', '32.06', 'Solid', 'Nonmetals'),
		('Se', 'Selenium', '34', '78.96', 'Solid', 'Nonmetals'),
		('Te', 'Tellurium', '52', '127.60', 'Solid', 'Nonmetals')]
		r = 2
		c = 15
		for b in column16_1:
			tk.Button(self,text=b[0],width=1,height=1, bg="Light Blue",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column16_2 = [
		('Po', 'Polonium', '84', '209.00', 'Solid', 'Other Metals')]
		r = 6
		c = 15
		for b in column16_2:
			tk.Button(self,text=b[0],width=1,height=1, bg="Light Pink",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column17 = [
		('F', 'Fluorine', '9', '18.99', 'Gas', 'Nonmetals'),
		('Cl', 'Chlorine', '17', '35.45', 'Gas', 'Nonmetals'),
		('Br', 'Bromine', '35', '79.90', 'Liquid', 'Nonmetals'),
		('I', 'Iodine', '53', '126.90', 'Solid', 'Nonmetals'),
		('At', 'Astatine', '85', '210.00', 'Solid', 'Nonmetals')]
		r = 2
		c = 16
		for b in column17:
			tk.Button(self,text=b[0],width=1,height=1, bg="Light Blue",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		column18 = [
		('He', 'Helium', '2', '4.00', 'Gas', 'Nobel Gases'),
		('Ne', 'Neon', '10', '20.18', 'Gas', 'Nobel Gases'),
		('Ar', 'Argon', '18', '39.95', 'Gas', 'Nobel Gases'),
		('Kr', 'Krypton', '36', '83.80', 'Gas', 'Nobel Gases'),
		('Xe', 'Xenon', '54', '131.30', 'Gas', 'Nobel Gases'),
		('Rn', 'Radon', '86', '222.00', 'Gas', 'Nobel Gases')]
		r = 1
		c = 17
		for b in column18:
			tk.Button(self,text=b[0],width=1,height=1, bg="indian red",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		self.fillerLine = Label(self, text = "")
		self.fillerLine.grid(row=10, column=0)

		lanthanide = [
		('Ce', 'Cerium', '58', '140.12', 'Solid', 'Transitional Metals'),
		('Pr', 'Praseodymium', '59', '140.91', 'Solid', 'Transitional Metals'),
		('Nd', 'Neodymium', '60', '144.24', 'Solid', 'Transitional Metals'),
		('Pm', 'Promethium', '61', '145.00', 'Synthetic', 'Transitional Metals'),
		('Sm', 'Samarium', '62', '150.40', 'Solid', 'Transitional Metals'),
		('Eu', 'Europium', '63', '151.96', 'Solid', 'Transitional Metals'),
		('Gd', 'Gadolinium', '64', '157.25', 'Solid', 'Transitional Metals'),
		('Tb', 'Terbium', '65', '158.93', 'Solid', 'Transitional Metals'),
		('Dy', 'Dyprosium', '66', '162.50', 'Solid', 'Transitional Metals'),
		('Ho', 'Holmium', '67', '164.93', 'Solid', 'Transitional Metals'),
		('Er', 'Erbium', '68', '167.26', 'Solid', 'Transitional Metals'),
		('Tm', 'Thulium', '69', '168.93', 'Solid', 'Transitional Metals'),
		('Yb', 'Ytterbium', '70', '173.04', 'Solid', 'Transitional Metals'),
		('Lu', 'Lutetium', '71', '174.97', 'Solid', 'Transitional Metals')]
		r = 11
		c = 3
		for b in lanthanide:
			tk.Button(self,text=b[0],width=1,height=1, bg="light goldenrod",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			c += 1
			if c > 18:
				c = 1
				r += 1

		Actinide = [
		('Th', 'Thorium', '90', '232.04', 'Solid', 'Transitional Metals'),
		('Pa', 'Protactinium', '91', '231.04', 'Solid', 'Transitional Metals'),
		('U', 'Uranium', '92', '238.03', 'Solid', 'Transitional Metals'),
		('Np', 'Neptunium', '93', '237.05', 'Synthetic', 'Transitional Metals'),
		('Pu', 'Plutonium', '94', '244.00', 'Synthetic', 'Transitional Metals'),
		('Am', 'Americium', '95', '243.00', 'Synthetic', 'Transitional Metals'),
		('Cm', 'Curium', '96', '247', 'Synthetic', 'Transitional Metals'),
		('Bk', 'Berkelium', '97', '247', 'Synthetic', 'Transitional Metals'),
		('Cf', 'Californium', '98', '247', 'Synthetic', 'Transitional Metals'),
		('Es', 'Einsteinium', '99', '252.00', 'Synthetic', 'Transitional Metals'),
		('Fm', 'Fermium', '100', '257.00', 'Synthetic', 'Transitional Metals'),
		('Md', 'Mendelevium', '101', '260.00', 'Synthetic', 'Transitional Metals'),
		('No', 'Nobelium', '102', '259', 'Synthetic', 'Transitional Metals'),
		('Lr', 'Lawrencium', '103', '262', 'Synthetic', 'Transitional Metals')]
		r = 12
		c = 3
		for b in Actinide:
			tk.Button(self,text=b[0],width=1,height=1, bg="light goldenrod",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			c += 1
			if c > 18:
				c = 1
				r += 1

		reset = [
		('Close', 'Click the element you would like information about.', '')]
		r = 12
		c = 0
		for b in reset:
			tk.Button(self,text=b[0],width=1,height=1, bg="black", fg="white",command=lambda text=b:self.return_element(text)).grid(row=r,column=c)
			r += 1
			if r > 7:
				r = 1
				c += 1

		self.infoLine = Label(self, text = "", justify = 'left')
		self.infoLine.grid(row=1,column=3,columnspan=10,rowspan=4)

		self.minsize(650, 300)
		self.mainloop()

	def return_element(self, elem):
		self.element_chosen = elem
		self.destroy()
		self.quit()
		return

#Creates an instance of 'app' class
def main():
	a = App()

def periodic_table_popup():
	a = App()
	if a.element_chosen[0] == 'Close':
		return 'Close'
	return a.element_chosen

#runs main function
if __name__ == "__main__":
	main()
