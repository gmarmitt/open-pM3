from math import sqrt
import argparse

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - DEFAULT  VALUES - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

defaults = {
	'v' 	: 2,
	'i1' 	: 1,
	'i2' 	: 2,
	'i3' 	: 3,
	'twocores' : False,
	'half' : False,
	'o' 	: 'coreshell.mtx'
}

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - GENERATE  - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def generate(args):
	x = int(2*args.r1)
	y = int(2*args.r1)
	z = int(2*args.r1)

	centro = (x - 1)/2

	# if using 'half' option, create only the top half of the matrix
	# example: python3 coreshell.py 10 5 --half
	if args.half == True:
		z0 = int(z/2)
	else:
		z0 = 0

	matriz = [[[0 for i in range(x)] for j in range(y)] for k in range(z)]

	for k in range(z - z0):
		for j in range(y):
			for i in range(x):
				d = sqrt((i-centro)**2 + (j-centro)**2 + (k-centro)**2)
				if d <= args.r1:
					matriz[i][j][k] = args.i1

	for k in range(z - z0):
		for j in range(y):
			for i in range(x):
				d = sqrt((i-centro)**2 + (j-centro)**2 + (k-centro)**2)
				if d <= args.r2:
					matriz[i][j][k] = args.i2

	# if using 'twocores' option, create a core shell with two cores
	# example: python3 coreshell.py 10 5 --twocores -r3 3 -i3 3
	if args.twocores == True:

		for k in range(z - z0):
			for j in range(y):
				for i in range(x):
					d = sqrt((i-centro)**2 + (j-centro)**2 + (k-centro)**2)
					if d <= args.r3:
						matriz[i][j][k] = args.i3

	# uncompressed output
	if args.v == 1:
		with open(args.o, 'w') as f:
			for k in range(z):
				for j in range(y):
					for i in range(x):
						print(matriz[i][j][k], end=' ', file=f)
					print('', file=f)
				print('', file=f)

	# compressed output
	if args.v == 2:
		with open(args.o, 'w') as f:
			print(x, y, z, '0', file=f)
			iCp_old = matriz[0][0][0]
			c = 0
			for k in range(z):
				for j in range(y):
					for i in range(x):
						iCp = matriz[i][j][k]
						if (iCp != iCp_old) or (c == 50000):
							print(c, iCp_old, file=f)
							c = 0
							iCp_old = iCp
						c += 1
			print(c, iCp_old, file=f)

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - MAIN  - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

if __name__ == '__main__':

	parser = argparse.ArgumentParser(description='Create a core/shell centered matrix for PowerMeis.')
	parser.add_argument('r1', type=float, help='radius of the shell + radius of the core')
	parser.add_argument('r2', type=float, help='radius of the core')
	parser.add_argument('-v', type=int, help='output type, 1=old version, 2=compacted (default=2)', default=defaults['v'])
	parser.add_argument('-i1', type=int, help='index of the shell (correspond to the compound used in PowerMEIS) (default=1)', default=defaults['i1'])
	parser.add_argument('-i2', type=int, help='index of the core (correspond to the compound used in PowerMEIS) (default=2)', default=defaults['i2'])
	parser.add_argument('--twocores', dest='twocores', action='store_true')
	parser.add_argument('-r3', type=float, help='radius of the second core')
	parser.add_argument('-i3', type=int, help='index of the core (default=3)', default=defaults['i3'])
	parser.add_argument('-o', type=str, help='output matrix\'s file name (default=coreshell.mtx)', default=defaults['o'])
	parser.add_argument('--half', dest='half', action='store_true')

	args = parser.parse_args()

	generate(args)
