import argparse

#Example:
#1) Simple film: 'python3 trench.py 100 10 200 "1000 1" 200'. In this case we create a layer with dimension 100 in x, 10 in y, and 200 in z.
#The size of the array will be determined by the dL parameter in PowerMEIS. Note that the compound is first described by the size x * y and then by the compound.

#2) FinFET structure:
#First step: 'python3 trench.py 100 10 200 "500 1/500 0" 50'
#Second step: 'python3 trench.py 100 10 200 "1000 1" 150 -i layer.mtx'
#With this procedure (layer-by-layer) we can build any structure.


### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - DEFAULT  VALUES - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

defaults = {
	'theta' : 0,
	'i' 	: None,
	'o' 	: 'layer.mtx'
}

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - GENERATE  - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def generate(args):
	i = 0
	mtx_out = ''

	c_array = args.c.split('/')
	while(i<(args.p)):
		for c_i in c_array:
			mtx_out += c_i+'\n'
		i=i+1;

	if args.i == None:
		with open(args.o, 'w') as f:
			print(args.x, args.y, args.z, args.theta, file=f)
			print(mtx_out, end='', file=f)

	if args.i != None:
		with open(args.i, 'r') as f:
			mtx_in = f.read().split('\n')

		with open(args.o, 'w') as f:
			print(args.x, args.y, args.z, args.theta, file=f)

			for line in mtx_in[1:-1]:
				print(line, file=f)
			print(mtx_in[len(mtx_in)-1], end='', file=f)

			print(mtx_out, end='', file=f)

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - MAIN  - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

if __name__ == '__main__':

	parser = argparse.ArgumentParser(description='Create a sample matrix with n layers for PowerMeis.')
	parser.add_argument('x', type=int, help='matrix dimension in x')
	parser.add_argument('y', type=int, help='matrix dimension in y')
	parser.add_argument('z', type=int, help='matrix dimension in z')
	parser.add_argument('-theta', type=int, help='rotation in the xy plane (default=0)', default=defaults['theta'])
	parser.add_argument('c', type=str, help='composition')
	parser.add_argument('p', type=int, help='depth')
	parser.add_argument('-o', type=str, help='output matrix\'s file name (default=layer.mtx)', default=defaults['o'])
	parser.add_argument('-i', type=str, help='!input or input', default=defaults['i'])

	args = parser.parse_args()

	generate(args)
