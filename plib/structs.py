# Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ctypes import *

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  CTYPES   - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
#This section defines all structures in ctypes. For some of them memory allocation is necessary.

class STvector(Structure):
	_fields_=[	("x"		, c_double),
				("y"		, c_double),
				("z"		, c_double)]

	def __init__(self):				#zeroes values by default
		self.x = 0
		self.y = 0
		self.z = 0

class STarray(Structure):
	_fields_=[	("array"	, POINTER(c_double)),
				("initial"	, c_double),
				("delta"	, c_double),
				("size"		, c_int)]

	def __init__(self, initial, delta, size):				#allocate memory for the array
		self.initial = initial
		self.delta = delta
		self.size = size

		self.array = (c_double * self.size)()
		for i in range(self.size):
			self.array[i] = 0

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

class STplas(Structure):	#plasmon parameters
	_fields_=[	("A"		, c_double),					#intensity
				("w0"		, c_double),					#energy mean
				("gamma"	, c_double)]					#energy width

	def __init__(self):				#zeroes values by default
		self.A 		= 0
		self.w0 	= 0
		self.gamma 	= 0

class STbstate(Structure):	#bound state parameters
	_fields_=[	("mode"		, c_int),					#mode: Ib=0, Zeff=1
				("n"		, c_int),					#electron shell
				("l"		, c_int),					#subshell label
				("Ne"		, c_double),				#number of electrons
				("Ib"		, c_double)]				#binding energy

	def __init__(self):				#zeroes values by default
		self.n 		= 0
		self.l 		= 0
		self.Ne 	= 0
		self.Ib 	= 0

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

class STelem(Structure):	#elements parameters
	_fields_=[	("name"		, c_char_p),					#name
				("CS_file"	, c_char_p),					#CS file
				("Z"		, c_int),						#atomic number
				("M"		, c_double),					#atomic mass
				("sig"		, c_double),					#sigma0 from single collision
				("Ekin"		, c_double),					#kinectic energy
				("Er_mean"	, c_double),					#ressonance energy mean
				("Er_width"	, c_double),					#ressonance energy std deviation
				("boundstate", POINTER(STbstate))]			#bound state vector

	def __init__(self, nBS):				#zeroes values by default
		self.Z 		= 0
		self.M 		= 0
		self.sig 	= 0
		self.Ekin 	= 0
		self.Er_mean 	= 0
		self.Er_width 	= 0

		self.boundstate = (STbstate * nBS)()
		for iBS in range(1, nBS):
			self.boundstate[iBS].__init__()

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

class STcomp(Structure):	#compositions parameters
	_fields_=[	("name"		, c_char_p),			#name
				("dens"		, c_double),			#density
				("nElem"	, c_int),				#number of elements in this compound
				("eRBS_dPdx", c_double),			#eRBS exponential probability
				("dedx"		, POINTER(c_double)),	#energy loss
				("dwdx"		, POINTER(c_double)),	#straggling
				("dwdx_corr", c_double),			#straggling multiplication correction
				("dEdx_corr", c_double),			#energy loss multiplication correction
				("frac_elem", POINTER(c_double)),	#elementar fraction of this compound
				("surface_plasmon"	, STplas),		#surface plasmon
				("plasmon"	, POINTER(STplas)),		#plasmon vector
				("VEGAS_file"	, c_char_p),		#vegas cristal visibility file from VEGAS
				("nCrystals"	, c_int)]			#number of crystaline planes

	def __init__(self, nEl, nPl):			#receives nEl, nPl from prmt __init__ cast
		self.dens 		= 0
		self.nElem 		= 0
		self.eRBS_dPdx 	= 0
		self.dwdx_corr 	= 0
		self.dEdx_corr 	= 0

		self.dedx 		= (c_double * nEl)()
		self.dwdx 		= (c_double * nEl)()
		self.frac_elem 	= (c_double * nEl)()
		for iEl in range(0, nEl):
			self.dedx[iEl]		= 0
			self.dwdx[iEl]		= 0
			self.frac_elem[iEl]	= 0

		self.plasmon = (STplas * nPl)()
		for iPl in range(1, nPl):
			self.plasmon[iPl].__init__()

		self.nCrystals = 0

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

class STlayer(Structure):	#layer description
	_fields_=[	("lay_type"		, c_int),				#layer type: 'nano=1' or 'film=0'
				("thick"		, c_double),				#layer total thickness
				("dL"			, POINTER(c_double)),	#dL's for this layer
				("period_cont"	, c_int),				#periodic contourn: 'yes=1' or 'no=0'
				("period_posi"	, c_int),				#periodic matrix position: 'yes=1' or 'no=0'
				("Cp"			, c_int),				#default composition
				("LAYER_nMt"	, c_int),				#number of matrix - INDEXED PER LAYER
				("Mt_weight"	, POINTER(c_double))]	#statisticas weight of each matrix - INDEXED GLOBALY

	def __init__(self, nMt):			#receives nMt from prmt __init__ cast
		self.dL = (c_double * 3)()

		self.Mt_weight 	= (c_double * nMt)()

		self.lay_type = 0
		self.thick = 0
		self.period_cont = 0
		self.period_posi = 0
		self.Cp = 0
		self.LAYER_nMt = 0
		for iMt in range(0, nMt):
			self.Mt_weight[iMt] = 0

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

class STsimu(Structure):	#simulation parameters
	_fields_=[	("algorithm"	, c_int),		#simulation algorithm: '0' std, '1' traj
				("surf_apprx"	, c_int),		#surface approximation: '0' off, 1 'on'
				("autofit"		, c_int),		#autofit switch: 'on=1' or 'off=0'
				("nThreads"		, c_int),		#number of threads
				("nI"			, c_int64),		#number of interactions
				("nI_per_thread", c_int64),		#number of interactions per thread
				("line_shape"	, c_int),		#line shape: 'EMG=1' or 'gauss=0'
				("cross_section", c_int),		#cross section: 'Ziegler=1' or 'Rutherford=0'
				("dL"			, c_double),	#length of each little cube used
				("dr"			, c_double)]	#length used in the path integration

	def __init__(self):				#zeroes values by default
		self.algorithm 		= 0
		self.surf_apprx 	= 0
		self.autofit		= 0
		self.nThreads 		= 0
		self.nI 			= 0
		self.nI_per_thread 	= 0
		self.line_shape 	= 0
		self.cross_section 	= 0
		self.dL 			= 0
		self.dr 			= 0

class STcexpl(Structure):	#coulomb explosion parameters
	_fields_=[	("stopping_ratio"	, c_double),		#energy loss ratio for molecules
				("gamma"			, c_double),		#straggling for molecules
				("alpha"			, c_double)]		#correction variable for deep gammas

	def __init__(self):				#zeroes values by default
		self.stopping_ratio 	= 0
		self.gamma		 		= 0
		self.alpha		 		= 0

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

class STcalc(Structure):		#calculation parameters
	_fields_=[	("stopp_mode"	, c_int),		#'STOP96=1'
				("strag_mode"	, c_int),		#'Bohr=1', 'Lind=2', 'Chu=3', 'Yang=4'
				("neutr_mode"	, c_int),		#'Marion=1', 'CasP=2'
				("dielec_mode"	, c_int),		#
				("stopp_dE"		, c_double),	#energy step
				("neutr_dE"		, c_double),	#energy step
				("dielec_dE"	, c_double),	#beam energy step
				("dielec_dw"	, c_double),	#recoil energy step
				("dielec_dq"	, c_double),	#recoil momemtum step
				("dielec_wmax"	, c_double),	#recoil maximum energy
				("dielec_qmax"	, c_double)]	#recoil maximum momemtum

	def __init__(self):				#zeroes values by default
		self.stopp_mode = 0
		self.strag_mode = 0
		self.neutr_mode = 0
		self.dielec_mode = 0
		self.stopp_dE = 0
		self.neutr_dE = 0
		self.dielec_dE = 0
		self.dielec_dw = 0
		self.dielec_dq = 0
		self.dielec_wmax = 0
		self.dielec_qmax = 0

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

class SToutp(Structure):	#output parameters
	_fields_=[	("Emin"			, c_double),	#initial value
				("Emax"			, c_double),	#final value
				("Tmin"			, c_double),	#initial value
				("Tmax"			, c_double),	#final value
				("dEnergy"		, c_double),	#energy step
				("dAngle"		, c_double),	#angle step
				("dTime"		, c_double),	#time step
				("nEn"			, c_int),		#number of steps in output
				("nAn"			, c_int),		#number of steps in output
				("nTm"			, c_int),		#number of steps in output
				("tof"			, c_int),		#output in time (ns) scale
				("histo_type"	, c_int)]		#histogram output options

	def __init__(self):				#zeroes values by default
		self.Emin 		= 0
		self.Emax 		= 0
		self.Tmin 		= 0
		self.Tmax 		= 0
		self.dEnergy 	= 0
		self.dAngle 	= 0
		self.dTime 		= 0
		self.nEn 		= 0
		self.nAn 		= 0
		self.nTm 		= 0
		self.tof	 	= 0
		self.histo_type	= 0

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

class STbeam(Structure):
	_fields_=[	("btype"	, c_int),	#beam type: '0' ion, '1' molecule, '2' electron
				("Z"	, c_int),		#atomic number
				("M"	, c_double),	#atomic mass
				("Q"	, c_double),	#charge
				("E0"	, c_double),	#energy
				("prtcl_sr"	, c_double),	#total charge deposited
				("T1"	, c_double),	#incident angle
				("vec"	, STvector)]	#vector pointing along the beam direction

	def __init__(self):				#zeroes values by default
		self.Z 	= 0
		self.M 	= 0
		self.Q 	= 0
		self.E0 = 0
		self.prtcl_sr = 0
		self.T1 = 0

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

class STdete(Structure):
	_fields_=[	("sig_exp2"	, c_double),		#sigma^2 of the experimental resolution
				("sig_exp2_TOF"	, c_double),	#sigma^2 of the experimental TOF resolution
				("det_type"	, c_int),			#detector type: 'eletrostatic=0' or 'TOF=1'
				("LX"		, c_double),		#delta 'out of plane'
				("X0"		, c_double),		#horizontal central position
				("dX"		, c_double),		#angle 'out of plane' step
				("LY"		, c_double),		#delta 'on plane'
				("Y0"		, c_double),		#vertical central position
				("dY"		, c_double),		#angle 'on plane' step
				("r"		, c_double),		#circular detector radius
				("d_tof"	, c_double),		#tof distance
				("vec"		, STvector),		#vector pointing to the center of the detector (X0, Y0)
				("nthetax"	, c_int),			#number of steps 'out of plane'
				("nthetay"	, c_int),			#number of steps 'on plane'
				("theta_min", c_double),		#min angle 'on plane'
				("theta_max", c_double)]		#max angle 'on plane'

	def __init__(self):				#zeroes values by default
		self.sig_exp2 		= 0
		self.sig_exp2_TOF 	= 0
		self.det_type 	= 0
		self.LX 		= 0
		self.X0 		= 0
		self.dX 		= 0
		self.LY 		= 0
		self.Y0 		= 0
		self.dY 		= 0
		self.r 			= 0
		self.d_tof 		= 0
		self.nthetax 	= 0
		self.nthetay 	= 0
		self.theta_min 	= 0
		self.theta_max 	= 0

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

class STssct(Structure):		#single scattering parameters
	_fields_=[	("NS_cut", c_double)]

	def __init__(self):				#zeroes values by default
		self.NS_cut 	= 0

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

class STstraj(Structure):		#trajectory simulation parameters
	_fields_=[	("MS"				, c_int),
				("direct"			, c_int),
				("memory"			, c_int),		#maximum allowed memory in MBs
				("iMt"				, c_int),
				("CS_cut_algo"		, c_int),
				("nI"				, c_int64),
				("nI_per_thread"	, c_int64),
				("nIon"				, c_int64),
				("max_distance"		, c_double),
				("dE"				, c_double),
				("angle_min"		, c_double),
				("angle_max"		, c_double),
				("CS_frac"			, c_double),	#if CS/CS_tot > CS_frac the connection is disregarded
				("eflp"				, c_double)]

	def __init__(self):				#zeroes values by default
		self.MS 				= 0
		self.direct 			= 0
		self.memory 			= 0
		self.nI 				= 0
		self.nI_per_thread		= 0
		self.nIon	 			= 0
		self.max_distance 		= 0
		self.dE			 		= 0
		self.angle_min 			= 0
		self.angle_max 			= 0
		self.CS_frac 			= 0
		self.eflp 				= 0
		self.iMt				= 0
		self.CS_cut_algo		= 0

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

class STsnrp(Structure):		#NRP simulation parameters
	_fields_=[	("Ei"		, c_double),		#initial scan energy
				("Ef"		, c_double)]		#final scan energy

	def __init__(self):				#zeroes values by default
		self.Ei 		= 0
		self.Ef 		= 0

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

class STprmt(Structure):
	_fields_=[	("debug"	, c_int),
				("log"		, c_int),
				("nEl"		, c_int),
				("nCp"		, c_int),
				("nLa"		, c_int),
				("nMt"		, c_int),
				("nPl"		, c_int),
				("nBS"		, c_int),
				("thick"	, c_double),
				("simu"		, STsimu),
				("cexpl"	, STcexpl),
				("calc"		, STcalc),
				("outp"		, SToutp),
				("beam"		, STbeam),
				("dete"		, STdete),
				("ssct"		, STssct),
				("straj"	, STstraj),
				("snrp"		, STsnrp),
				("elem"		, POINTER(STelem)),
				("comp"		, POINTER(STcomp)),
				("layer"	, POINTER(STlayer))]

	def __init__(self, nCp, nEl, nLa, nMt, nPl, nBS):

		#set these values as it was opened
		self.nCp 	= nCp
		self.nEl 	= nEl
		self.nLa 	= nLa
		self.nMt 	= nMt
		self.nPl 	= nPl
		self.nBS 	= nBS

		#zeroes other values of these variables
		self.debug 	= 0
		self.nSc 	= 0
		self.thick 	= 0
		self.simu.__init__()
		self.cexpl.__init__()
		self.calc.__init__()
		self.outp.__init__()
		self.beam.__init__()
		self.dete.__init__()
		self.ssct.__init__()
		self.straj.__init__()
		self.snrp.__init__()

		self.comp 	= (STcomp * (nCp+1))()		#open an array with size p_comp, ...
		self.elem 	= (STelem * (nEl+1))()
		self.layer 	= (STlayer * (nLa+1))()

		for iEl in range(self.nEl+1):
			self.elem[iEl].__init__(nBS+1)

		for iCp in range(self.nCp+1):				#for each element in comp[i]:
			self.comp[iCp].__init__(nEl+1, nPl+1)	#sends nEl, nPl as parameters

		for iLa in range(self.nLa+1):			#for each element in layer[i]:
			self.layer[iLa].__init__(nMt+1)		#sends nMt as parameter


### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

class STtraj(Structure):
	_fields_=[	("theta"	, c_double),
				("phi"		, c_double),
				("delta_E"	, c_double),
				("delta_W2"	, c_double),
				("E"		, c_double),
				("CS_norm"	, c_double)]
	def __init__(self):
		self.theta 		= 0
		self.phi 		= 0
		self.delta_E 	= 0
		self.delta_W2 	= 0
		self.E 			= 0
		self.CS_norm	= 0

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

class STmatrix(Structure):
	_fields_=[	("FileName"	, c_char_p),
				("FileData"	, c_char_p),
				("mtx_type"	, c_char_p),
				("matgen_name"	, c_char_p),			#matrix generator name
				("matgen_attr"	, c_char_p),			#matrix generator attributes
				("nx"		, c_int),
				("ny"		, c_int),
				("nz"		, c_int),
				("phi"		, c_double),
				("dCp"		, c_double),
				("Cp_ratio"	, c_double),
				("rand_phi"	, c_int),
				("Cp"		, POINTER(POINTER(POINTER(c_ushort)))),
				("theta_avrg", POINTER(POINTER(POINTER(c_double)))),
				("theta_devi", POINTER(POINTER(POINTER(c_double)))),
				("traj"		, POINTER(POINTER(POINTER(POINTER(STtraj)))))]

	def __init__(self):
		self.Cp = (POINTER(POINTER(c_ushort)) * self.nx)()
		for i in range(self.nx):
			self.Cp[i] = (POINTER(c_ushort) * self.ny)()
			for j in range(self.ny):
				self.Cp[i][j] = (c_ushort * self.nz)()

	def allocate_traj(self, nIon):
		N = 100*nIon+1
		self.traj = (POINTER(POINTER(POINTER(STtraj))) * self.nx)()
		self.theta_avrg = (POINTER(POINTER(c_double)) * self.nx)()
		self.theta_devi = (POINTER(POINTER(c_double)) * self.nx)()
		for i in range(self.nx):
			self.traj[i] = (POINTER(POINTER(STtraj)) * self.ny)()
			self.theta_avrg[i] = (POINTER(c_double) * self.ny)()
			self.theta_devi[i] = (POINTER(c_double) * self.ny)()
			for j in range(self.ny):
				self.traj[i][j] = (POINTER(STtraj) * self.nz)()
				self.theta_avrg[i][j] = (c_double * self.nz)()
				self.theta_devi[i][j] = (c_double * self.nz)()
				for k in range(self.nz):
					if self.Cp[i][j][k] != 0:
						self.traj[i][j][k] = (STtraj * N)()

	def zeroes_traj(self, nIon):
		N = 100*nIon+1
		for i in range(self.nx):
			for j in range(self.ny):
				for k in range(self.nz):
					if self.Cp[i][j][k] != 0:
						for iTraj in range(N):
							self.traj[i][j][k][iTraj].delta_E = 0

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

class STcomptab(Structure):	#compounds table
	_fields_=[	("P_wq"		, POINTER(POINTER(c_double))),	#inelastic energy loss probability (without delta)
				("P_E"		, STarray),						#integral of P_Eq for all dq and dw (with delta)
				("P_E_accu"	, POINTER(POINTER(c_double))),	#accumulation function of P_E with respect to w
				("dedx_E"	, POINTER(STarray)),	#energy loss as a function of energy
				("dwdx_E"	, POINTER(STarray)),	#straggling as a function of energy
				("neutr"	, STarray),				#neutralization vector
				("visibility", POINTER(STarray))] 		#crystaline visibility, by angle and depth

	def __init__(self, E_min, dE, nE):				#zeroes values by default
		self.neutr.__init__(E_min, dE, nE)

	def allocmem_dedx_dwdx(self, nEl, dE, nE):
		self.dedx_E 	= (STarray * nEl)()
		self.dwdx_E 	= (STarray * nEl)()
		for iEl in range(0, nEl):
			self.dedx_E[iEl].__init__(0, dE, nE)
			self.dwdx_E[iEl].__init__(0, dE, nE)

	def allocate_dielectric_vectors(self, E_min, dE, nE, nw, nq):
		self.P_E.__init__(E_min, dE, nE)

		self.P_wq = (POINTER(c_double) * nw)()
		for iw in range(nw):
			self.P_wq[iw] = (c_double * nq)()
			for iq in range(nq):
				self.P_wq[iw][iq] = 0

		self.P_E_accu = (POINTER(c_double) * nE)()
		for iE in range(nE):
			self.P_E_accu[iE] = (c_double * nw)()
			for iw in range(nw):
				self.P_E_accu[iE][iw] = 0

	def allocate_visibility_vectors(self, theta_min, dtheta, ntheta, nCrystals):
		self.visibility = (STarray * nCrystals)()
		for iCrystal in range(nCrystals):
			self.visibility[iCrystal].__init__(theta_min, dtheta, ntheta)

class STelemtab(Structure):	#elements table
	_fields_=[	("K"				, STarray),		#kinemectic factor
				("T"				, STarray),		#transmission factor (T = 1-K)
				("CS"				, STarray),		#scattering cross section
				("CS_total"			, STarray),		#total cross section for colisions in this element
				("CS_angle_cut"		, STarray),		#total cross section in (angle_cut, 180)
				("CS_accumulation"	, POINTER(POINTER(c_double))),		#total cross section accumulation
				("CS_correc"		, POINTER(POINTER(c_double)))]	#scattering cross section correction

	def __init__(self, theta_min, dtheta, ntheta, Emin, dEn, nEn):	#zeroes values by default
		self.K.__init__(theta_min, dtheta, ntheta)
		self.T.__init__(theta_min, dtheta, ntheta)
		self.CS.__init__(theta_min, dtheta, ntheta)
		self.CS_total.__init__(Emin, dEn, nEn)
		self.CS_angle_cut.__init__(Emin, dEn, nEn)

		self.CS_accumulation = (POINTER(c_double) * ntheta)()
		self.CS_correc = (POINTER(c_double) * ntheta)()
		for iAn in range(ntheta):
			self.CS_accumulation[iAn] = (c_double * nEn)()
			self.CS_correc[iAn] = (c_double * nEn)()
			for iEn in range(nEn):
				self.CS_accumulation[iAn][iEn] = 0
				self.CS_correc[iAn][iEn] = 1

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

class STtables(Structure):
	_fields_=[	("comp"	, POINTER(STcomptab)),			#compound vector
				("elem"	, POINTER(STelemtab))]			#element vector

	def __init__(self, nEl, nCp, theta_min, ntheta, dtheta, Emin, dEn, nEn):
		self.comp = (STcomptab * (nCp+1))()
		for iCp in range(1, nCp+1):
			self.comp[iCp].__init__(Emin, dEn, nEn)

		self.elem = (STelemtab * (nEl+1))()
		for iEl in range(1, nEl+1):
			self.elem[iEl].__init__(theta_min, dtheta, ntheta, Emin, dEn, nEn)

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

class SThisto(Structure):
	_fields_=[	("norm_ssct"		, c_double),
				("simple"			, POINTER(POINTER(c_double))),
				("element"			, POINTER(POINTER(c_double))),
				("compound"			, POINTER(POINTER(c_double)))]

	def __init__(self, histo_type, tof, nCp, nEl, nEn, nAn, nTm, norm):	#receives params from cast
		self.norm_ssct = norm;

		if tof == 1:
			nx = nTm
		else:
			nx = nEn

		if histo_type == 0:
			self.simple = (POINTER(c_double) * nx)()
			for i in range(0, nx):
				self.simple[i] = (c_double * nAn)()
		elif histo_type == 1:
			self.element = (POINTER(c_double) * nx)()
			for i in range(0, nx):
				self.element[i] = (c_double * nEl)()
		elif histo_type == 2:
			self.compound = (POINTER(c_double) * nx)()
			for i in range(0, nx):
				self.compound[i] = (c_double * nCp)()
