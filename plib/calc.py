# Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import math
import ctypes
import hashlib

#local libs
if __name__ == 'pm3.plib.calc':
	from pm3.plib.structs 	import *
else:
	from plib.structs 	import *

#constants
e2 = 1.4398e-7;			#eV * cm

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - GENERATE MATRIX - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def MTX_GEN_C(PATH):
	#open pM3 c++ library
	clib = CDLL(PATH.clib[0])

	clib.launcher.argtypes = [c_float, c_float, c_float, c_float, c_int, c_int, c_int, c_int, c_int, c_int, c_char, c_char, c_int, c_float, c_float, c_float]

	dl = c_float(2)
	radiuscore = c_float(20)
	radiusshell = c_float(30)
	radiusc = c_float(40)
	corenumber = c_int(1)
	shellnumber = c_int(2)
	radc = c_int(1)
	Npix = c_int(4)
	Npiy = c_int(4)
	Ntam = c_int(4)
	alloytudo = c_char('n'.encode())
	cascaptpd = c_char('n'.encode())
	cnum = c_int(4)
	radiuspt = c_float(20)
	radiuspd = c_float(30)
	cs = c_float(10)

	clib.launcher(dl, radiuscore, radiusshell, radiusc, corenumber, shellnumber, radc, Npix, Npiy, Ntam, alloytudo, cascaptpd, cnum, radiuspt, radiuspd, cs);

def get_matrix_size(matrix_c, iMt):
	#if the matrix size was not defined in the cpm, read it from the matrix file
	if matrix_c[iMt].nx == 0 and matrix_c[iMt].ny == 0 and matrix_c[iMt].nz == 0:
		try:
			matrix_file = open(matrix_c[iMt].FileName, 'r')
		except:
			raise ValueError(200)

		#read the first line
		line_raw = matrix_file.readline()
		line = list(filter(None, line_raw.split()))

		#read the first line
		matrix_c[iMt].nx = int(line[0])
		matrix_c[iMt].ny = int(line[1])
		matrix_c[iMt].nz = int(line[2])

		if len(line) >= 4:
			matrix_c[iMt].phi = math.radians(float(line[3]))
		else:
			matrix_c[iMt].phi = 0

def GENERATE_MATRIX_C(PATH, prmt_c, matrix_c):

	#open pM3 c++ library
	clib = CDLL(PATH.clib[0])

	clib.GENERATE_PM3_MATRIX.argtypes = [POINTER(STmatrix)]
	clib.GENERATE_PM3_MATRIX.restypes = c_int;
	clib.GENERATE_TRI3DYN_MATRIX.argtypes = [POINTER(STprmt), POINTER(STmatrix)]
	clib.GENERATE_TRI3DYN_MATRIX.restypes = c_int;

	# this change is made to keep the correct coordinates when converting TRI3DYN matrixes
	for iMt in range(1, prmt_c.nMt+1):		#for each matrix
		if matrix_c[iMt].mtx_type.decode() == 'TRI3DYN':
			# i ---> k
			# k ---> j
			# j ---> i
			var_ny = matrix_c[iMt].ny
			matrix_c[iMt].ny = matrix_c[iMt].nz
			matrix_c[iMt].nz = matrix_c[iMt].nx
			matrix_c[iMt].nx = var_ny
	for iLa in range(1, prmt_c.nLa+1):
		for iMt in range(1, prmt_c.nMt+1):		#for each matrix
			if prmt_c.layer[iLa].Mt_weight[iMt] != 0 and matrix_c[iMt].mtx_type.decode() == 'TRI3DYN':
				# i ---> k
				# k ---> j
				# j ---> i
				var_dLy = prmt_c.layer[iLa].dL[1]
				prmt_c.layer[iLa].dL[1] = prmt_c.layer[iLa].dL[2]
				prmt_c.layer[iLa].dL[2] = prmt_c.layer[iLa].dL[0]
				prmt_c.layer[iLa].dL[0] = var_dLy
				break

	for iMt in range(1, prmt_c.nMt+1):		#for each matrix
		if matrix_c[iMt].mtx_type.decode() == 'pM3':
			#get nx, ny, nz and phi values
			get_matrix_size(matrix_c, iMt)

			#initialize the structure, allocate memory for Cp
			matrix_c[iMt].__init__()

			#run generation function
			err = clib.GENERATE_PM3_MATRIX(ctypes.byref(matrix_c[iMt]))
			if err != 0:
				raise ValueError(err)

		if matrix_c[iMt].mtx_type.decode() == 'TRI3DYN':
			#initialize the structure, allocate memory for Cp
			matrix_c[iMt].__init__()

			#run generation function
			err = clib.GENERATE_TRI3DYN_MATRIX(ctypes.byref(prmt_c), ctypes.byref(matrix_c[iMt]))
			if err != 0:
				raise ValueError(err)

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - ANGULAR FUNCTIONS - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def get_angle_between(a, b):
	return math.acos(a.x*b.x + a.y*b.y + a.z*b.z);

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

def rotate(s, u):
	cos_theta = u.z
	sin_theta = math.sqrt(1 - cos_theta*cos_theta)

	if sin_theta == 0 :
		cos_phi = 1
		sin_phi = 0
	else:
		sin_phi = u.x/sin_theta
		cos_phi = u.y/sin_theta

	s_sup = STvector()
	s_sup.x = s.x
	s_sup.y = s.y
	s_sup.z = s.z

	var = s_sup.y*cos_theta + s_sup.z*sin_theta

	s.x = var*sin_phi + s_sup.x*cos_phi
	s.y = var*cos_phi - s_sup.x*sin_phi
	s.z = -s_sup.y*sin_theta + s_sup.z*cos_theta

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - PREPARATION OF VARIABLES  - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
import ast
import importlib
from collections import namedtuple

def MATGEN(prmt_c, matrix_c):
	for iMt in range(1, prmt_c.nMt+1):
		if matrix_c[iMt].matgen_name.decode() != 'None' and matrix_c[iMt].matgen_name.decode() != '':

			# load matgen module by name
			module_name = 'plib.matgens.'+matrix_c[iMt].matgen_name.decode().split('.')[0]
			matgen = importlib.import_module(module_name)

			# read parameters
			params = dict()
			attr = matrix_c[iMt].matgen_attr.decode().split()
			for i in range(0, len(attr), 2):
				params[attr[i]] = ast.literal_eval(attr[i+1])

			# if parameter is not in args, use its default value
			for key in matgen.defaults:
				params[key] = params.get(key, matgen.defaults[key])

			# convert from dictionary to tuple to enable attribute calls
			args = namedtuple('args', params.keys())(*params.values())

			# generate matrix
			matgen.generate(args)

			# update matrix name to matgen output name
			matrix_c[iMt].FileName = c_char_p((args.o).encode())

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def REFACTOR_PARAMETERS(prmt_c, matrix_c, config):
	for iMt in range(1, prmt_c.nMt+1):
		config['matrix '+str(iMt)]['data'] = matrix_c[iMt].FileData.decode()

	#count the number of matrix files per layer
	nMt_layer = [0 for iLa in range(prmt_c.nLa+1)]
	iMt_layer = [0 for iLa in range(prmt_c.nLa+1)]
	for iLa in range(1, prmt_c.nLa+1):
		for iMt in range(1, prmt_c.nMt+1):
			if prmt_c.layer[iLa].Mt_weight[iMt] > 0:
				nMt_layer[iLa] += 1
				iMt_layer[iLa] = iMt

	#if there is one matrix and one layer, trivial case
	if sum(nMt_layer) == 1 and prmt_c.nLa == 1:
		config['trajectory'].iMt = nMt_layer[1]
		return config

	#else we need to build a matrix from the given layers and/or matrix
	else:
		#if there is no matrix, build a simple matrix file with nx=1 and ny=1
		#if there is one matrix but many layers, use the nx and ny from the matrix
		if sum(nMt_layer) == 0 or sum(nMt_layer) == 1:
			nx = 1
			ny = 1
			phi = 0
			nz = 0
			data = ''
			#first we need to get the matrix index (iMt) used in the NPs layer (iLa)
			for iLa in range(1, prmt_c.nLa+1):
				if iMt_layer[iLa] != 0:
					get_matrix_size(matrix_c, iMt_layer[iLa])
					nx = matrix_c[iMt].nx
					ny = matrix_c[iMt].ny
					phi = matrix_c[iMt].phi

			#now we build each layer as a matrix
			for iLa in range(prmt_c.nLa, 0, -1):
				if iMt_layer[iLa] != 0:
					nz += matrix_c[iMt].nz

					#if the matrix data was not yet read, read it now
					if matrix_c[iMt_layer[iLa]].FileData.decode() == 'None':
						with open(matrix_c[iMt].FileName, 'r') as matrix_file:
							matrix_c[iMt_layer[iLa]].FileData = c_char_p(matrix_file.read().encode())

					lines = matrix_c[iMt_layer[iLa]].FileData.decode().strip().split('\n')[1:]
					for l in lines:
						data += l + '\n'

				else:
					next_nz 	= int(prmt_c.layer[iLa].thick/prmt_c.simu.dL)
					if next_nz > 0:
						nz 		+= next_nz
						data 	+= str(nx*ny*next_nz) + ' ' + str(prmt_c.layer[iLa].Cp) + '\n'

		#if there is more than one matrix, abort simulation
		else:
			print('ERROR! Only one matrix per layer can be used when trajectory simulation mode is used.')

	#first, we add a new matrix entry in config
	iMt = prmt_c.nMt+1
	config['matrix '+str(iMt)] = {}
	config['matrix '+str(iMt)]['file'] = 'virtual'
	config['matrix '+str(iMt)]['type'] = 'pM3'
	config['matrix '+str(iMt)]['nx'] = str(nx)
	config['matrix '+str(iMt)]['ny'] = str(ny)
	config['matrix '+str(iMt)]['nz'] = str(nz)
	config['matrix '+str(iMt)]['phi'] = str(phi)
	config['matrix '+str(iMt)]['data'] = str(nx)+' '+str(ny)+' '+str(nz)+' '+str(phi)+'\n' + data
	#print(str(nx)+' '+str(ny)+' '+str(nz)+' '+str(phi)+'\n' + data)

	#then, we delete the old film layers from the config file
	for iLa in range(1, prmt_c.nLa+1):
		del config['layer '+str(iLa)]

	#and add the new NP layer linked to the created matrix
	config['layer 1'] = {}
	config['layer 1']['periodic contourn'] = 'yes'
	config['layer 1']['periodic position'] = 'yes'
	config['layer 1']['%matrix '+str(iMt)] = '1'

	#finally, save the matrix index to be used in trajectory mode
	config['trajectory']['iMt'] = str(iMt)

	return config

def PREPARATION(prmt_c, matrix_c):

	#for electron beams, use electron parameters
	if prmt_c.beam.btype == 2:
		prmt_c.beam.Z = 1
		prmt_c.beam.M = 0.000548579909
		prmt_c.beam.Q = 1

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

	#traj specific
	if prmt_c.simu.algorithm == 1:
		Nmtx = matrix_c[prmt_c.straj.iMt].nx * matrix_c[prmt_c.straj.iMt].ny * matrix_c[prmt_c.straj.iMt].nz * matrix_c[prmt_c.straj.iMt].Cp_ratio

		prmt_c.straj.memory = 500
		#memory necessary to run the simulation
		var = 5
		memory_per_traj = Nmtx * var * 4 / 1e6 * 2
		N = int(prmt_c.straj.memory / memory_per_traj)
		prmt_c.straj.nIon = int(N/100)

		#for last, straj.nI gives the number of trajectories necessary to fill the matrix
		prmt_c.straj.nI 			= int(1.0*matrix_c[prmt_c.straj.iMt].nx*matrix_c[prmt_c.straj.iMt].ny*prmt_c.straj.nIon*2)		#*2 to fill the majority of slots
		prmt_c.straj.nI_per_thread 	= int(1.0*prmt_c.straj.nI/prmt_c.simu.nThreads)

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

	#calculate the number of interations for each thread
	prmt_c.simu.nI_per_thread 	= int(1.0*prmt_c.simu.nI/prmt_c.simu.nThreads)

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

	#if using TOF detector
	if prmt_c.dete.det_type == 1:
		prmt_c.calc.neutr_mode = 0

	#if using radial detector
	if prmt_c.dete.r != 0:
		prmt_c.dete.LX = 2*prmt_c.dete.r
		prmt_c.dete.LY = 2*prmt_c.dete.r

	#smallest possible angular size
	if prmt_c.dete.LY < prmt_c.dete.dY:
		prmt_c.dete.LY = prmt_c.dete.dY
	if prmt_c.dete.LX < prmt_c.dete.dX:
		prmt_c.dete.LX = prmt_c.dete.dX

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

	#if using energy spectra, change unit: keV -> eV
	if prmt_c.outp.dEnergy == 0:
		prmt_c.outp.dEnergy = 1
	if prmt_c.outp.Emax == 0:
		prmt_c.outp.Emax = prmt_c.beam.E0*1.1

	prmt_c.outp.Emin 		*= 1e3
	prmt_c.outp.Emax 		*= 1e3
	prmt_c.outp.dEnergy	 		*= 1e3
	prmt_c.outp.nEn = int((prmt_c.outp.Emax - prmt_c.outp.Emin) / prmt_c.outp.dEnergy);
	if prmt_c.outp.dAngle == 0:
		prmt_c.outp.dAngle = 1
	prmt_c.outp.nAn = int(prmt_c.dete.LY / prmt_c.outp.dAngle)+1

	prmt_c.straj.dE	 		*= 1e3

	#if using time spectra, calculate the range
	if prmt_c.outp.Tmax == 0:
		m = prmt_c.beam.M/6.02e23/1e3
		prmt_c.outp.Tmax = prmt_c.dete.d_tof * math.sqrt(m/2/max(prmt_c.outp.Emin, 1000)/1.6e-19) * 1e9
	prmt_c.outp.nTm = int((prmt_c.outp.Tmax - prmt_c.outp.Tmin) / prmt_c.outp.dTime)+1

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

	#change unit: keV -> eV
	prmt_c.beam.E0			*= 1e3
	prmt_c.calc.stopp_dE	*= 1e3
	prmt_c.calc.neutr_dE	*= 1e3

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	#change unit: eV -> a.u.
	for iCp in range(1, prmt_c.nCp+1):
		for iPl in range(1, prmt_c.nPl+1):
			prmt_c.comp[iCp].plasmon[iPl].w0 *= 1/27.211
			prmt_c.comp[iCp].plasmon[iPl].gamma *= 1/27.211
	for iEl in range(1, prmt_c.nEl+1):
		for iBS in range(1, prmt_c.nBS+1):
			prmt_c.elem[iEl].boundstate[iBS].Ib *= 1/27.211

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

	#construct the beam and detector direction vectors
	prmt_c.beam.vec.x = 0
	prmt_c.beam.vec.y = math.cos(prmt_c.beam.T1 + math.pi/2)
	prmt_c.beam.vec.z = math.sin(prmt_c.beam.T1 + math.pi/2)
	T2 = math.pi - prmt_c.beam.T1 - prmt_c.dete.Y0
	prmt_c.dete.vec.x = math.sin(T2)*math.cos(prmt_c.dete.X0 + math.pi/2)
	prmt_c.dete.vec.y = math.sin(T2)*math.sin(prmt_c.dete.X0 + math.pi/2)
	prmt_c.dete.vec.z = math.cos(T2)

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

	#change resolution: FWHM -> standard deviantion
	prmt_c.dete.sig_exp2		= pow(prmt_c.dete.sig_exp2/math.sqrt(8*math.log(2)), 2)
	prmt_c.dete.sig_exp2_TOF	= pow(prmt_c.dete.sig_exp2_TOF/math.sqrt(8*math.log(2)), 2)

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

	#normalize the element fractions
	for iCp in range(1, prmt_c.nCp+1):
		frac_total = 0
		for iEl in range(1, prmt_c.nEl+1):
			frac_total += prmt_c.comp[iCp].frac_elem[iEl]

		for iEl in range(1, prmt_c.nEl+1):
			prmt_c.comp[iCp].frac_elem[iEl] *= 1/frac_total

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

	#convert compound density: g/cm^3 -> atoms/A^3
	for iCp in range(1, prmt_c.nCp+1):
		total_comp_mass = 0
		for iEl in range(1, prmt_c.nEl+1):
			total_comp_mass += prmt_c.comp[iCp].frac_elem[iEl] * prmt_c.elem[iEl].M
		prmt_c.comp[iCp].dens = (prmt_c.comp[iCp].dens * 6.022e23 * 1e-24)/total_comp_mass;

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

	#create the detector description
	nthetax = int(prmt_c.dete.LX/prmt_c.dete.dX)+1
	nthetay = int(prmt_c.dete.LY/prmt_c.dete.dY)+1

	theta_min = math.pi
	theta_max = 0
	for i in range(0, nthetax+1):
		for j in range(0, nthetay+1):
			thetax = - prmt_c.dete.LX/2 + i*prmt_c.dete.dX
			thetay = - prmt_c.dete.LY/2 + j*prmt_c.dete.dY

			s = STvector()
			s.x = math.sin(thetax)
			s.y = math.sin(thetay)
			s.z = math.sqrt(1 - (math.sin(thetax)**2 + math.sin(thetay)**2))

			rotate(s, prmt_c.dete.vec)

			theta_scattering = math.pi - get_angle_between(s, prmt_c.beam.vec)

			if theta_scattering < theta_min: theta_min = theta_scattering
			if theta_scattering > theta_max: theta_max = theta_scattering

	prmt_c.dete.theta_min = theta_min		#+- dY to go a little further then the
	prmt_c.dete.theta_max = theta_max		#prmt_c.dete.LY size, to avoid problems

	prmt_c.dete.nthetax = nthetax+1
	prmt_c.dete.nthetay = nthetay+1

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

	#default value of dL is given by simu.dL
	for iLa in range(1, prmt_c.nLa+1):
		for i in range(3):
			if prmt_c.layer[iLa].dL[i] == 0:
				prmt_c.layer[iLa].dL[i] = prmt_c.simu.dL

	#automatically resizes the thickness of the layer to ajust with its nz
	for iMt in range(1, prmt_c.nMt+1):		#for each matrix
		for iLa in range(1, prmt_c.nLa+1):
			if prmt_c.layer[iLa].Mt_weight[iMt] != 0 and prmt_c.layer[iLa].thick < matrix_c[iMt].nz*prmt_c.layer[iLa].dL[2]:
				#first remove the old size from the sample's overall thickness
				prmt_c.thick -= prmt_c.layer[iLa].thick

				#then change the thickness of the layer
				prmt_c.layer[iLa].thick = matrix_c[iMt].nz * prmt_c.layer[iLa].dL[2]

				#for last, recontruct the overall thickness with the updated value
				prmt_c.thick += prmt_c.layer[iLa].thick

	#renormalize the matrix fractions based on matrices' surface area
	for iLa in range(1, prmt_c.nLa+1):

		#multiply by the matrix surface area: nx*ny
		for iMt in range(1, prmt_c.nMt+1):
			prmt_c.layer[iLa].Mt_weight[iMt] *= matrix_c[iMt].nx*matrix_c[iMt].ny

		#calculate the sum of the matrices fractions
		Mt_weight_sum = 0
		for iMt in range(1, prmt_c.nMt+1):
			Mt_weight_sum += prmt_c.layer[iLa].Mt_weight[iMt]

		#renormalize to 1
		if Mt_weight_sum != 0:
			for iMt in range(1, prmt_c.nMt+1):
				prmt_c.layer[iLa].Mt_weight[iMt] *= 1/Mt_weight_sum

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	#nrp section

	#change unit: keV -> eV
	prmt_c.snrp.Ei			*= 1e3
	prmt_c.snrp.Ef			*= 1e3

	for iEl in range(1, prmt_c.nEl+1):
		prmt_c.elem[iEl].Er_width		*= 1e3
		prmt_c.elem[iEl].Er_mean		*= 1e3


### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - LAB ANGLE TO CM ANGLE - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def lab_to_cm(theta_lab, m1, m2):
	theta_cm = [0, 0]
	ro = m1/m2
	gamma = ro*ro - 1
	beta_sqr = math.tan(theta_lab)*math.tan(theta_lab)

	if ro <= 1:
		theta_cm[0] = theta_lab +  math.asin(math.sin(theta_lab)*ro)
		theta_cm[1] = -1 #-1 here is the error signal
#		print(i, theta_cm * 180/math.pi, '\n')
	else:
		if beta_sqr <= 1/gamma:
			theta_cm[0] = theta_lab + math.asin(math.sin(theta_lab)*ro)
			theta_cm[1] = theta_lab + math.pi - math.asin(math.sin(theta_lab)*ro)
			#print(theta_lab * 180/math.pi, theta_cm[0] * 180/math.pi, theta_cm[1] * 180/math.pi)
		else:
			theta_cm[0] = -1
			theta_cm[1] = -1

		#if ro > 1 there can be no backscattering, so theta_lab must be lower than pi/2
		if theta_lab > math.pi/2:
			theta_cm[0] = -1
			theta_cm[1] = -1

	if theta_cm[0] >= math.pi:
		theta_cm[0] = -1

	if theta_cm[1] >= math.pi:
		theta_cm[1] = -1

	return theta_cm

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def cm_to_lab(theta_cm, m1, m2):
	ro = m1/m2

	if math.cos(theta_cm)+ro != 0:
		theta_lab = math.atan(math.sin(theta_cm)/(math.cos(theta_cm)+ro))
	else:
		theta_lab = math.pi/2

	#to avoid problems with the sign of math.atan()
	if theta_lab < 0:
		theta_lab += math.pi

	#print(theta_cm*180/math.pi, theta_lab*180/math.pi, ro)

	return theta_lab

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  OPEN TABLES  - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def OPEN_TABLES(prmt_c):

	#in PS or MS calculate for every possible angle
	if prmt_c.straj.MS == 1 or prmt_c.simu.algorithm == 2:
		theta_min = 0.00
		dtheta = math.radians(0.1)
		ntheta = int(math.pi/dtheta)
	#in SS calculate only for possible exit angles: detector parameters
	else :
		theta_min = prmt_c.dete.theta_min
		dtheta = prmt_c.dete.dY
		ntheta = prmt_c.dete.nthetay

	#neutralisation calculation needs an energy array
	nEn_beam = int(prmt_c.beam.E0/prmt_c.calc.neutr_dE)+1
	nEn_outp = int(prmt_c.outp.Emax/prmt_c.calc.neutr_dE)+1
	nEn = max(nEn_beam, nEn_outp)

	tables_c = STtables(prmt_c.nEl, prmt_c.nCp, theta_min, ntheta, dtheta, 0, prmt_c.calc.neutr_dE, nEn)

	#allocate memory for dedx_E and dwdx_E
	if prmt_c.simu.surf_apprx == 0:
		#calculates vector size for the energy loss
		nE_beam = int(prmt_c.beam.E0/prmt_c.calc.stopp_dE)+1
		nE_outp = int(prmt_c.outp.Emax/prmt_c.calc.stopp_dE)+1
		nEn = max(nE_beam, nE_outp)
		for iCp in range(1, prmt_c.nCp+1):
			if prmt_c.simu.algorithm == 2:	#ERDA simulations need dedx/dwdx calculations for each element
				tables_c.comp[iCp].allocmem_dedx_dwdx(prmt_c.nEl+1, prmt_c.calc.stopp_dE, nEn)
			else:
				tables_c.comp[iCp].allocmem_dedx_dwdx(1, prmt_c.calc.stopp_dE, nEn)

	return tables_c

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - -  KINEMATIC FACTOR - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def K_RBS(m1, m2, theta_lab):
	#following the cap. 2.2 pg. 23, from 1978 Chu's 'Backscattering Spectrometry'

	#use laboratory m1 scattering angle: theta_lab
	sqr_cte = pow(m2,2) - pow(m1*math.sin(theta_lab),2)

	if m1 <= m2:
		K = pow(( math.sqrt(sqr_cte) + m1*math.cos(theta_lab))/(m1 + m2),2)
	else:
		if sqr_cte >= 0:
			K = pow(( +math.sqrt(sqr_cte) + m1*math.cos(theta_lab))/(m1 + m2),2)
			K = pow(( -math.sqrt(sqr_cte) + m1*math.cos(theta_lab))/(m1 + m2),2)
		#return an error signal
		else:
			K = -1
	return K

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

def K_ERBS(beam_energy, m2, theta_lab):
	#use laboratory m1 scattering angle: theta_lab

	#we should use 'theta_cm = lab_to_cm(theta_lab, m1, m2)', but for electrons it isn't important
	theta_cm = theta_lab

	m2 = m2 / 6.02214e23 / 1e3

	K = 1 - 2*pow(math.sin(theta_cm/2),2) *(1.60217646E-19*beam_energy) * (1+2*0.510998910*1e6/beam_energy) / (m2*pow(299792458,2))

	return K

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

def K_ERDA(m1, m2, phi_lab):
	#use laboratory m2 scattering angle: phi_lab

	if phi_lab <= math.pi/2:
		K = 4 * m1 * m2 * pow(math.cos(phi_lab),2) / pow(m1 + m2,2)
	else:
		K = -1

	return K

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def K_CALCULATION(prmt_c, tables_c):

	for iEl in range(1, prmt_c.nEl+1):
		for iAn in range(tables_c.elem[iEl].K.size):
			theta = tables_c.elem[iEl].K.delta * iAn + tables_c.elem[iEl].K.initial

			if prmt_c.beam.btype == 2:
				tables_c.elem[iEl].K.array[iAn] = K_ERBS(prmt_c.beam.E0, prmt_c.elem[iEl].M, theta)
			elif prmt_c.simu.algorithm == 2:
				tables_c.elem[iEl].K.array[iAn] = K_ERDA(prmt_c.beam.M, prmt_c.elem[iEl].M, theta)
			else :
				tables_c.elem[iEl].K.array[iAn] = K_RBS(prmt_c.beam.M, prmt_c.elem[iEl].M, theta)

			#if K returned an error
			if tables_c.elem[iEl].K.array[iAn] != -1:
				tables_c.elem[iEl].T.array[iAn] = 1-tables_c.elem[iEl].K.array[iAn]
			else:
				tables_c.elem[iEl].T.array[iAn] = -1

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - -  CROSS SECTIONS - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def CS_RBS_cm(Z1, Z2, m1, m2, theta_cm, dtheta):
	#OBS: This is NOT the complete Rutherford cross section! This is (Rth_CS * Ein_lab^2),
	#to get the cross section just divide this value by Ein_lab^2: Rth_CS = CS_Ruth_RBS/Ein_lab^2

	#following the Chapter 2 pg. 29 and the Appendix A pg. 321, from 1978 Chu's 'Backscattering Spectrometry'
	#Ec = E/(1 + m1/m2)
	#so this cross section must be multiplied by (1+m1/m2)^2

	#use center of mass m1 scattering angle: theta_cm
	#e2 = 1.4398e-7 is the electron charge squared (eV*cm)

	if theta_cm == 0:
		theta_cm += dtheta
	elif theta_cm >= 2*math.pi:
		theta_cm -= dtheta

	CS = pow(Z1*Z2*1.4398e-7*1e8/4,2) * pow(math.sin(theta_cm/2),-4)

	return CS

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
import os
def CS_correction_cm(PATH_clib, Z1, Z2, m1, m2, q, E0, theta_min, dtheta, ntheta):
	crossR = [None] * (ntheta+1)

#	'''
	#create hash string from inputs
	string = str(Z1)+str(Z2)+str(m1)+str(m2)+str(q)+str(E0)+str(theta_min)+str(dtheta)+str(ntheta)
	hash_object = hashlib.md5(string.encode())
	hash = hash_object.hexdigest()

	# if object already exists, load its result
	dir = '/home/gmtt/ufrgs/open-pM3/cache/'
	if hash in os.listdir(dir):
		print('Found hash', hash)
		with open(dir+hash, 'r') as f:
			values = f.readlines()
			for i in range(len(values)):
				crossR[i] = float(values[i])

	# else, calculate the result and save it
	else:
#	'''
#	if True:
		#open cross c library
		clib = CDLL(PATH_clib[2])
		clib.cross_ratio_launcher.argtypes = [c_double, c_double, c_double, c_double, c_double, c_double, c_double, c_int, c_double, c_double, c_double, c_int, POINTER(c_double)]

		#energy in keV/u.m.a.
		E_per_nucleon = E0/1e3/m1

		#pointer to get values back from cross_ratio_launcher
		crossR_c = (c_double * 100000)()

		if m1 < m2:
			clib.cross_ratio_launcher(c_double(Z1), c_double(m1), c_double(Z2),	c_double(m2), c_double(q),
										c_double(E_per_nucleon), c_double(2), c_int(200), c_double(1e-5),
										c_double(theta_min), c_double(dtheta), c_int(ntheta+1), crossR_c)
			for iAn in range(0, ntheta+1):
				crossR[iAn] = crossR_c[iAn]
		else:
			for iAn in range(0, ntheta+1):
				crossR[iAn] = 1


#		'''
		# save reult in hash file
		result = []
		for iAn in range(0, ntheta+1):
			result.append('{0:.4f}'.format(crossR[iAn]))
		text = '\n'.join(result)
		with open(dir+hash, 'w') as f:
			print(text, file=f)
#		'''

	return crossR

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def CS_correction_andersen_cm(PATH_clib, Z1, Z2, m1, m2, q, E, theta_min, dtheta, ntheta):
	#This procedure is equal to CS_correction_cm but using Andersen.

	#open cross c library
	clib = CDLL(PATH_clib[2])
	clib.cross_ratio_launcher.argtypes = [c_double, c_double, c_double, c_double, c_double, c_double, c_double, c_int, c_double, c_double, c_double, c_int, POINTER(c_double)]

	#energy in keV/u.m.a.
	E_per_nucleon = E/1e3/m1

	#pointer to get values back from cross_ratio_launcher
	crossR = [1 for i in range(ntheta+1)]

	#energy in keV/u.m.a.
	E_per_nucleon = E/1e3/m1
	Ecm = E_per_nucleon*m1*m2/(m1+m2)

	a_u = [0.18175,0.50986,0.28022,0.028171]
	alpha_u = [3.1998,0.94229,0.4029, 0.20162]
	screening_length_u = 0.8853/(Z1**0.23+Z2**0.23)

	theta_cm=math.pi

	f=0
	for i in range(4):
		f = Z1*Z2/screening_length_u*a_u[i]*alpha_u[i]/(Ecm*1000/27.2)
	correction_u = (1+0.5*f)**2/(1 + f + (0.5*f/math.sin(theta_cm/2))**2)**2

#	ss_u=0
#	for i in range(4):
#		ss_u = ss_u + a_u[i]*Z1*Z2*alpha_u[i]/screening_length_u
#	ss_u  = ss_u/(Ecm*1000/27.2)
#	correction = 1/(1+ss_u)**2

	for iAn in range(0, ntheta+1):
		crossR[iAn] = correction_u

	return crossR

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def CS_CALCULATION_CM(PATH, prmt_c, tables_c):
	import time
	start_time = time.time()

	#parameters of the cm vector
	dtheta_cm = math.radians(0.1)
	ntheta_cm = int(math.pi/dtheta_cm)+1
	CS_cm = [[0 for j in range(ntheta_cm)] for i in range(prmt_c.nEl+1)]

	Emin_correc = 1e3
	dE_correc = 10e3
	Emax = tables_c.elem[prmt_c.nEl].CS_total.size*tables_c.elem[prmt_c.nEl].CS_total.delta + tables_c.elem[prmt_c.nEl].CS_total.initial
	nE_correc = int((prmt_c.beam.E0-Emin_correc)/dE_correc)+3

	correc_cm = [[[0 for k in range(ntheta_cm)] for j in range(tables_c.elem[prmt_c.nEl].CS_total.size)] for i in range(prmt_c.nEl+1)]

	'''
	with open('120.dat', 'w') as f:
		theta_cm = 120
		iAn = int(theta_cm*math.pi/180/dtheta_cm_correc)
		for iE in range(1, 101):
			E = iE*1000

			iEl = 1
			crossR = CS_correction_cm(PATH.clib, prmt_c.beam.Z, prmt_c.elem[iEl].Z, prmt_c.beam.M, prmt_c.elem[iEl].M, prmt_c.beam.Q, E, 0, dtheta_cm_correc, ntheta_cm_correc)
			Si = crossR[iAn]
			iEl = 2
			crossR = CS_correction_cm(PATH.clib, prmt_c.beam.Z, prmt_c.elem[iEl].Z, prmt_c.beam.M, prmt_c.elem[iEl].M, prmt_c.beam.Q, E, 0, dtheta_cm_correc, ntheta_cm_correc)
			Hf = crossR[iAn]

			print(E/1e3, Si, Hf, file=f)
	'''

	#Rutherford cross section: CS_Ruth
	if prmt_c.simu.cross_section == 0 or prmt_c.simu.cross_section == 1 or prmt_c.simu.cross_section == 3:
		for iEl in range(1, prmt_c.nEl+1):
			for iAn in range(ntheta_cm):
				theta_cm = iAn * dtheta_cm
				theta_lab = cm_to_lab(theta_cm, prmt_c.beam.M, prmt_c.elem[iEl].M)
				if theta_lab > 0:
					CS_cm[iEl][iAn] = CS_RBS_cm(prmt_c.beam.Z, prmt_c.elem[iEl].Z, prmt_c.beam.M, prmt_c.elem[iEl].M, theta_cm, prmt_c.dete.dX)
				else:
					CS_cm[iEl][iAn] = 0
				#print(theta_lab*180/math.pi, CS_cm[iEl][iAn])

	#Corrected cross section correction: CS_Ruth*corretion
	if prmt_c.simu.cross_section == 1:
		for iEl in range(1, prmt_c.nEl+1):
			#calculate correction for only a few energies
			crossR = [1 for i in range(nE_correc)]
			for iE_corr in range(0, nE_correc):
				E = iE_corr * dE_correc + Emin_correc
				crossR[iE_corr] = CS_correction_cm(PATH.clib, prmt_c.beam.Z, prmt_c.elem[iEl].Z, prmt_c.beam.M, prmt_c.elem[iEl].M, prmt_c.beam.Q, E, 0, dtheta_cm, ntheta_cm)
			#energy interpolation
			for iE in range(tables_c.elem[iEl].CS_total.size):
				for iAn in range(ntheta_cm):
					E = iE*tables_c.elem[iEl].CS_total.delta + tables_c.elem[iEl].CS_total.initial
					iE_corr = min(int(E/dE_correc), nE_correc)

					if iE_corr < nE_correc-1:
						slope = (crossR[iE_corr+1][iAn] - crossR[iE_corr][iAn]) / dE_correc
						inter = crossR[iE_corr][iAn] + (E - iE_corr*dE_correc) * slope
					else:
						inter = crossR[iE_corr][iAn]

					correc_cm[iEl][iE][iAn] = inter

	#Corrected cross section correction: CS_Ruth*Andersencorretion
	if prmt_c.simu.cross_section == 3:
		for iEl in range(1, prmt_c.nEl+1):
			#calculate correction for only a few energies
			crossR = [1 for i in range(nE_correc)]
			for iE in range(1, tables_c.elem[iEl].CS_total.size):
				E = iE*tables_c.elem[iEl].CS_total.delta + tables_c.elem[iEl].CS_total.initial
				correc_cm[iEl][iE] = CS_correction_andersen_cm(PATH.clib, prmt_c.beam.Z, prmt_c.elem[iEl].Z, prmt_c.beam.M, prmt_c.elem[iEl].M, prmt_c.beam.Q, E, 0, dtheta_cm, ntheta_cm)

	if prmt_c.simu.cross_section == 1 or prmt_c.simu.cross_section == 3:
		#Transform correc_cm vector to a lab vector and then interpolate the angles
		for iEl in range(1, prmt_c.nEl+1):
			for iAn in range(tables_c.elem[iEl].CS.size):
				for iE in range(1, tables_c.elem[iEl].CS_total.size):
					theta_lab = tables_c.elem[iEl].CS.delta * iAn + tables_c.elem[iEl].CS.initial
					theta_cm = lab_to_cm(theta_lab, prmt_c.beam.M, prmt_c.elem[iEl].M)
					index_cm = int(theta_cm[0]/dtheta_cm)

					#print(index_cm, ntheta_cm, theta_lab*180/math.pi)
					if theta_cm[0] != -1:
						slope = (correc_cm[iEl][iE][index_cm+1] - correc_cm[iEl][iE][index_cm]) / dtheta_cm
						inter = correc_cm[iEl][iE][index_cm] + (theta_cm[0] - index_cm*dtheta_cm) * slope
					else:
						inter = 0

					tables_c.elem[iEl].CS_correc[iAn][iE] = inter

					#if iEl == 1 and iAn == tables_c.elem[iEl].CS.size/2:
						#print(iEl, theta_lab*180/math.pi, tables_c.elem[iEl].CS_correc[iAn][iE])

	#ELSEPA cross section
	if prmt_c.simu.cross_section == 2:
		for iEl in range(1, prmt_c.nEl+1):
			CS_ELSEPA = []
			ELSEPA_file = open(prmt_c.elem[iEl].CS_file.decode(), 'r')
			lines = ELSEPA_file.readlines()
			for l in lines:
				words = l.split()
				if words[0][0] != '#':
					CS_ELSEPA.append([math.radians(float(words[0])), float(words[2])])

			for iAn in range(0, ntheta_cm):
				theta_cm = iAn * dtheta_cm

				theta_lab = cm_to_lab(theta_cm, prmt_c.beam.M, prmt_c.elem[iEl].M)
				index = 0
				for l in CS_ELSEPA:
					index += 1
					if l[0] > theta_cm or index >= len(CS_ELSEPA)-1:
						index -= 1
						break

				dangle = CS_ELSEPA[index+1][0] - CS_ELSEPA[index][0]
				slope = (CS_ELSEPA[index+1][1] - CS_ELSEPA[index][1]) / dangle
				inter = CS_ELSEPA[index][1] + (theta_cm - CS_ELSEPA[index][0]) * slope

				#here we integrate the DCS in the solid angle, considering azimuthal simmetry '2*math.pi' and
				#a delta_theta equal to the step in angle 'dtheta_cm': -(math.cos(theta_final) - math.cos(theta_inicial))
				#the 1e16 is the conversion from cm^2 to A^2
				CS_cm[iEl][iAn] = inter * 1e16

				#here we multiple by E0^2, to later divide by the same amount
				CS_cm[iEl][iAn] *= prmt_c.beam.E0*prmt_c.beam.E0

				#print(theta_cm*180/math.pi, CS_cm[iEl][iAn])

	#Transform the cm vector to a lab vector and then interpolate the angles
	inter = [0, 0]
	for iEl in range(1, prmt_c.nEl+1):
		m1_m2 = prmt_c.beam.M/prmt_c.elem[iEl].M
		for iAn in range(tables_c.elem[iEl].CS.size):
			theta_lab = tables_c.elem[iEl].CS.delta * iAn + tables_c.elem[iEl].CS.initial
			theta_cm = lab_to_cm(theta_lab, prmt_c.beam.M, prmt_c.elem[iEl].M)

			inter = [0, 0]
			index_cm = [int(theta_cm[0]/dtheta_cm), int(theta_cm[1]/dtheta_cm)]

			#for some conditions there is no theta_cm, in this case the function lab_to_cm()
			#return an error signal '-1' that we filter here
			if theta_cm[0] != -1 and index_cm[0]+1 < ntheta_cm:
				slope = (CS_cm[iEl][index_cm[0]+1] - CS_cm[iEl][index_cm[0]]) / dtheta_cm
				inter[0] = CS_cm[iEl][index_cm[0]] + (theta_cm[0] - index_cm[0]*dtheta_cm) * slope

			if theta_cm[1] != -1:
				slope = (CS_cm[iEl][index_cm[1]+1] - CS_cm[iEl][index_cm[1]]) / dtheta_cm
				inter[1] = CS_cm[iEl][index_cm[1]] + (theta_cm[1] - index_cm[1]*dtheta_cm) * slope
			else:
				inter[1] = 0

			# to avoid negative values inside the sqrt()
			if (m1_m2*math.sin(theta_lab))**2 < 1:
				sqrt_m1_m2 = math.sqrt( 1 - (m1_m2*math.sin(theta_lab))**2 )
				tables_c.elem[iEl].CS.array[iAn] = (inter[0] + inter[1]) * (m1_m2 * math.cos(theta_lab) +  sqrt_m1_m2)**2 / sqrt_m1_m2 * (1 + m1_m2)**2
			else:
				tables_c.elem[iEl].CS.array[iAn] = 0

	if prmt_c.straj.CS_cut_algo == 2:
		beta = 1/prmt_c.straj.angle_min
		for iAn in range(tables_c.elem[iEl].CS.size):
			theta_lab = tables_c.elem[iEl].CS.delta * iAn + tables_c.elem[iEl].CS.initial
			tables_c.elem[iEl].CS.array[iAn] *= 1/(1 + math.exp( beta * (prmt_c.straj.angle_min - theta_lab)))

	#total cross section for a colision in this element
	if prmt_c.simu.algorithm == 1:
		for iEl in range(1, prmt_c.nEl+1):
			for iAn in range(tables_c.elem[iEl].CS.size):
				theta_lab = tables_c.elem[iEl].CS.delta * iAn + tables_c.elem[iEl].CS.initial
				angular_factor = 2*math.pi*(math.cos(theta_lab)-math.cos(theta_lab+ tables_c.elem[iEl].CS.delta))

				for iEn in range(1, tables_c.elem[iEl].CS_total.size):
					E = iEn*tables_c.elem[iEl].CS_total.delta + tables_c.elem[iEl].CS_total.initial

					tables_c.elem[iEl].CS_total.array[iEn] += tables_c.elem[iEl].CS.array[iAn]*tables_c.elem[iEl].CS_correc[iAn][iEn]/E/E * angular_factor
					if theta_lab > prmt_c.straj.angle_min:
						tables_c.elem[iEl].CS_angle_cut.array[iEn] += tables_c.elem[iEl].CS.array[iAn]*tables_c.elem[iEl].CS_correc[iAn][iEn]/E/E * angular_factor

						# accumulate the CS_total until certain angle
						tables_c.elem[iEl].CS_accumulation[iAn][iEn] = tables_c.elem[iEl].CS_angle_cut.array[iEn]

			if(prmt_c.debug == 1):
				print(iEl, tables_c.elem[iEl].CS_total.array[1], tables_c.elem[iEl].CS_angle_cut.array[1])

	end_time = time.time()
	print('CS total time:', end_time-start_time)
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - -  STOPPING POWER - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def STOPPING_POWER_CALCULATION(PATH, prmt_c, tables_c):

	#if stopping calculation is off, return doing nothing
	if prmt_c.calc.stopp_mode == 0 or prmt_c.calc.dielec_mode != 0:
		return

	BIN_PATH_c = c_char_p(str(PATH.binary).encode())

	#open SRIM c library
	clib = CDLL(PATH.clib[1])

	clib.STOP96_laucher.argtypes = [c_int, c_float, c_int, c_float, c_int, c_char_p]
	clib.STOP96_laucher.restype = c_float

	clib.STOP96_inside_C.argtypes = [POINTER(STprmt), POINTER(STtables), c_int, c_char_p]
	clib.STOP96_inside_C.retypes = c_int

	#If SOLIDGAS=0, then stopping in solids, SOLIDGAS=1 for gases.
	SOLIDGAS = 0

	#particle properties - beam ions as default
	particle_M = prmt_c.beam.M
	particle_Z = prmt_c.beam.Z

	#for iEn in range(1, 1000):
	#	SE = 1/3 * clib.STOP96_laucher(1, 1, 72, iEn, SOLIDGAS, BIN_PATH_c) + 2/3 * clib.STOP96_laucher(1, 1, 8, iEn, SOLIDGAS, BIN_PATH_c)
	#	print(iEn, SE / 10)
	#exit()

	for iCp in range(1, prmt_c.nCp+1):
		#for beam energy
		if prmt_c.comp[iCp].dedx[0] == 0:
			for iEl in range(1, prmt_c.nEl+1):
				if prmt_c.calc.stopp_mode == 1:
					SE = clib.STOP96_laucher(prmt_c.beam.Z, prmt_c.beam.M, prmt_c.elem[iEl].Z, prmt_c.beam.E0/1e3, SOLIDGAS, BIN_PATH_c)

				prmt_c.comp[iCp].dedx[0] += SE * prmt_c.comp[iCp].frac_elem[iEl]

			prmt_c.comp[iCp].dedx[0] *= prmt_c.comp[iCp].dens * prmt_c.comp[iCp].dEdx_corr

		if prmt_c.debug == 1:
			print(iCp, 0, prmt_c.comp[iCp].dedx[0])

		#after the ion collision with elem iEl_col
		for iEl_col in range(1, prmt_c.nEl+1):
			#calculates stopping for the center of the detector
			iAn = int((prmt_c.dete.Y0 - tables_c.elem[iEl_col].K.initial)/tables_c.elem[iEl_col].K.delta)

			if prmt_c.comp[iCp].dedx[iEl_col] == 0:
				if prmt_c.simu.algorithm == 2:
					particle_M = prmt_c.elem[iEl_col].M
					particle_Z = prmt_c.elem[iEl_col].Z

				for iEl in range(1, prmt_c.nEl+1):
					if prmt_c.calc.stopp_mode == 1:
						SE = clib.STOP96_laucher(particle_Z, particle_M, prmt_c.elem[iEl].Z, prmt_c.beam.E0/1e3*tables_c.elem[iEl_col].K.array[iAn], SOLIDGAS, BIN_PATH_c)
					prmt_c.comp[iCp].dedx[iEl_col] += SE * prmt_c.comp[iCp].frac_elem[iEl]

				prmt_c.comp[iCp].dedx[iEl_col] *= prmt_c.comp[iCp].dens * prmt_c.comp[iCp].dEdx_corr

			if prmt_c.debug == 1:
				print(iCp, iEl_col, prmt_c.comp[iCp].dedx[iEl_col])

	#in the case of NO surface approximation, calculate dEdx for many energies
	if prmt_c.simu.surf_apprx == 0:
		clib.STOP96_inside_C(byref(prmt_c), byref(tables_c), SOLIDGAS, BIN_PATH_c)

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  STRAGGLING - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def STRAGG_CALCULATION(prmt_c, tables_c):

	if prmt_c.calc.strag_mode == 0 or prmt_c.calc.dielec_mode != 0:
		return

	#particle properties - beam ions as default
	particle_M = prmt_c.beam.M
	particle_Z = prmt_c.beam.Z

	for iCp in range(1, prmt_c.nCp+1):
		#for beam energy
		if prmt_c.comp[iCp].dwdx[0] == 0:
			for iEl in range(1, prmt_c.nEl+1):
				if prmt_c.calc.strag_mode == 1:
					SS = SBohr(prmt_c.beam.Z, prmt_c.elem[iEl].Z, prmt_c.comp[iCp].dens)
				if prmt_c.calc.strag_mode == 2:
					SS = SLind(prmt_c.beam.Z, prmt_c.elem[iEl].Z, prmt_c.comp[iCp].dens,prmt_c.beam.E0/1e3/prmt_c.beam.M)
				if prmt_c.calc.strag_mode == 3:
					SS = SChu(prmt_c.beam.Z, prmt_c.elem[iEl].Z, prmt_c.comp[iCp].dens, prmt_c.beam.E0/1e6/prmt_c.beam.M)
				if prmt_c.calc.strag_mode == 4:
					SS = SYang(prmt_c.beam.Z, prmt_c.elem[iEl].Z, prmt_c.beam.E0/1e6/prmt_c.beam.M, prmt_c.comp[iCp].dens)

				prmt_c.comp[iCp].dwdx[0] += SS * prmt_c.comp[iCp].frac_elem[iEl] * prmt_c.comp[iCp].dwdx_corr

		if prmt_c.debug == 1:
			print(iCp, 0, prmt_c.comp[iCp].dwdx[0])

		#after the ion collision with elem iEl
		for iEl_col in range(1, prmt_c.nEl+1):
			#calculates stopping for the center of the detector
			iAn = int((prmt_c.dete.Y0 - tables_c.elem[iEl_col].K.initial)/tables_c.elem[iEl_col].K.delta)

			if prmt_c.comp[iCp].dwdx[iEl_col] == 0:
				#if getting straggling for erda, use the element as the ion traveling
				if prmt_c.simu.algorithm == 2:
					particle_M = prmt_c.elem[iEl_col].M
					particle_Z = prmt_c.elem[iEl_col].Z

				for iEl in range(1, prmt_c.nEl+1):
					if prmt_c.calc.strag_mode == 1:
						SS = SBohr(particle_Z, prmt_c.elem[iEl].Z, prmt_c.comp[iCp].dens)
					if prmt_c.calc.strag_mode == 2:
						SS = SLind(particle_Z, prmt_c.elem[iEl].Z, prmt_c.comp[iCp].dens, prmt_c.beam.E0/1e3*tables_c.elem[iEl_col].K.array[iAn]/particle_M)
					if prmt_c.calc.strag_mode == 3:
						SS = SChu(particle_Z, prmt_c.elem[iEl].Z, prmt_c.comp[iCp].dens, prmt_c.beam.E0/1e6*tables_c.elem[iEl_col].K.array[iAn]/particle_M)
					if prmt_c.calc.strag_mode == 4:
						SS = SYang(particle_Z, prmt_c.elem[iEl].Z, prmt_c.beam.E0/1e6*tables_c.elem[iEl_col].K.array[iAn]/particle_M, prmt_c.comp[iCp].dens)
					prmt_c.comp[iCp].dwdx[iEl_col] += SS * prmt_c.comp[iCp].frac_elem[iEl] * prmt_c.comp[iCp].dwdx_corr

			if prmt_c.debug == 1:
				print(iCp, iEl_col, prmt_c.comp[iCp].dwdx[iEl_col])

	#in the case of NO surface approximation, calculate dWdx for many energies
	if prmt_c.simu.surf_apprx == 0:
		#calculates vector size for the energy loss
		nE_beam = int(prmt_c.beam.E0/prmt_c.calc.stopp_dE)+1
		nE_outp = int(prmt_c.outp.Emax/prmt_c.calc.stopp_dE)+1
		nE = max(nE_beam, nE_outp)

		if prmt_c.simu.algorithm == 2:	#ERDA simulations need dedx/dwdx calculations for each element
			nEl_proj = prmt_c.nEl+1
		else:
			nEl_proj = 1

		for iCp in range(1, prmt_c.nCp+1):
			#for beam energy
			for iE in range(1, nE):
				E = iE*prmt_c.calc.stopp_dE

				#iEl = 0; Energy stopping force for the beam element
				#iEl > 0; Energy stopping force for the element knocked off by the collision - ERDA mode
				for iEl in range(0, nEl_proj):
					#iEl = 0; Energy stopping force for the beam element
					if iEl==0:
						Z = prmt_c.beam.Z
						M = prmt_c.beam.M
					#iEl > 0; Energy stopping force for the element knocked off by the collision - ERDA mode
					if iEl>0:
						Z = prmt_c.elem[iEl].Z
						M = prmt_c.elem[iEl].M
					for jEl in range(1, prmt_c.nEl+1):
						if prmt_c.calc.strag_mode == 1:
							SS = SBohr(Z, prmt_c.elem[jEl].Z, prmt_c.comp[iCp].dens)
						if prmt_c.calc.strag_mode == 2:
							SS = SLind(Z, prmt_c.elem[jEl].Z, prmt_c.comp[iCp].dens, E/1e3/M)
						if prmt_c.calc.strag_mode == 3:
							SS = SChu(Z, prmt_c.elem[jEl].Z, prmt_c.comp[iCp].dens, E/1e6/M)
						if prmt_c.calc.strag_mode == 4:
							SS = SYang(Z, prmt_c.elem[jEl].Z, E/1e6/M, prmt_c.comp[iCp].dens)

						tables_c.comp[iCp].dwdx_E[iEl].array[iE] += SS * prmt_c.comp[iCp].frac_elem[jEl] * prmt_c.comp[iCp].dwdx_corr

				if prmt_c.debug == 1:
					print(iCp, iE, tables_c.comp[iCp].dwdx_E[0].array[iE])

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

#Z1 -- projectile atomic number
#sZ2 -- sum of the w(i)Z(i) of the elements in the compound, where w(i) is the i-element normalized fraction
#N -- density of the compound in at/cm3

#Calculates Bohr's straggling
def SBohr(Z1, sZ2, N):
	return 4*math.pi*pow(Z1*e2,2)*N*sZ2*1e16	#ev^2/angstrom

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

#Calculates Lindhard-Scharff's straggling
def SLind(Z1, sZ2, N, E):
	if E <= 0:
		return 0

	x = E/(25*sZ2)
	if x <= 3:
		L = 1.36*math.sqrt(x) - 0.016*pow(x,3/2);
		return 0.5*L*SBohr(Z1, sZ2, N)		#ev^2/angstrom
	else:
		return SBohr(Z1, sZ2, N)

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

#Calculates Chu's straggling
def SChu(Z1, sZ2, N, E):
	#E in units of MeV/amu
	if E <= 0:
		return 0
	Z = int(sZ2)
	A1 = 0; A2 = 1; A3 = 0; A4 = 1;
	if Z == 2:
		A1 = -0.3291; A2 = - 0.8312; A3 = 0.2460; A4 = -1.0220;
	if Z == 3:
		A1 = -0.5615; A2 = -0.5898; A3 = 0.5205; A4 = -0.7258;
	if Z == 4:
		A1 = -0.5280; A2 = -0.4981; A3 = 0.5519; A4 = -0.5865;
	if Z == 5:
		A1 = -0.5125; A2 = -0.4625; A3 = 0.5660; A4 = -0.5190;
	if Z == 6:
		A1 = -0.5127; A2 = -0.8595; A3 = 0.5626; A4 = -0.8721;
	if Z == 7:
		A1 = -0.5174; A2 = -1.1930; A3 = 0.5565; A4 = -1.1980;
	if Z == 8:
		A1= -0.5179; A2 = -1.1850; A3 = 0.5560; A4 = -1.2070;
	if Z == 9:
		A1 = -0.5209; A2 = -0.9355; A3 = 0.5590; A4 = -1.0250;
	if Z == 10:
		A1 = -0.5255; A2 = -0.7766; A3 = 0.5720; A4 = -0.9412;
	if Z ==11:
		A1 = -0.5776; A2 = -0.6665; A3 = 0.6598; A4 = -0.8484;
	if Z == 12:
		A1 = -0.6013; A2 = -0.6045; A3 = 0.7321; A4 = -0.7671;
	if Z == 13:
		A1 = -0.5781; A2 = -0.5518; A3 = 0.7605; A4 = -0.6919;
	if Z == 14:
		A1 = -0.5587; A2 = -0.4981; A3 = 0.7835; A4 = -0.6195;
	if Z == 15:
		A1 = -0.5466; A2 = -0.4656; A3 = 0.7978; A4 = -0.5771;
	if Z == 16:
		A1 = -0.5406; A2 = -0.4690; A3 = 0.8031; A4 = -0.5718;
	if Z == 17:
		A1 = -0.5391; A2 = -0.5061; A3 = 0.8024; A4 = -0.5974;
	if Z == 18:
		A1 = -0.5380; A2 = -0.6483; A3 = 0.7962; A4 = -0.6970;
	if Z == 19:
		A1 = -0.5355; A2 = -0.7722; A3 = 0.7962; A4 = -0.7839;
	if Z == 20:
		A1 = -0.5329; A2 = -0.7720; A3 = 0.7988; A4 = -0.7846;
	if Z == 21:
		A1 = -0.5335; A2 = -0.7671; A3 = 0.7984; A4 = -0.7933;
	if Z == 22:
		A1 = -0.5324; A2 = -0.7612; A3 = 0.7998; A4 = -0.8031;
	if Z == 23:
		A1 = -0.5305; A2 = -0.7300; A3 = 0.8031; A4 = -0.7990;
	if Z == 24:
		A1 = -0.5307; A2 = -0.7178; A3 = 0.8049; A4 = -0.8216;
	if Z == 25:
		A1 = -0.5248; A2 = -0.6621; A3 = 0.8165; A4 = -0.7919;
	if Z == 26:
		A1 = -0.5180; A2 = -0.6502; A3 = 0.8266; A4 = -0.7986;
	if Z == 27:
		A1 = -0.5084; A2 = -0.6408; A3 = 0.8396; A4 = -0.8048;
	if Z == 28:
		A1 = -0.4967; A2 = -0.6331; A3 = 0.8549; A4 = -0.8093;
	if Z == 29:
		A1 = -0.4861; A2 = -0.6508; A3 = 0.8712; A4 = -0.8432;
	if Z == 30:
		A1 = -0.4700; A2 = -0.6186; A3 = 0.8961; A4 = -0.8132;
	if Z == 31:
		A1 = -0.4545; A2 = -0.5720; A3 = 0.9227; A4 = -0.7710;
	if Z == 32:
		A1 = -0.4404; A2 = -0.5226; A3 = 0.9481; A4 = -0.7254;
	if Z == 33:
		A1 = -0.4288; A2 = -0.4778; A3 = 0.9701; A4 = -0.6850;
	if Z == 34:
		A1 = -0.4199; A2 = -0.4425; A3 = 0.9874; A4 = -0.6539;
	if Z == 35:
		A1 = -0.4131; A2 = -0.4188; A3 = 0.9998; A4 = -0.6332;
	if Z == 36:
		A1 = -0.4089; A2 = -0.4057; A3 = 1.0070; A4 = -0.6218;
	if Z == 37:
		A1 = -0.4039; A2 = -0.3913; A3 = 1.0150; A4 = -0.6107;
	if Z == 38:
		A1 = -0.3987; A2 = -0.3698; A3 = 1.0240; A4 = -0.5938;
	if Z == 39:
		A1 = -0.3977; A2 = -0.3608; A3 = 1.0260; A4 = -0.5852;
	if Z == 40:
		A1 = -0.3972; A2 = -0.3600; A3 = 1.0260; A4 = -0.5842;
	if Z == 41:
		A1 = -0.3985; A2 = -0.3803; A3 = 1.0200; A4 = -0.6013;
	if Z == 42:
		A1 = -0.3985; A2 = -0.3979; A3 = 1.0150; A4 = -0.6168;
	if Z == 43:
		A1 = -0.3968; A2 = -0.3990; A3 = 1.0160; A4 = -0.6195;
	if Z == 44:
		A1 = -0.3971; A2 = -0.4432; A3 = 1.0050; A4 = -0.6591;
	if Z == 45:
		A1 = -0.3944; A2 = -0.4665; A3 = 1.0010; A4 = -0.6825;
	if Z == 46:
		A1 = -0.3924; A2 = -0.5109; A3 = 0.9921; A4 = -0.7235;
	if Z == 47:
		A1 = -0.3882; A2 = -0.5158; A3 = 0.9947; A4 = -0.7343;
	if Z == 48:
		A1 = -0.3838; A2 = -0.5125; A3 = 0.9999; A4 = -0.7370;
	if Z == 49:
		A1 = -0.3786; A2 = -0.4976; A3 = 1.0090; A4 = -0.7310;
	if Z == 50:
		A1 = -0.3741; A2 = -0.4738; A3 = 1.0200; A4 = -0.7155;
	if Z == 51:
		A1 = -0.3969; A2 = -0.4496; A3 = 1.0320; A4 = -0.6982;
	if Z == 52:
		A1 = -0.3663; A2 = -0.4297; A3 = 1.0430; A4 = -0.6828;
	if Z == 53:
		A1 = -0.3630; A2 = -0.4120; A3 = 1.0530; A4 = -0.6689;
	if Z == 54:
		A1 = -0.3595; A2 = -0.3964; A3 = 1.0620; A4 = -0.6564;
	if Z == 55:
		A1 = -0.3555; A2 = -0.3809; A3 = 1.0720; A4 = -0.6454;
	if Z == 56:
		A1 = -0.3525; A2 = -0.3607; A3 = 1.0820; A4 = -0.6289;
	if Z == 57:
		A1 = -0.3505; A2 = -0.3465; A3 = 1.0900; A4 = -0.6171;
	if Z == 58:
		A1 = -0.3397; A2 = -0.3570; A3 = 1.1020; A4 = -0.6384;
	if Z == 59:
		A1 = -0.3314; A2 = -0.3552; A3 = 1.1130; A4 = -0.6441;
	if Z == 60:
		A1 = -0.3235; A2 = -0.3531; A3 = 1.1230; A4 = -0.6498;
	if Z == 61:
		A1 = -0.3150; A2 = -0.3483; A3 = 1.1360; A4 = -0.6539;
	if Z == 62:
		A1 = -0.3060; A2 = -0.3441; A3 = 1.1490; A4 = -0.6593;
	if Z == 63:
		A1 = -0.2968; A2 = -0.3396; A3 = 1.1630; A4 = -0.6649;
	if Z == 64:
		A1 = -0.2935; A2 = -0.3225; A3 = 1.1760; A4 = -0.6527;
	if Z == 65:
		A1 = -0.2797; A2 = -0.3262; A3 = 1.1940; A4 = -0.6722;
	if Z == 66:
		A1 = -0.2704; A2 = -0.3202; A3 = 1.2100; A4 = -0.6770;
	if Z == 67:
		A1 = -0.2815; A2 = -0.3227; A3 = 1.2480; A4 = -0.6775;
	if Z == 68:
		A1 = -0.2880; A2 = -0.3245; A3 = 1.2810; A4 = -0.6801;
	if Z == 69:
		A1 = -0.3034; A2 = -0.3263; A3 = 1.3270; A4 = -0.6778;
	if Z == 70:
		A1 = -0.2936; A2 = -0.3215; A3 = 1.3430; A4 = -0.6835;
	if Z == 71:
		A1 = -0.3282; A2 = -0.3200; A3 = 1.3980; A4 = -0.6650;
	if Z == 72:
		A1 = -0.3260; A2 = -0.3070; A3 = 1.4090; A4 = -0.6552;
	if Z == 73:
		A1 = -0.3511; A2 = -0.3074; A3 = 1.4470; A4 = -0.6442;
	if Z == 74:
		A1 = -0.3501; A2 = -0.3064; A3 = 1.4500; A4 = -0.6442;
	if Z == 75:
		A1 = -0.3490; A2 = -0.3027; A3 = 1.4550; A4 = -0.6418;
	if Z == 76:
		A1 = -0.3487; A2 = -0.3048; A3 = 1.4570; A4 = -0.6447;
	if Z == 77:
		A1 = -0.3478; A2 = -0.3074; A3 = 1.4600; A4 = -0.6483;
	if Z == 78:
		A1 = -0.3501; A2 = -0.3283; A3 = 1.4540; A4 = -0.6669;
	if Z == 79:
		A1 = -0.3494; A2 = -0.3373; A3 = 1.4550; A4 = -0.6765;
	if Z == 80:
		A1 = -0.3485; A2 = -0.3373;  A3 = 1.4560; A4 = -0.6774;
	if Z == 81:
		A1 = -0.3462; A2 = -0.3300; A3 = 1.4630; A4 = -0.6728;
	if Z == 82:
		A1 = -0.3462; A2 = -0.3225; A3 = 1.4690; A4 = -0.6662;
	if Z == 83:
		A1 = -0.3453; A2 = -0.3094; A3 = 1.4790; A4 = -0.6553;
	if Z == 84:
		A1 = -0.3844; A2 = -0.3134; A3 = 1.5240; A4 = -0.6412;
	if Z == 85:
		A1 = -0.3848; A2 = -0.3018; A3 = 1.3510; A4 = -0.6303;
	if Z == 86:
		A1 = -0.3862; A2 = -0.2955; A3 = 1.5360; A4 = -0.6237;
	if Z == 87:
		A1 = -0.4262; A2 = -0.2991; A3 = 1.5860; A4 = -0.6115;
	if Z == 88:
		A1 = -0.4278; A2 = -0.2910; A3 = 1.5900; A4 = -0.6029;
	if Z == 89:
		A1 = -0.4303; A2 = -0.2817; A3 = 1.5940; A4 = -0.5927;
	if Z == 90:
		A1 = -0.4315; A2 = -0.2719; A3 = 1.6010; A4 = -0.5829;
	if Z == 91:
		A1 = -0.4359; A2 = -0.2914; A3 = 1.6050; A4 = -0.6010;
	if Z == 92:
		A1 = -0.4365; A2 = -0.2982; A3 = 1.6080; A4 = -0.6080;
	if Z == 93:
		A1 = -0.4253; A2 = -0.3037; A3 = 1.6120; A4 = -0.6377;
	if Z == 94:
		A1 = -0.4335; A2 = -0.3245; A3 = 1.6160; A4 = -0.6377;
	if Z == 95:
		A1 = -0.4307; A2 = -0.3292; A3 = 1.6210; A4 = -0.6447;
	if Z == 96:
		A1 = -0.4284; A2 = -0.3204; A3 = 1.6290; A4 = -0.6380;
	if Z == 97:
		A1 = -0.4227; A2 = -0.3217; A3 = 1.6360; A4 = -0.6438;

	return SBohr(Z1, sZ2, N)/(1 + A1*pow(E,A2) + A3*pow(E,A4))

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

#Calculates Yang's straggling

def SYang(Z1, sZ2, E, N):

#Parameters for solid target

	B1 = 0.1955; B2 = 0.6941; B3 = 2.522; B4 = -1.040;
	C1 = 1.273e-2; C2 = 3.458e-2; C3 = 0.3931; C4 = -3.812;

	if E <= 0:
		return 0
	if Z1 == 1:
		gamma = B3*(1-math.exp(B4*E))
		return SBohr(Z1, sZ2, N)*((B1*gamma)/(pow(E-B2,2) + pow(gamma,2)))
	if Z1 == 2:
		Xi = E/(pow(Z1,3/2)*pow(sZ2,1/2))        #Solid target
		GAMMA = C3*(1-math.exp(C4*Xi))
		return SBohr(Z1, sZ2, N)*((pow(Z1,4/3)*C1*GAMMA)/(pow(Xi-C2,2) + pow(GAMMA,2)))


### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def CHARGE_FRACTION_CALCULATION(PATH, prmt_c, tables_c):

	'''
	nEn_beam = int(prmt_c.beam.E0/prmt_c.calc.neutr_dE)+1
	nEn_outp = int(prmt_c.outp.Emax/prmt_c.calc.neutr_dE)+1
	nEn = max(nEn_beam, nEn_outp)
	'''

	if prmt_c.calc.neutr_mode == 0:
		for iCp in range(1, prmt_c.nCp+1):
			for iEn in range(tables_c.comp[iCp].neutr.size):
				tables_c.comp[iCp].neutr.array[iEn] = 1
		return

	if prmt_c.calc.neutr_mode == 1:
		for iCp in range(1, prmt_c.nCp+1):
			for iEn in range(tables_c.comp[iCp].neutr.size):
				E = iEn * tables_c.comp[iCp].neutr.delta
				if prmt_c.beam.M < 4:
					if E/1000 > 10.2087:
						F = 0.17442*pow((E/1000 - 10.2087),1.0/3)
					else:
						F = 0
				else:
					if E/1000 > 12.3388:
						F = 0.02045*pow((E/1000 - 12.3388),2.0/3)
					else:
						F = 0

				# F must be inside [0, 1]
				F = max(F, 0)
				F = min(F, 1)

				tables_c.comp[iCp].neutr.array[iEn] = F
				#if prmt_c.debug == 1:
				#print(iCp, E/1000, F)

	if prmt_c.calc.neutr_mode == 2:
		#open pM3 c++ library
		clib = CDLL(PATH.clib[3])

		clib.CASP_charge_laucher.restype = c_double
		frac = clib.CASP_charge_laucher(c_double(2), c_double(1), c_double(200), c_double(14), c_bool(True), c_bool(False))

		clib.CASP_charge_inside_C.argtypes = [POINTER(STprmt), POINTER(STtables), c_bool, c_bool]
		clib.CASP_charge_inside_C.restype = c_int
		clib.CASP_charge_inside_C(byref(prmt_c), byref(tables_c), c_bool(True), c_bool(False))

	if prmt_c.calc.neutr_mode == 3:
		for iCp in range(1, prmt_c.nCp+1):
			for iEn in range(tables_c.comp[iCp].neutr.size):
				if prmt_c.beam.M < 4:
					alpha = 0.019 #1/keV
					F = 1 - math.exp(-alpha*iEn)
				if prmt_c.beam.M >= 4:
					alpha = 0.0064 #1/keV
					F = 1 - math.exp(-alpha*iEn)
				tables_c.comp[iCp].neutr.array[iEn] = F
		return

	if prmt_c.calc.neutr_mode == 4:
		for iCp in range(1, prmt_c.nCp+1):
			for iEn in range(tables_c.comp[iCp].neutr.size):
				if prmt_c.beam.M < 4:
					alpha = 0.019 #1/keV
					F = 1 - math.exp(-alpha*iEn)
				if prmt_c.beam.M >= 4:
					alpha = 0.0064 #1/keV
					F = 1 - math.exp(-alpha*iEn)
				tables_c.comp[iCp].neutr.array[iEn] = F
		return

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def DIELECTRIC_FUNCTION(PATH, prmt_c, tables_c):
	if prmt_c.calc.dielec_mode == 0:
		return

	for iCp in range(1, prmt_c.nCp+1):
		Ne_plasmon = 0
		for iPl in range(1, prmt_c.nPl+1):
			Ne_plasmon += prmt_c.comp[iCp].plasmon[iPl].A * pow(prmt_c.comp[iCp].plasmon[iPl].w0, 2)
		Ne_plasmon *= 1/(4*math.pi) * 1/(prmt_c.comp[iCp].dens*pow(0.529, 3))

		min_frac = 1
		for iEl in range(1, prmt_c.nEl+1):
			if prmt_c.comp[iCp].frac_elem[iEl] > 0 and prmt_c.comp[iCp].frac_elem[iEl] < min_frac:
				min_frac = prmt_c.comp[iCp].frac_elem[iEl]

		Ne_bstate = 0
		for iEl in range(1, prmt_c.nEl+1):
			for iBS in range(1, prmt_c.nBS+1):
				Ne_bstate += prmt_c.elem[iEl].boundstate[iBS].Ne * prmt_c.comp[iCp].frac_elem[iEl] / min_frac

		print(prmt_c.comp[iCp].name.decode(), '\tNe plasmon:', Ne_plasmon, '\tNe boundstate:', Ne_bstate)

	#open pM3 c++ library
	clib = CDLL(PATH.clib[4])
	clib.dielectric_inside_C.argtypes = [POINTER(STprmt), POINTER(STtables), c_int, c_int]
	clib.dielectric_inside_C.restype = c_int

	me = 0.000548579909
	M_beam = prmt_c.beam.M/me
	E0_beam = prmt_c.beam.E0/27.211

	v0 = math.sqrt(2*E0_beam/M_beam)

	#prmt_c.calc.dielec_qmax = 2*(v0+1)
	#prmt_c.calc.dielec_wmax = prmt_c.calc.dielec_qmax*v0 * 5 * 27.211
	qmax = math.sqrt(2*E0_beam*M_beam) + math.sqrt(2*(E0_beam-prmt_c.calc.dielec_wmax)*M_beam)

	#print(v0, prmt_c.calc.dielec_qmax, 2*(v0+1), qmax)
	nq = 1000
	nw = 1000

	#prmt_c.calc.dielec_dq = prmt_c.calc.dielec_qmax / nq
	prmt_c.calc.dielec_dq = prmt_c.calc.dielec_qmax / nq / nq
	prmt_c.calc.dielec_dw = prmt_c.calc.dielec_wmax/27.211 / nw

	#E_range = prmt_c.outp.Emax - prmt_c.outp.Emin
	E_range = prmt_c.beam.E0 - prmt_c.outp.Emin
	nE = int(E_range/prmt_c.calc.dielec_dE)+1

	#w_max = (prmt_c.outp.Emax - prmt_c.outp.Emin)/27.211
	#nw = int(w_max/prmt_c.calc.dielec_dw)+1
	#nq = int(q_max/prmt_c.calc.dielec_dq)+1
	#print(nE, nw, nq)

	for iCp in range(1, prmt_c.nCp+1):
		tables_c.comp[iCp].allocate_dielectric_vectors(prmt_c.outp.Emin, prmt_c.calc.dielec_dE, nE, nw, nq)

	clib.dielectric_inside_C(byref(prmt_c), byref(tables_c), nw, nq)
#	sys.exit()

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  VEGAS INPUT  - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def READ_VEGAS_INPUT(prmt_c, tables_c):
	iCp = 1

	if prmt_c.comp[iCp].VEGAS_file.decode() == '':
		return

	visibility = []
	angles = []

	allocate_visibility_vectors(theta_min, dtheta, ntheta, nCrystals)
	VEGAS_file = open(prmt_c.comp[iCp].VEGAS_file.decode(), 'r')
	lines = VEGAS_file.readlines()
	for l in lines:
		words = l.strip().split()
		if words[0] == '#' and len(words) > 1:
			if words[1] == 'lattice':
				lattice = float(words[2])

		if words[0] != '#':
			visibility.append([])
			allocate_visibility_vectors(self, theta_min, dtheta, ntheta, nCrystals)
			angles.append(float(words[0]))
			for iLayer in range(1, len(words)):
				visibility[-1].append(float(words[iLayer]))

	#Calculate the dx and dy values
	dphi = []
	dtheta = []
	theta_max = max(angles)
	theta_min = min(angles)
	bin_size = angles[1] - angles[0]
	ntheta = int((theta_max - theta_min)/bin_size)+1

	os.exit()

	for iphi in range(phi_size):
		delta_phi = iphi*2.725
		for itheta in range(ntheta):
			theta_scatt = (itheta*bin_size + theta_min)
			dphi.append(delta_phi)
			dtheta.append(theta_scatt)


	#	print the final result
	with open('output_exp.out', 'w') as f:
		for i in range(len(dtheta)):
	#		print(i)
			print(dtheta[i], dphi[i], counts[i], file=f)
