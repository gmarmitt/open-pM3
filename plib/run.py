# Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import threading
from ctypes import *

#local libs
if __name__ == 'pm3.plib.run':
	from pm3.plib.structs 	import *
	from pm3.plib.progbar import *
else:
	from plib.structs 	import *
	from plib.progbar import *

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  SIMULATION - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def SIMU_TRAJ(PATH, prmt_c, matrix_c, tables_c, histo_c, progcount, is_progbar_active, is_plot_active):

	#global_norm = 1
	#number of z slices
	#number_z_slices = 0
	#for iLa in range(1, prmt_c.nLa+1):
	#	number_z_slices += prmt_c.layer[iLa].thick / prmt_c.layer[iLa].dL[2]
	#number of interactions in this loop
	#norm =	global_norm * number_z_slices / prmt_c.simu.nI

	norm =	prmt_c.beam.prtcl_sr * prmt_c.thick / prmt_c.simu.nI

	#open histogram
	#if there is no histogram array from input, create one
	if histo_c == None:
		histo_c = (SThisto * prmt_c.simu.nThreads)()
	#open histogram
	for iThread in range(prmt_c.simu.nThreads):
		histo_c[iThread].__init__(prmt_c.outp.histo_type, prmt_c.outp.tof, prmt_c.nCp+1, prmt_c.nEl+1, prmt_c.outp.nEn+1, prmt_c.outp.nAn+1, prmt_c.outp.nTm+1, norm)

	#open progress counts
	if progcount == None:
		progcount = (c_long * prmt_c.simu.nThreads)()

	progcount_fill = (c_long * prmt_c.simu.nThreads)()
	for iThread in range(prmt_c.simu.nThreads):
		progcount[iThread] = 0
		progcount_fill[iThread] = 0

	#open pM3 c++ library
	clib = CDLL(PATH.clib[0])
	clib.TRAJ_DIRECT.argtypes = [c_int, POINTER(c_long * prmt_c.simu.nThreads), POINTER(STprmt), POINTER(STmatrix * (prmt_c.nMt+1)), POINTER(STtables), POINTER(SThisto * prmt_c.simu.nThreads)]
	clib.TRAJ_DIRECT.restypes = c_int;
	clib.TRAJ_FILL.argtypes = [c_int, POINTER(c_long * prmt_c.simu.nThreads), POINTER(STprmt), POINTER(STmatrix * (prmt_c.nMt+1)), POINTER(STtables)]
	clib.TRAJ_FILL.restypes = c_int;
	clib.TRAJ_CONNECT.argtypes = [c_int, POINTER(c_long * prmt_c.simu.nThreads), POINTER(STprmt), POINTER(STmatrix * (prmt_c.nMt+1)), POINTER(STtables), POINTER(SThisto * prmt_c.simu.nThreads)]
	clib.TRAJ_CONNECT.restypes = c_int;

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	#if using direct trajectories, run a simpler code and exit function
	if prmt_c.straj.direct == 1:
		#allocate space for a single trajectory, necessary for the algorithm to work
		matrix_c[prmt_c.straj.iMt].allocate_traj(prmt_c.simu.nThreads)
		matrix_c[prmt_c.straj.iMt].zeroes_traj(prmt_c.simu.nThreads)

		#trajectories progbar
		if prmt_c.debug == 0 and is_progbar_active == True:
			progbar = progress_bar
			count_total = prmt_c.simu.nI_per_thread * prmt_c.simu.nThreads
			progbar.back = threading.Thread(target=progbar.RUN, args=(count_total,progcount))
			progbar.back.start()

		print('\nBuilding trajectories...')
		#starts all filling threads
		back = [None] * prmt_c.simu.nThreads
		for iThread in range(prmt_c.simu.nThreads):
			#starts trajectory fill
			back[iThread] = threading.Thread(target=clib.TRAJ_DIRECT, args=(c_int(iThread), byref(progcount), byref(prmt_c), byref(matrix_c), byref(tables_c), byref(histo_c)))
			back[iThread].start()

		#wait until all simulation threads are done
		for iThread in range(prmt_c.simu.nThreads):
			back[iThread].join()

		#sets off the progress bar thread
		if prmt_c.debug == 0 and is_progbar_active == True:
			progbar.back.join()

		return histo_c

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	#else, run the trajectory connection algorithm

	#allocate memory to save the trajectories
	matrix_c[prmt_c.straj.iMt].allocate_traj(prmt_c.straj.nIon)
	matrix_c[prmt_c.straj.iMt].zeroes_traj(prmt_c.straj.nIon)

	#starts all filling threads
	back_fill = [None] * prmt_c.simu.nThreads
	for iThread in range(prmt_c.simu.nThreads):
		#starts trajectory fill
		back_fill[iThread] = threading.Thread(target=clib.TRAJ_FILL, args=(c_int(iThread), byref(progcount_fill), byref(prmt_c), byref(matrix_c), byref(tables_c)))
		back_fill[iThread].start()

	#wait until all simulation threads are done
	for iThread in range(prmt_c.simu.nThreads):
		back_fill[iThread].join()

	#trajectories progbar
	if prmt_c.debug == 0 and is_progbar_active == True:
		progbar = progress_bar
		count_total = prmt_c.simu.nI_per_thread * prmt_c.simu.nThreads
		progbar.back = threading.Thread(target=progbar.RUN, args=(count_total, progcount))
		progbar.back.start()

	#starts all connection threads
	back = [None] * prmt_c.simu.nThreads
	'''
	for iThread in range(prmt_c.simu.nThreads):
		back[iThread] = threading.Thread(target=clib.TRAJ_CONNECT, args=(c_int(iThread), byref(progcount), byref(prmt_c), byref(matrix_c), byref(tables_c), byref(histo_c)))
		back[iThread].start()
	'''
	back[0] = threading.Thread(target=clib.TRAJ_CONNECT, args=(c_int(0), byref(progcount), byref(prmt_c), byref(matrix_c), byref(tables_c), byref(histo_c)))
	back[0].start()

	while back[0].is_alive():
		for iThread in range(prmt_c.simu.nThreads):
			progcount_fill[iThread] = 0
			back_fill[iThread] = threading.Thread(target=clib.TRAJ_FILL, args=(c_int(iThread), byref(progcount_fill), byref(prmt_c), byref(matrix_c), byref(tables_c)))
			back_fill[iThread].start()
		for iThread in range(prmt_c.simu.nThreads):
			back_fill[iThread].join()

	#wait until all simulation threads are done
	'''
	for iThread in range(prmt_c.simu.nThreads):
		back[iThread].join()
	'''
	back[0].join()

	#sets off the progress bar thread
	if prmt_c.debug == 0 and is_progbar_active == True:
		progbar.back.join()

	return histo_c

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def SIMULA(PATH, prmt_c, matrix_c, tables_c, histo_c, progcount, is_progbar_active, is_plot_active):
	#number of z slices
	#number_z_slices = 0
	#for iLa in range(1, prmt_c.nLa+1):
	#	number_z_slices += prmt_c.layer[iLa].thick / prmt_c.layer[iLa].dL[2]
	#number of interactions in this loop
	#norm =	global_norm * number_z_slices / prmt_c.simu.nI
	norm =	prmt_c.beam.prtcl_sr * prmt_c.thick / prmt_c.simu.nI

	#if there is no histogram array from input, create one
	if histo_c == None:
		histo_c = (SThisto * prmt_c.simu.nThreads)()

	#open histogram
	for iThread in range(prmt_c.simu.nThreads):
		histo_c[iThread].__init__(prmt_c.outp.histo_type, prmt_c.outp.tof, prmt_c.nCp+1, prmt_c.nEl+1, prmt_c.outp.nEn+1, prmt_c.outp.nAn+1, prmt_c.outp.nTm+1, norm)

	if progcount == None:
		#open progress counts
		progcount = (c_long * prmt_c.simu.nThreads)()

	#open pM3 c++ library
	clib = CDLL(PATH.clib[0])
	clib.SIMU_SSCT.argtypes = [c_int, POINTER(c_long * prmt_c.simu.nThreads), POINTER(STprmt), POINTER(STmatrix * (prmt_c.nMt+1)), POINTER(STtables), POINTER(SThisto * prmt_c.simu.nThreads)]
	clib.SIMU_SSCT.restypes = c_int;
	clib.SIMU_ERDA.argtypes = [c_int, POINTER(c_long * prmt_c.simu.nThreads), POINTER(STprmt), POINTER(STmatrix * (prmt_c.nMt+1)), POINTER(STtables), POINTER(SThisto * prmt_c.simu.nThreads)]
	clib.SIMU_ERDA.restypes = c_int;
	clib.SIMU_NRP.argtypes = [c_int, POINTER(c_long * prmt_c.simu.nThreads), POINTER(STprmt), POINTER(STmatrix * (prmt_c.nMt+1)), POINTER(STtables), POINTER(SThisto * prmt_c.simu.nThreads)]
	clib.SIMU_NRP.restypes = c_int;

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #


	#sets on the progress bar thread, if no debug options are used
	if prmt_c.debug == 0 and is_progbar_active == True:
		progbar = progress_bar
		count_total = prmt_c.simu.nI_per_thread * prmt_c.simu.nThreads
		progbar.back = threading.Thread(target=progbar.RUN, args=(count_total, progcount))
		progbar.back.start()

	back = []
	#pool = ThreadPool(processes=4)
	for iThread in range(prmt_c.simu.nThreads):
		back.append(None)
		if 	prmt_c.simu.algorithm == 0:
			back[iThread] = threading.Thread(target=clib.SIMU_SSCT, args=(c_int(iThread), byref(progcount), byref(prmt_c), byref(matrix_c), byref(tables_c), byref(histo_c)))
		if 	prmt_c.simu.algorithm == 2:
			back[iThread] = threading.Thread(target=clib.SIMU_ERDA, args=(c_int(iThread), byref(progcount), byref(prmt_c), byref(matrix_c), byref(tables_c), byref(histo_c)))
		if 	prmt_c.simu.algorithm == 3:
			back[iThread] = threading.Thread(target=clib.SIMU_NRP, args=(c_int(iThread), byref(progcount), byref(prmt_c), byref(matrix_c), byref(tables_c), byref(histo_c)))
		back[iThread].start()

	#wait until all simulation threads are done
	for iThread in range(prmt_c.simu.nThreads):
		back[iThread].join()

	#sets off the progress bar thread
	if prmt_c.debug == 0 and is_progbar_active == True:
		progbar.back.join()

	return histo_c
