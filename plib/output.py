# Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import math

#local libs
if __name__ == 'pm3.plib.output':
	from pm3.plib.structs 	import *
else:
	from plib.structs 	import *

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - LOGFILE - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def cross_section_log(prmt_c, tables_c):
	log = '###--------------------------------------------------------------------------###\n'
	log += '# Section: Cross Section\n'
	log += '#\n'
	if prmt_c.simu.cross_section == 0:
		log += '# Rutherford cross section\n'
	if prmt_c.simu.cross_section == 1:
		log += '# Rutherford cross section with screening potential correction\n'

	E = prmt_c.beam.E0
	iEn = int(prmt_c.beam.E0/tables_c.comp[prmt_c.nCp].neutr.delta)
	log += '# E = ' + str(prmt_c.beam.E0/1e3) + ' keV\n'
	log += '# [CS] = (Angs.)^2\n'
	log += '#\n'
	log += '#\n'
	log += '###--------------------------------------------------------------------------###\n'

	log += '# theta (degress)' + '\t'
	for iEl in range(1, prmt_c.nEl+1):
		log += prmt_c.elem[iEl].name.decode() + '\t'
	log += '\n'

	for iAn in range(tables_c.elem[iEl].CS.size):
		theta_lab = math.degrees(tables_c.elem[iEl].CS.delta * iAn + tables_c.elem[iEl].CS.initial)
		log += '{0:2.4e}'.format(theta_lab) + '\t'
		for iEl in range(1, prmt_c.nEl+1):
			log += '{0:2.2e}'.format( tables_c.elem[iEl].CS.array[iAn]*tables_c.elem[iEl].CS_correc[iAn][iEn]/E/E) + '\t'
		log += '\n'

	with open('cross_section.log', 'w') as f:
		print(log, file=f)

def energy_stopping_log(prmt_c, tables_c):
	log = '###--------------------------------------------------------------------------###\n'
	log += '# Section: Energy Stopping\n'
	log += '#\n'
	log += '# Energy stopping using SRIM\n'

	if prmt_c.simu.surf_apprx != 0:
		log += '# Only available without surface approximation\n'
		log += '###--------------------------------------------------------------------------###\n'
	else:
		nE = int(prmt_c.beam.E0/prmt_c.calc.stopp_dE) + 1

		log += '# [Stopping] = eV/Angs.\n'
		log += '#\n'
		log += '#\n'
		log += '#\n'
		log += '###--------------------------------------------------------------------------###\n'

		log += '# Energy (keV)' + '\t'
		for iCp in range(1, prmt_c.nCp+1):
			log += prmt_c.comp[iCp].name.decode() + '\t'
		log += '\n'

		for iE in range(1, nE):
			E = iE*prmt_c.calc.stopp_dE
			log += '{0:2.2e}'.format(E/1e3) + '\t'
			for iCp in range(1, prmt_c.nCp+1):
				log += '{0:2.2e}'.format(tables_c.comp[iCp].dedx_E[0].array[iE]) + '\t'
			log += '\n'

	with open('energy_stopping.log', 'w') as f:
		print(log, file=f)

def energy_straggling_log(prmt_c, tables_c):
	log = '###--------------------------------------------------------------------------###\n'
	log += '# Section: Energy Straggling\n#\n'
	if prmt_c.calc.strag_mode == 1:
		log += '# Energy straggling using Bohr model\n'
	if prmt_c.calc.strag_mode == 2:
		log += '# Energy straggling using Lindhard model\n'
	if prmt_c.calc.strag_mode == 3:
		log += '# Energy straggling using Chu model\n'
	if prmt_c.calc.strag_mode == 4:
		log += '# Energy straggling using Yang model\n'

	if prmt_c.simu.surf_apprx != 0:
		log += '# Only available without surface approximation\n'
		log += '###--------------------------------------------------------------------------###\n'
	else:
		nE = int(prmt_c.beam.E0/prmt_c.calc.stopp_dE) + 1

		log += '# [Straggling] = eV^2/Angs.\n'
		log += '#\n'
		log += '#\n'
		log += '#\n'
		log += '###--------------------------------------------------------------------------###\n'

		log += '# Energy (keV)' + '\t'
		for iCp in range(1, prmt_c.nCp+1):
			log += prmt_c.comp[iCp].name.decode() + '\t'
		log += '\n'

		for iE in range(1, nE):
			E = iE*prmt_c.calc.stopp_dE
			log += '{0:2.2e}'.format(E/1e3) + '\t'
			for iCp in range(1, prmt_c.nCp+1):
				log += '{0:2.2e}'.format(tables_c.comp[iCp].dwdx_E[0].array[iE]) + '\t'
			log += '\n'

	with open('energy_straggling.log', 'w') as f:
		print(log, file=f)

def charge_neutralization_log(prmt_c, tables_c):
	log =  '###--------------------------------------------------------------------------###\n'
	log += '# Section: charge neutralization\n'
	log += '#\n'
	if prmt_c.calc.neutr_mode == 1:
		log += '# Charge neutralization using Marion model\n'
	if prmt_c.calc.neutr_mode == 2:
		log += '# Charge neutralization using CasP function\n'

	log += '# [N] = charge ratio [0, 1]\n'
	log += '#\n'
	log += '#\n'
	log += '#\n'
	log += '###--------------------------------------------------------------------------###\n'

	log += '# Energy (keV)\t'
	for iCp in range(1, prmt_c.nCp+1):
		log += prmt_c.comp[iCp].name.decode() + '\t'
	log += '\n'

	for iE in range(tables_c.comp[prmt_c.nCp].neutr.size):
		E = iE*tables_c.comp[prmt_c.nCp].neutr.delta + tables_c.comp[prmt_c.nCp].neutr.initial
		log += '{0:2.2e}'.format(E/1e3) + '\t'
		for iCp in range(1, prmt_c.nCp+1):
			log += '{0:2.2e}'.format(tables_c.comp[iCp].neutr.array[iE]) + '\t'
		log += '\n'

	if prmt_c.log == True:
		with open('charge_neutralization.log', 'w') as f:
			print(log, file=f)


def LOGFILE(prmt_c, tables_c):
	energy_stopping_log(prmt_c, tables_c)
	energy_straggling_log(prmt_c, tables_c)
	cross_section_log(prmt_c, tables_c)
	charge_neutralization_log(prmt_c, tables_c)


### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - OUTPUT  - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def OUTPUT(term_input, prmt_c, histo_c, write_to_file=True):

	if write_to_file == True:
		f = open(term_input.outp_FileName, 'w')

	#time or energy output
	if prmt_c.outp.tof == 0:
		nx = prmt_c.outp.nEn
		dx = prmt_c.outp.dEnergy/1e3
		xmin = prmt_c.outp.Emin/1e3
	elif prmt_c.outp.tof == 1:
		nx = prmt_c.outp.nTm
		dx = prmt_c.outp.dTime
		xmin = prmt_c.outp.Tmin

	#simple spectrum
	if prmt_c.outp.histo_type == 0:
		simple = [[0 for iAn in range (prmt_c.outp.nAn)] for i in range(nx)]
		for iThread in range(prmt_c.simu.nThreads):
			for i in range(nx):
				for iAn in range(prmt_c.outp.nAn):
					simple[i][iAn] += histo_c[iThread].simple[i][iAn]

		if write_to_file == True:
			for i in range(nx):
				print(i*dx+xmin, end='\t', file=f)
				for iAn in range(prmt_c.outp.nAn):
					print(simple[i][iAn], end='\t', file=f)
				print(end='\n', file=f)
		else:
			return simple

	elif prmt_c.outp.histo_type == 1:
		element = [[0 for iEl in range(prmt_c.nEl+1)] for i in range(nx)]
		for iThread in range(prmt_c.simu.nThreads):
			for i in range(nx):
				for iEl in range(1, prmt_c.nEl+1):
					element[i][iEl] += histo_c[iThread].element[i][iEl]

		if write_to_file == True:
			for i in range(nx):
				print(i*dx+xmin, end='\t', file=f)
				for iEl in range(1, prmt_c.nEl+1):
					print(element[i][iEl], end='\t', file=f)
				print(end='\n', file=f)
		else:
			return element

	elif prmt_c.outp.histo_type == 2:
		compound = [[0 for iCp in range(prmt_c.nCp+1)] for i in range(nx)]
		for iThread in range(prmt_c.simu.nThreads):
			for i in range(nx):
				for iCp in range(1, prmt_c.nCp+1):
					compound[i][iCp] += histo_c[iThread].compound[i][iCp]

		if write_to_file == True:
			for i in range(nx):
				print(i*dx+xmin, end='\t', file=f)
				for iCp in range(1, prmt_c.nCp+1):
					print(compound[i][iCp], end='\t', file=f)
				print(end='\n', file=f)
		else:
			return compound

	if write_to_file == True:
		f.close()

	return
