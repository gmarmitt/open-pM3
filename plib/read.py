# Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import math
#local libs
if __name__ == 'pm3.plib.read':
	from pm3.plib.structs 	import *
else:
	from plib.structs 	import *

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  FUNCTIONS  - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def READING(filename):
	with open(filename, 'r') as f:
		#read the file
		lines = f.read().split('\n')
		#remove the spaces in the end of the line
		lines_without_spaces = [i.strip() for i in filter(None, (lines))]

		#remove the comments lines
		lines_without_comments = []
		for j in range(0, len(lines_without_spaces)):
			if lines_without_spaces[j][0] != '#':
				lines_without_comments.append(lines_without_spaces[j])

		#separate each line in a list of words
		lines_of_separeted_words = [list(filter(None, j.split())) for j in lines_without_comments]

	number_of_words = [len(i) for i in lines_of_separeted_words]
	max_number_of_words = max(number_of_words)

	#put a blank value in each non occupied position
	matrix_of_words = [i + [''] * (max_number_of_words - len(i)) for i in lines_of_separeted_words]

	return matrix_of_words

### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - -  PARAMETERS - - - - - - - - - - - - - - - - - - - - -###
### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -###

def OPEN_PARAMETERS_FILE(term_input, ISOTOPES_PATH, config):
	#read a table of periodic elements
	isotopesData = READING(ISOTOPES_PATH)

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	#evaluate the number of elements, compound, layers and matrix
	nEl = nCp = nLa = nMt = 0
	while True:
		nEl += 1
		try:
			config['element '+str(nEl)]
		except KeyError:
			nEl -= 1
			break
	while True:
		nCp += 1
		try:
			config['compound '+str(nCp)]
		except KeyError:
			nCp -= 1
			break
	while True:
		nLa += 1
		try:
			config['layer '+str(nLa)]
		except KeyError:
			nLa -= 1
			break
	while True:
		nMt += 1
		try:
			config['matrix '+str(nMt)]
		except KeyError:
			nMt -= 1
			break
	#find the maximum number of plasmons
	nPl = 0
	for iCp in range(1, nCp+1):
		nPl_iCp = 0
		while True:
			nPl_iCp += 1
			A = config['compound '+str(iCp)].get('plasmon '+str(nPl_iCp)+' A', 'error')
			if A == 'error':
				nPl_iCp -= 1
				break
		if nPl_iCp > nPl:
			nPl = nPl_iCp
	#find the maximum number of bound states
	nBS = 0
	for iEl in range(1, nEl+1):
		nBS_iEl = 0
		while True:
			nBS_iEl += 1
			A = config['element '+str(iEl)].get('bound state '+str(nBS_iEl), 'error')
			if A == 'error':
				nBS_iEl -= 1
				break
		if nBS_iEl > nBS:
			nBS = nBS_iEl

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	#check for isotopic elements
	iEl = 1
	elem_bckp = {}
	while iEl < nEl+1:
		Z = config['element '+str(iEl)].getint('Z')
		Symb = isotopesData[Z-1][0]

		#if the element uses isotopes
		if config['element '+str(iEl)].get('M', '0') == 'All Isotopes':
			#make a backup of this element parameters
			elem_bckp[Symb] = {}
			for key in config['element '+str(iEl)]:
				elem_bckp[Symb][key] = config['element '+str(iEl)][key]

			#for each compound
			for iCp in range(1, nCp+1):
				iEl_exist = '%element '+str(iEl) in config['compound '+str(iCp)]
				nEl_exist = '%element '+str(nEl) in config['compound '+str(iCp)]

				#change the linked compounds to use the isotopicData element
				if iEl_exist:
					config['compound '+str(iCp)]['%'+Symb] = config['compound '+str(iCp)]['%element '+str(iEl)]
					config['compound '+str(iCp)]['%element '+str(iEl)] = '0'

				#shift all elements in all compounds
				for jEl in range(iEl, nEl):
					jEl1_frac = config['compound '+str(iCp)].getfloat('%element '+str(jEl+1), 0)
					config['compound '+str(iCp)]['%element '+str(jEl)] = str(jEl1_frac)
					if jEl1_frac == 0:
						del config['compound '+str(iCp)]['%element '+str(jEl)]

				#remove the last element in the compound
				if nEl_exist:
					del config['compound '+str(iCp)]['%element '+str(nEl)]

			#do the same in the elements sections
			for jEl in range(iEl, nEl):
				config['element '+str(jEl)] = config['element '+str(jEl+1)]

			del config['element '+str(nEl)]
			iEl -= 1
			nEl -= 1

		iEl += 1

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	#if using isotopesData elements, creates new [element iEl] sections and [compound iCp] keys
	for iCp in range(1, nCp+1):
		for line in range(118):		#because isotopes.dat has 118 elements
			match_found_in_column = None
			try:
				config['compound '+str(iCp)]['%'+isotopesData[line][0]]
				match_found_in_column = 0
			except:
				pass
			try:
				config['compound '+str(iCp)]['%'+isotopesData[line][1]]
				match_found_in_column = 1
			except:
				pass

			#if there is a match: creat a new element
			if match_found_in_column != None:
				isotope = 3
				while isotopesData[line][isotope] != '':
					#element symbol
					Symb = isotopesData[line][0]

					#first, test if element was already written in the config structure
					isotope_already_added = False
					for iEl in range(1, nEl+1):
						if config['element '+str(iEl)].get('name') == 'Tabled '+isotopesData[line][isotope]+Symb:
							isotope_already_added = True
							break

					#if not, create new [element iEl] section
					if isotope_already_added == False:
						nEl += 1
						iEl = nEl
						config['element '+str(nEl)] = {}
						#try to load the backup of this element parameters
						try:
							for key in elem_bckp[Symb]:
								config['element '+str(nEl)][key] = elem_bckp[Symb][key]
						except:
							pass
						config['element '+str(nEl)]['name'] = 'Tabled '+isotopesData[line][isotope]+Symb
						config['element '+str(nEl)]['Z'] = isotopesData[line][1]
						config['element '+str(nEl)]['M'] = isotopesData[line][isotope]

					#and in [compound iCp] section create a new '%element iEl' key
					total_fraction = config['compound '+str(iCp)].getfloat('%'+isotopesData[line][match_found_in_column])
					isotopic_abundance = float(isotopesData[line][isotope+1])
					config['compound '+str(iCp)]['%element '+str(iEl)] = str(total_fraction*isotopic_abundance)
					#finally, goes to the next isotope
					isotope += 2

				#then delete the old compound key
				del config['compound '+str(iCp)]['%'+isotopesData[line][match_found_in_column]]

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #

	#open the parameters and matrix structures
	prmt_c = STprmt(nCp, nEl, nLa, nMt, nPl, nBS)
	matrix_c = (STmatrix * (nMt+1))()

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	#the following section passes the parameters from configparser structure 'config'
	#to the ctype structures 'prmt_c' and 'matrix_c'

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	try:
		prmt_c.debug = config['header'].getint('debug', 0)
		prmt_c.log	 = int(config['header'].getboolean('log', False))
	except:
		pass

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	try:
		if config['simulation'].get('algorithm', 'ssct') == "ssct" : prmt_c.simu.algorithm = 0
		if config['simulation'].get('algorithm', 'ssct') == "traj" : prmt_c.simu.algorithm = 1
		if config['simulation'].get('algorithm', 'ssct') == "erda" : prmt_c.simu.algorithm = 2
		if config['simulation'].get('algorithm', 'ssct') == "nrp" : prmt_c.simu.algorithm = 3

		prmt_c.simu.surf_apprx = int(config['simulation'].getboolean('surface approximation', False))

		prmt_c.simu.autofit 		= config['simulation'].getint('autofit', 0)
		prmt_c.simu.nI 				= int(config['simulation'].getfloat('nI', 0))
		prmt_c.simu.dL 				= config['simulation'].getfloat('dL', 0)
		prmt_c.simu.dr 				= config['simulation'].getfloat('dr', 0)
		prmt_c.simu.nThreads		= config['simulation'].getint('cores', 1)

		if config['simulation'].get('line shape', 'EMG') == "gauss" 	: prmt_c.simu.line_shape = 0
		if config['simulation'].get('line shape', 'EMG') == "EMG" 		: prmt_c.simu.line_shape = 1
		if config['simulation'].get('cross section', 'correc') == "Rutherford" 	: prmt_c.simu.cross_section = 0
		if config['simulation'].get('cross section', 'correc') == "correc" 		: prmt_c.simu.cross_section = 1
		if config['simulation'].get('cross section', 'correc') == "ELSEPA" 		: prmt_c.simu.cross_section = 2
		if config['simulation'].get('cross section', 'correc') == "Andersen" 	: prmt_c.simu.cross_section = 3
	except:
		pass

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	try:
		prmt_c.cexpl.stopping_ratio	= config['coulomb explosion'].getfloat('ratio', 0)
		prmt_c.cexpl.gamma 			= config['coulomb explosion'].getfloat('gamma', 0)
		prmt_c.cexpl.alpha 			= config['coulomb explosion'].getfloat('alpha', 0)
	except:
		pass

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	try:
		prmt_c.calc.stopp_dE = config['calculations'].getfloat('dE', 	1.0)
		prmt_c.calc.neutr_dE = config['calculations'].getfloat('dE', 	1.0)
		prmt_c.calc.dielec_dE = config['calculations'].getfloat('dielec dE', 	0)
		prmt_c.calc.dielec_wmax = config['calculations'].getfloat('dielec wmax', 	1000)
		prmt_c.calc.dielec_qmax = config['calculations'].getfloat('dielec qmax', 	10)
		if config['calculations'].get('dielec function', 'off') == "off" 	: prmt_c.calc.dielec_mode = 0
		if config['calculations'].get('dielec function', 'off') == "Lindhard" 	: prmt_c.calc.dielec_mode = 1
		if config['calculations'].get('dielec function', 'off') == "Mermin" 		: prmt_c.calc.dielec_mode = 2
		if config['calculations'].get('stopping', 	'stop96') == "off" 	: prmt_c.calc.stopp_mode = 0
		if config['calculations'].get('stopping', 	'stop96') == "stop96" 	: prmt_c.calc.stopp_mode = 1
		if config['calculations'].get('straggling', 'Chu') == "off" 	: prmt_c.calc.strag_mode = 0
		if config['calculations'].get('straggling', 'Chu') == "Bohr" 	: prmt_c.calc.strag_mode = 1
		if config['calculations'].get('straggling', 'Chu') == "Lind" 	: prmt_c.calc.strag_mode = 2
		if config['calculations'].get('straggling', 'Chu') == "Chu" 	: prmt_c.calc.strag_mode = 3
		if config['calculations'].get('straggling', 'Chu') == "Yang" 	: prmt_c.calc.strag_mode = 4
		if config['calculations'].get('neutralization', 'Marion') == "off" 		: prmt_c.calc.neutr_mode = 0
		if config['calculations'].get('neutralization', 'Marion') == "Marion" 	: prmt_c.calc.neutr_mode = 1
		if config['calculations'].get('neutralization', 'Marion') == "CasP" 	: prmt_c.calc.neutr_mode = 2
		if config['calculations'].get('neutralization', 'Marion') == "Zalm" 	: prmt_c.calc.neutr_mode = 3
		if config['calculations'].get('neutralization', 'Marion') == "Experimental" 	: prmt_c.calc.neutr_mode = 4
	except:
		pass

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	try:
		prmt_c.outp.Emin 		= config['output'].getfloat('Emin', 0)
		prmt_c.outp.Emax 		= config['output'].getfloat('Emax', 0)
		prmt_c.outp.Tmin 		= config['output'].getfloat('Tmin', 0)
		prmt_c.outp.Tmax 		= config['output'].getfloat('Tmax', 0)
		prmt_c.outp.dEnergy		= config['output'].getfloat('dEnergy', 0.1)
		prmt_c.outp.dAngle 		= math.radians(config['output'].getfloat('dAngle', 0.1))
		prmt_c.outp.dTime		= config['output'].getfloat('dTime', 1)
		if config['output'].get('histogram', 'simple') == 'simple' 		: prmt_c.outp.histo_type = 0
		if config['output'].get('histogram', 'simple') == 'element' 	: prmt_c.outp.histo_type = 1
		if config['output'].get('histogram', 'simple') == 'compound' 	: prmt_c.outp.histo_type = 2
		prmt_c.outp.tof = int(config['output'].getboolean('tof', False))
	except:
		pass

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	try:
		try:
			ion = config['beam'].get('ion')
			for line in range(118):		#because isotopes.dat has 118 elements
				if ion == isotopesData[line][0] or ion == isotopesData[line][1]:
					config['beam']['Z'] = isotopesData[line][1]
					config['beam']['M'] = isotopesData[line][2]
		except:
			pass
		if config['beam'].get('type', 'ion') == 'ion' : prmt_c.beam.btype = 0
		if config['beam'].get('type', 'ion') == 'molecule' : prmt_c.beam.btype = 1
		if config['beam'].get('type', 'ion') == 'electron' : prmt_c.beam.btype = 2

		prmt_c.beam.Z	= config['beam'].getint('Z', 0)
		prmt_c.beam.M	= config['beam'].getfloat('M', 0)
		prmt_c.beam.Q	= config['beam'].getfloat('Q', 1)
		prmt_c.beam.E0	= config['beam'].getfloat('E0', 0)
		prmt_c.beam.prtcl_sr	= config['beam'].getfloat('particle sr', 1)
		prmt_c.beam.T1	= math.radians(config['beam'].getfloat('theta inc', 0))
		try:
			prmt_c.beam.T1	= math.radians(config['beam'].getfloat('theta'))
		except:
			pass
		try:
			prmt_c.beam.T1	= math.radians(config['beam'].getfloat('T1'))
		except:
			pass
	except:
		pass

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	try:
		if config['detector'].get('type', 'electro') == "electro" : prmt_c.dete.det_type = 0
		if config['detector'].get('type', 'electro') == "magnetic" : prmt_c.dete.det_type = 0
		if config['detector'].get('type', 'electro') == "TOF" 			: prmt_c.dete.det_type = 1

		prmt_c.dete.sig_exp2		= config['detector'].getfloat('resolution', 0)
		prmt_c.dete.sig_exp2_TOF	= config['detector'].getfloat('TOF resolution', 0)
		prmt_c.dete.LY				= math.radians(config['detector'].getfloat('LY', 0))
		prmt_c.dete.Y0 				= math.radians(config['detector'].getfloat('Y0', 0))
		prmt_c.dete.dY 				= math.radians(config['detector'].getfloat('dY', 0.1))
		prmt_c.dete.LX 				= math.radians(config['detector'].getfloat('LX', 0))
		prmt_c.dete.X0 				= math.radians(config['detector'].getfloat('X0', 0))
		prmt_c.dete.dX 				= math.radians(config['detector'].getfloat('dX', 0.1))

		prmt_c.dete.r	 		= math.radians(config['detector'].getfloat('radius', 0))
		prmt_c.dete.d_tof		= config['detector'].getfloat('TOF distance', 0)
		prmt_c.outp.timeoutp	= config['detector'].getint('TOF output', 0)
	except:
		pass

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	try:
		prmt_c.straj.MS				= int(config['trajectory'].getboolean('MS', False))
		prmt_c.straj.direct			= int(config['trajectory'].getboolean('direct', False))
		prmt_c.straj.memory			= config['trajectory'].getint('memory', 1000)
		prmt_c.straj.nIon			= int(config['trajectory'].getfloat('nIon', 0))
		prmt_c.straj.max_distance	= config['trajectory'].getfloat('max distance', 100000)
		prmt_c.straj.dE				= config['trajectory'].getfloat('accept dE', 5)
		prmt_c.straj.angle_min		= math.radians(config['trajectory'].getfloat('angle min', 1))
		prmt_c.straj.angle_max		= math.radians(config['trajectory'].getfloat('angle max', 180))
		prmt_c.straj.CS_frac		= config['trajectory'].getfloat('CS frac', 1e-2)
		prmt_c.straj.eflp			= config['trajectory'].getfloat('elastic flp', 1)
		prmt_c.straj.iMt			= config['trajectory'].getint('iMt', 1)
		prmt_c.straj.CS_cut_algo	= config['trajectory'].getint('CS_cut_algo', 0)
	except:
		pass

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	try:
		prmt_c.snrp.Ei				= config['nrp'].getfloat('Ei', 0)
		prmt_c.snrp.Ef				= config['nrp'].getfloat('Ef', 0)
	except:
		pass

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	for iEl in range(1, nEl+1):
		prmt_c.elem[iEl].name 	= c_char_p(config['element '+str(iEl)].get('name', 'Element '+str(iEl)).encode())
		prmt_c.elem[iEl].Z 		= config['element '+str(iEl)].getint('Z', 0)
		prmt_c.elem[iEl].M 		= config['element '+str(iEl)].getfloat('M', 0)
		prmt_c.elem[iEl].sig	= config['element '+str(iEl)].getfloat('sig0', 0)
		prmt_c.elem[iEl].Ekin	= config['element '+str(iEl)].getfloat('Ekin', 0)
		prmt_c.elem[iEl].Er_mean	= config['element '+str(iEl)].getfloat('Er mean', 0)
		prmt_c.elem[iEl].Er_width	= config['element '+str(iEl)].getfloat('Er width', 0)
		prmt_c.elem[iEl].CS_file	= c_char_p(config['element '+str(iEl)].get('CS file', 'None').encode())

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	for iCp in range(1, nCp+1):
		prmt_c.comp[iCp].name = c_char_p(config['compound '+str(iCp)].get('name', 'Compound '+str(iCp)).encode())
		prmt_c.comp[iCp].dens = config['compound '+str(iCp)].getfloat('density', 0)
		prmt_c.comp[iCp].eRBS_dPdx = config['compound '+str(iCp)].getfloat('eRBS dPdx', 0)

		#get the custom stopping and straggling values
		prmt_c.comp[iCp].dedx[0] = config['compound '+str(iCp)].getfloat('dedx in', 0)
		prmt_c.comp[iCp].dwdx[0] = config['compound '+str(iCp)].getfloat('dw2dx in', 0)
		prmt_c.comp[iCp].dwdx_corr = config['compound '+str(iCp)].getfloat('dwdx corr', 1)
		prmt_c.comp[iCp].dEdx_corr = config['compound '+str(iCp)].getfloat('dEdx corr', 1)
		prmt_c.comp[iCp].VEGAS_file = c_char_p(config['compound '+str(iCp)].get('VEGAS file', '').encode())
		for iEl in range(1, nEl+1):
			prmt_c.comp[iCp].dedx[iEl] = config['compound '+str(iCp)].getfloat('dedx out '+str(iEl), 0)
			prmt_c.comp[iCp].dwdx[iEl] = config['compound '+str(iCp)].getfloat('dw2dx out '+str(iEl), 0)

		#store the elements fractions
		for iEl in range(1, nEl+1):
			prmt_c.comp[iCp].frac_elem[iEl] = config['compound '+str(iCp)].getfloat('%element '+str(iEl), 0)
			if prmt_c.comp[iCp].frac_elem[iEl] != 0:
				prmt_c.comp[iCp].nElem += 1

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	for iLa in range(1, nLa+1):
		prmt_c.layer[iLa].thick		= config['layer '+str(iLa)].getfloat('thickness', 0)
		prmt_c.layer[iLa].dL[0] 	= config['layer '+str(iLa)].getfloat('dLx', 0)
		prmt_c.layer[iLa].dL[1] 	= config['layer '+str(iLa)].getfloat('dLy', 0)
		prmt_c.layer[iLa].dL[2] 	= config['layer '+str(iLa)].getfloat('dLz', 0)
		prmt_c.layer[iLa].Cp		= config['layer '+str(iLa)].getint('compound', 0)
		prmt_c.layer[iLa].period_cont = int(config['layer '+str(iLa)].getboolean('periodic contourn', False))
		prmt_c.layer[iLa].period_posi = int(config['layer '+str(iLa)].getboolean('periodic position', False))

		prmt_c.thick += prmt_c.layer[iLa].thick

		for iMt in range(1, nMt+1):
			prmt_c.layer[iLa].Mt_weight[iMt] = config['layer '+str(iLa)].getfloat('%matrix '+str(iMt), 0)
			if prmt_c.layer[iLa].Mt_weight[iMt] != 0:
				prmt_c.layer[iLa].lay_type = 1
				prmt_c.layer[iLa].LAYER_nMt += 1

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	for iMt in range(1, nMt+1):
		matrix_c[iMt].FileName = c_char_p((term_input.prmt_FileDir + config['matrix '+str(iMt)].get('file', 'None')).encode())
		matrix_c[iMt].mtx_type = c_char_p(config['matrix '+str(iMt)].get('type', 'pM3').encode())
		matrix_c[iMt].phi = math.radians(config['matrix '+str(iMt)].getfloat('phi', 0))
		matrix_c[iMt].dCp = config['matrix '+str(iMt)].getfloat('dCp', 1e-3)
		matrix_c[iMt].rand_phi = int(config['matrix '+str(iMt)].getboolean('rand phi', False))

		matrix_c[iMt].nx = config['matrix '+str(iMt)].getint('nx', 0)
		matrix_c[iMt].ny = config['matrix '+str(iMt)].getint('ny', 0)
		matrix_c[iMt].nz = config['matrix '+str(iMt)].getint('nz', 0)

		matrix_c[iMt].matgen_name = c_char_p(config['matrix '+str(iMt)].get('matgen name', 'None').encode())
		matrix_c[iMt].matgen_attr = c_char_p(config['matrix '+str(iMt)].get('matgen attr', 'None').encode())

		#first try to read the matrix data from config
		matrix_c[iMt].FileData = c_char_p(config['matrix '+str(iMt)].get('data', 'None').encode())
		#then delete the data entry from config
		try:
			del config['matrix '+str(iMt)]['data']
		except:
			pass

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	for iCp in range(1, nCp+1):
		#surface plasmon
		prmt_c.comp[iCp].surface_plasmon.A = config['compound '+str(iCp)].getfloat('plasmon surface A', 0)
		prmt_c.comp[iCp].surface_plasmon.w0 = config['compound '+str(iCp)].getfloat('plasmon surface w0', 0)
		prmt_c.comp[iCp].surface_plasmon.gamma = config['compound '+str(iCp)].getfloat('plasmon surface gamma', 0)
		#bulk plasmons
		for iPl in range(1, nPl+1):
			prmt_c.comp[iCp].plasmon[iPl].A = config['compound '+str(iCp)].getfloat('plasmon '+str(iPl)+' A', 0)
			prmt_c.comp[iCp].plasmon[iPl].w0 = config['compound '+str(iCp)].getfloat('plasmon '+str(iPl)+' w0', 1)
			prmt_c.comp[iCp].plasmon[iPl].gamma = config['compound '+str(iCp)].getfloat('plasmon '+str(iPl)+' gamma', 1)

	for iEl in range(1, nEl+1):
		#bound states
		for iBS in range(1, nBS+1):
			electronic_shell = config['element '+str(iEl)].get('bound state '+str(iBS), None)
			if electronic_shell != None:
				prmt_c.elem[iEl].boundstate[iBS].n = int(electronic_shell[0])
				if electronic_shell[1] == 's' : prmt_c.elem[iEl].boundstate[iBS].l = 0
				if electronic_shell[1] == 'p' : prmt_c.elem[iEl].boundstate[iBS].l = 1
				if electronic_shell[1] == 'd' : prmt_c.elem[iEl].boundstate[iBS].l = 2
				if electronic_shell[1] == 'f' : prmt_c.elem[iEl].boundstate[iBS].l = 3
				if electronic_shell[1] == 'g' : prmt_c.elem[iEl].boundstate[iBS].l = 4
				prmt_c.elem[iEl].boundstate[iBS].Ne = float(electronic_shell[2:])
				prmt_c.elem[iEl].boundstate[iBS].mode = config['element '+str(iEl)].getint('bound state '+str(iBS)+' mode', 0)
				prmt_c.elem[iEl].boundstate[iBS].Ib = config['element '+str(iEl)].getfloat('bound state '+str(iBS)+' Ib', 0)

#-  -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  #
	try:
		mode = config['simulation'].getint('mode', 0)
	except:
		pass
	if mode == 1 or mode == 2 or mode == 4:
		print('Error! Mode', mode, 'not implemented.')
		quit()
	if mode == 3:
		print('Missing!!!')
		quit()
	if mode == 5:
		prmt_c.beam.btype = 1

	#log file
	with open(term_input.prmt_FileDir+'last-cpm.log', 'w') as f:
		config.write(f)

	return [prmt_c, matrix_c, config]
