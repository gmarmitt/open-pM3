/*
 * Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "clib_misc.h"

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - -  MATRIX - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

int GENERATE_PM3_MATRIX(STmatrix *_matrix){
	int i, j, k, iCp, err, use_FileData;
	long int nf, f, Cp_counter=0;
	char gar[100];

	if(strcmp(_matrix->FileData, "None") == 0)
		use_FileData = 0;
	else
		use_FileData = 1;

	FILE *matrix_file;
	char *token;
	switch(use_FileData){
		case 0:
			matrix_file = fopen(_matrix->FileName, "r");
			if(fgets(gar, 100, matrix_file) == NULL)
				return 200;
			break;
		case 1:
			// create a token to preload each value
			token = strtok (_matrix->FileData,"\n");
			// now jump the first line of the matrix
			token = strtok (NULL,"\n");
			break;
	}
	f = 0; nf = 0;
	for(k=_matrix->nz-1;k>=0;k--)
		for(i=0;i<_matrix->nx;i++)
			for(j=0;j<_matrix->ny;j++){
				if(f == nf){

					// try to read the matrix, if it fails throw an exception back to python
					switch(use_FileData){
						case 0:
							err = fscanf(matrix_file, "%ld %d\n", &nf, &iCp);
							break;
						case 1:
							if(token == NULL) return 201;
							err = sscanf(token, "%ld %d\n", &nf, &iCp);
							// go to the next token on the list
							token = strtok (NULL,"\n");
							break;
					}
					if(err != 2) return 202;

					f = 0;
				}

				if(iCp != 0)Cp_counter += 1;

				//fill the matrix
				_matrix->Cp[i][j][k] = iCp;
				//next point in the matrix
				f += 1;
			}

	_matrix->Cp_ratio = 1.0*Cp_counter/(_matrix->nx*_matrix->ny*_matrix->nz);

	switch(use_FileData){
		case 0:
			fclose(matrix_file);
			break;
		case 1:
			break;
	}

	return 0;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

int GENERATE_TRI3DYN_MATRIX(STprmt *_prmt, STmatrix *_matrix){
	int i, j, k, iCp, iEl, jEl, new_nCp, err, use_FileData, prf_FileType=0, jline=5;
	long long int Cp_counter=0, factor, index;
	double buffer, density, elem_frac;
	//second buffer, used when reading prf2 filetypes
	double buffer_prf2[5];

	double *nCp_quant = new double[_prmt->nEl+1];
	double *max_Cp = new double[_prmt->nEl+1];
	double *min_Cp = new double[_prmt->nEl+1];
	for(iEl=1; iEl<_prmt->nEl+1; iEl++){
		nCp_quant[iEl] = 0;
		max_Cp[iEl] = -1e9;
		min_Cp[iEl] = +1e9;
	}

	double **row = new double*[(_prmt->nEl+1)*_matrix->nz];
	for(i=0; i<(_prmt->nEl+1)*_matrix->nz; i++)
		row[i] = new double[_matrix->ny * _matrix->nx];

	if(strcmp(_matrix->FileData, "None") == 0)
		use_FileData = 0;
	else
		use_FileData = 1;

	//get file extension
	char *file_ext;
	file_ext = strrchr(_matrix->FileName, '.');

	//define the file type for reading
	if(strcmp(file_ext, ".prf") == 0)
		prf_FileType = 0;
	if(strcmp(file_ext, ".prf2") == 0)
		prf_FileType = 1;

	FILE *matrix_file;
	switch(use_FileData){
		case 0:
			matrix_file = fopen(_matrix->FileName, "r");
			break;
		case 1:
			break;
	}

	// create a token to preload each value
	char *token;
	token = strtok (_matrix->FileData,"\t\n");

	//first we need the maximum and minumum concentration of each element
	//for(k=0;k<_matrix->nz;k++)
	for(k=_matrix->nz-1;k>=0;k--){
		for(iEl=0; iEl<_prmt->nEl+1; iEl++){
			for(j=0;j<_matrix->ny;j++)
				for(i=0;i<_matrix->nx;i++){
					buffer = 0;

					switch(prf_FileType){
						//first TRY3DIN format, nz*nEl lines each with nx*ny components
						case 0:
							switch(use_FileData){
								case 0:
									err = fscanf(matrix_file, "%le\t", &buffer);
									break;
								case 1:
									err = sscanf(token, "%le\t", &buffer);
									// go to the next token on the list
									token = strtok (NULL,"\t\n");
									break;
							}
							if(err != 1) return 202;
							break;
						//Oct,2016 TRY3DIN format, nz*nEl*nx*ny/5 lines each with 5 components
						case 1:
							if(jline >= 5){
								jline = 0;
								switch(use_FileData){
									case 0:
										err = fscanf(matrix_file, "%lf\t%lf\t%lf\t%lf\t%lf\t\n", &buffer_prf2[0], &buffer_prf2[1], &buffer_prf2[2], &buffer_prf2[3], &buffer_prf2[4]);
										break;
									case 1:
										err = sscanf(token, "%lf\t%lf\t%lf\t%lf\t%lf\t", &buffer_prf2[0], &buffer_prf2[1], &buffer_prf2[2], &buffer_prf2[3], &buffer_prf2[4]);
										token = strtok (NULL,"\t\n");	// go to the next token on the list

										break;
								};
								//cout << buffer_prf2[0] << ' ' << buffer_prf2[1] << ' ' << buffer_prf2[2] << ' ' << buffer_prf2[3] << ' ' << buffer_prf2[4] << endl;
								if(err != 5) return 202;
							};

							buffer = buffer_prf2[jline];
							jline += 1;
							break;
					}

					row[k*(_prmt->nEl+1) + iEl][j*_matrix->nx + i] = buffer;
					if(iEl != 0 && buffer < min_Cp[iEl])
						min_Cp[iEl] = buffer;
					if(iEl != 0 && buffer > max_Cp[iEl])
						max_Cp[iEl] = buffer;
				}
			switch(use_FileData){
				case 0:
					if(fscanf(matrix_file, "\n") != 0) return 203;
					break;
				case 1:
					break;
			}
			//if prf2 filetype is used, we have to guarantee that we read a new line here
			if(prf_FileType == 1) jline = 5;
		}
	//cout << k << ' ' << iEl << ' ' << j << ' ' << i << endl;
	}

	switch(use_FileData){
		case 0:
			fclose(matrix_file);
			break;
		case 1:
			break;
	}

	//then, we use this max and min values to calculate the number of concentration
	//divisions to be used in the compounds description
	long long int nCp_total=1;
	for(iEl=1; iEl<_prmt->nEl+1; iEl++){
		nCp_quant[iEl] = int((max_Cp[iEl] - min_Cp[iEl]) / _matrix->dCp) + 1;
		nCp_total *= nCp_quant[iEl];
		//cout << nCp_quant[iEl] << ' ' << min_Cp[iEl] << ' ' << max_Cp[iEl] << endl;
	}
	int *Cp_quant = new int[_prmt->nEl+1];
	int *iCp_array = new int[nCp_total+1];
	for(index=0; index<nCp_total+1; index++)
		iCp_array[index] = 0;

	double **new_Cp = new double*[65535];
	for(iCp=1; iCp<65535; iCp++)
		new_Cp[iCp] = new double[_prmt->nEl+1];

	//for last the main loop, where the concentrations by element in each voxel are
	//converted to discrete compounds
	new_nCp = _prmt->nCp;
	//for(k=0;k<_matrix->nz;k++)
	for(k=_matrix->nz-1;k>=0;k--)
		for(j=0;j<_matrix->ny;j++)
			for(i=0;i<_matrix->nx;i++){
				index = 0;
				for(iEl=1; iEl<_prmt->nEl+1; iEl++){
					factor = 1;
					//quantization
					Cp_quant[iEl] = int((row[k*(_prmt->nEl+1) + iEl][j*_matrix->nx + i] - min_Cp[iEl]) / _matrix->dCp);
					//calculate the correspondent compound
					for(jEl=iEl+1; jEl<_prmt->nEl+1; jEl++)
						factor *= nCp_quant[jEl];

					index += Cp_quant[iEl] * factor;
				}
				//check if the compound already exist; if it doesn't, create one
				if(iCp_array[index] == 0 && index != 0){
					new_nCp += 1;
					iCp_array[index] = new_nCp;
					density = 0;
					for(iEl=1; iEl<_prmt->nEl+1; iEl++){
						elem_frac = 1.0*Cp_quant[iEl]*_matrix->dCp + min_Cp[iEl];
						new_Cp[new_nCp][iEl] = elem_frac;
						density += elem_frac / 6.02e23 * 1e24 * _prmt->elem[iEl].M;
					}
					new_Cp[new_nCp][0] = density;
				}
				if(index != 0)Cp_counter += 1;
				_matrix->Cp[i][j][k] = iCp_array[index];
			}
	_matrix->Cp_ratio = 1.0*Cp_counter/(_matrix->nx*_matrix->ny*_matrix->nz);

	//first we need to create the arrays for the new compounds
	STcomp *new_Cp_struct = new STcomp[new_nCp+1];

	//now, we copy the compounds already existent
	for(iCp=1; iCp<_prmt->nCp+1; iCp++){
		//allocate memory for the copy
		//new_Cp_struct[iCp].name = new char[100];
		//new_Cp_struct[iCp].frac_elem = new double[_prmt->nEl];
		//new_Cp_struct[iCp].dedx = new double[_prmt->nEl+1];
		//new_Cp_struct[iCp].dwdx = new double[_prmt->nEl+1];

		new_Cp_struct[iCp].name = _prmt->comp[iCp].name;
		new_Cp_struct[iCp].frac_elem = _prmt->comp[iCp].frac_elem;
		new_Cp_struct[iCp].dedx = _prmt->comp[iCp].dedx;
		new_Cp_struct[iCp].dwdx = _prmt->comp[iCp].dwdx;

		new_Cp_struct[iCp] = _prmt->comp[iCp];
	}

	//and add the new ones
	for(iCp=_prmt->nCp+1; iCp<new_nCp+1; iCp++){
		//allocate memory
		new_Cp_struct[iCp].name = new char[100];
		new_Cp_struct[iCp].frac_elem = new double[_prmt->nEl+1];
		new_Cp_struct[iCp].dedx = new double[_prmt->nEl+1];
		new_Cp_struct[iCp].dwdx = new double[_prmt->nEl+1];

		new_Cp_struct[iCp].name[0] = 'A';
		new_Cp_struct[iCp].name[1] = 'u';
		new_Cp_struct[iCp].name[2] = 't';
		new_Cp_struct[iCp].name[3] = 'o';
		new_Cp_struct[iCp].dwdx_corr = 1;
		new_Cp_struct[iCp].dEdx_corr = 1;
		new_Cp_struct[iCp].nElem = _prmt->nEl;
		new_Cp_struct[iCp].dens = new_Cp[iCp][0];
		for(iEl=0; iEl<_prmt->nEl+1; iEl++){
			new_Cp_struct[iCp].frac_elem[iEl] = new_Cp[iCp][iEl];
			new_Cp_struct[iCp].dedx[iEl] = 0;
			new_Cp_struct[iCp].dwdx[iEl] = 0;
		}
	}

	//for last, we change the prmt->comp pointer to the new allocated memory address
	_prmt->comp = new_Cp_struct;

	//and update the new number of compounds
	_prmt->nCp = new_nCp;

	/*
	//some output codes for debug purposes
	for(iCp=1; iCp<new_nCp+1; iCp++)
		cout << iCp << ' ' << new_Cp[iCp][0] << ' ' << new_Cp[iCp][1] << ' ' << new_Cp[iCp][2] << ' ' << new_Cp[iCp][3] << endl;
	for(k=0;k<_matrix->nz;k++){
		for(i=0;i<_matrix->nx;i++){
			for(j=0;j<_matrix->ny;j++)
				//cout << _matrix->Cp[i][j][k] << ' ';
				cout << row[k*(_prmt->nEl+1) + 1][j*_matrix->nx + i] << ' ';
			cout << endl;
		}
		cout << endl;
	}
	*/

	return 0;
}

/*
int GENERATE_UNIFIED_MATRIX(STprmt *_prmt, STmatrix *_matrix){
	STmatrix *_new_matrix = new STmatrix;

	for(iLa=1; iLa<_prmt->nLa+1; iLa++){

	}

	return 0;
}
*/
