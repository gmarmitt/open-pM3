/*
 * Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef	MSCT
#define	MSCT

#include "msct.h"

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - GET POSITION  - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

void GET_FIRST_POSITION_TRAJ(const STprmt *_prmt, STmatrix *_matrix, STrand *_randgen, STposition *_pos){

	//only works for 1 layer and matrix
	_pos->l = 1;
	_pos->m = _prmt->straj.iMt;

	//current phi rotation
	_pos->phi = _matrix[_pos->m].phi;

	//initial position
	_pos->x = _randgen->randF()*_matrix[_pos->m].nx*_prmt->layer[_pos->l].dL[0];	//randomize in x
	_pos->y = _randgen->randF()*_matrix[_pos->m].ny*_prmt->layer[_pos->l].dL[1];	//randomize in y
	_pos->z = _matrix[_pos->m].nz*_prmt->layer[_pos->l].dL[2]	//top of the matrix
				+ 2*_randgen->randF()*_prmt->simu.dr;			//add a random starting point above surface

	_pos->double_to_index(_prmt);		//generates the index counterparts i,j,k of the x,y,z double

	_pos->lay_type = 1;		//defines the layer type: NP

	//values not needed
	_pos->Cp = 0;
	_pos->El = 0;

	if(_prmt->debug == 2) cout << "pos: i " << _pos->i << " j " << _pos->j << " k " << _pos->k << endl;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - -  SCATTERING INFO  - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

void SCATT_INFO_TRAJ(const STprmt *_prmt, const STtables *_tables, STrand *_randgen, STscatt *_scatt){
	//energy loss due to the collision
	_scatt->E_aft	= _scatt->E_bef * _tables->elem[_scatt->pos.El].K.get(_scatt->theta);
	_scatt->W2_aft	= _scatt->W2_bef * pow(_tables->elem[_scatt->pos.El].K.get(_scatt->theta), 2);
	//cout << _scatt->E_bef << ' ' << _tables->K[_scatt->itheta][_scatt->pos.El] << endl;

	//for electrons, account for the thermal kinectic energy
	if(_prmt->beam.btype == 2){
		double Erec, Ekin;
		Erec = _tables->elem[_scatt->pos.El].T.get(_scatt->theta) * _prmt->beam.E0;
		Ekin = _prmt->elem[_scatt->pos.El].Ekin;
		_scatt->E_aft = _randgen->randG(_scatt->E_aft, sqrt(4.0/3.0*Erec*Ekin));
	}

	if(_prmt->debug == 2)
	cout << "Ebef: " << _scatt->E_bef << " Wbef: " << _scatt->W2_bef << " Eaft: " << _scatt->E_aft << " Waft: " << _scatt->W2_aft << endl;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - EVENT HISTOGRAM - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

double HISTOGRAM_TRAJ_DIRECT(int iThread, const STprmt *_prmt, const STmatrix *_matrix, const STtables *_tables, STrand *_randgen, SThisto *_histo){
	double theta;
	theta = PI-_matrix[_prmt->straj.iMt].traj[0][0][0][iThread].theta;
	if(theta == 0) return 0;

	double theta_min, theta_max;
	theta_min = _prmt->dete.Y0-_prmt->dete.LY/2;
	theta_max = _prmt->dete.Y0+_prmt->dete.LY/2;

	if(theta_min == theta_max){
		theta_min -= _prmt->dete.dY;
		theta_max += _prmt->dete.dY;
	}

	if(theta_min <= theta && theta < theta_max){
		int itheta_histo;
		STenergy energy;
		energy.E_out = _prmt->beam.E0-_matrix[_prmt->straj.iMt].traj[0][0][0][iThread].delta_E;
		energy.W2_out = _matrix[_prmt->straj.iMt].traj[0][0][0][iThread].delta_W2 + _prmt->dete.sig_exp2;
		energy.E = _randgen->randG(energy.E_out, sqrt(energy.W2_out));
		energy.E_to_iE(_prmt, _randgen);

		itheta_histo = 0;
		if(energy.is_inside_spectrum)_histo->simple[energy.iE][itheta_histo] += 1;
		return 1;
	}

	return 0;
}

double HISTOGRAM_TRAJ(const STprmt *_prmt, const STmatrix *_matrix, const STtables *_tables, STrand *_randgen, STpath *_path_IN, STpath *_path_OUT, STscatt *_scatt, STenergy *_energy, SThisto *_histo){
	int i=0, iE, iAn, iEl;
	double F=1, CS, CS_tot=0, CS_correc, gauss_amp, gauss_sigma2;
	STposition *_pos;
	_pos = &_scatt->pos;

	iE = (int)(_scatt->E_bef/_tables->comp[_prmt->nCp].neutr.delta);
	iAn = (int)((_scatt->theta-_tables->elem[_pos->El].CS.initial)/_tables->elem[_pos->El].CS.delta);
	CS_correc = _tables->elem[_pos->El].CS_correc[iAn][iE];

	CS = CS_correc*_tables->elem[_pos->El].CS.get(_scatt->theta)/_scatt->E_bef/_scatt->E_bef;

	for(iEl=1; iEl<=_prmt->nEl; iEl++)
		CS_tot += _prmt->comp[_pos->Cp].frac_elem[iEl] * _tables->elem[iEl].CS_angle_cut.get(_scatt->E_bef);

	if( CS < _tables->elem[_pos->El].CS_angle_cut.get(_scatt->E_bef)*_prmt->straj.CS_frac		//if CS/CS_tot is smaller than the max CS_frac acceptable
		|| _prmt->straj.MS == 0){			//OR if simulation is single scattering, there will be no angle problems

		F *= 	_histo->norm_ssct;						//histogram normalization

		F *= 	_tables->comp[_path_OUT->last_Cp].neutr.get(_energy->E);		//charge neutralization

		//volume of the box where the points are chosen
		//diferential of volume: dV = dL^3
		F *= _prmt->layer[_pos->l].dL[0]*_prmt->layer[_pos->l].dL[1]*_prmt->layer[_pos->l].dL[2];



		if(_prmt->straj.CS_cut_algo == 0){

		if(_scatt->theta > _prmt->straj.angle_min){
			F *= 	CS 										//cross section
					*_prmt->comp[_pos->Cp].dens				//density of the component _pos->Cp
					*_prmt->simu.dr;						//lambda
		}
		else{
			F *= 1 	- 	CS_tot							//total cross section
						* _prmt->comp[_pos->Cp].dens	//density of the component _pos->Cp
						* _prmt->simu.dr;				//lambda
			//F = 0;
			//cout << "\n" << _scatt->theta*180/PI << ' ' << CS*_prmt->comp[_pos->Cp].dens*_prmt->simu.dr << ' ' <<1-CS_tot* _prmt->comp[_pos->Cp].dens* _prmt->simu.dr << endl;
		}

	}
	else if(_prmt->straj.CS_cut_algo == 1 or _prmt->straj.CS_cut_algo == 2){

		gauss_amp = 1 	- 	CS_tot							//total cross section
					* _prmt->comp[_pos->Cp].dens	//density of the component _pos->Cp
					* _prmt->simu.dr;				//lambda

		gauss_sigma2 = pow(_prmt->straj.angle_min, 2);

		if(_scatt->theta < _prmt->straj.angle_min){
			CS = 0;
		}

		F *= 	CS 										//cross section
				*_prmt->comp[_pos->Cp].dens				//density of the component _pos->Cp
				*_prmt->simu.dr						//lambda
				+ gauss_amp / sqrt(gauss_sigma2*2*PI) * exp( - pow(_scatt->theta,2)/2/gauss_sigma2 );

		}



		_scatt->itheta_histo = 0;

		//energy or tof output
		switch(_prmt->outp.tof){
			case 0:
				i = _energy->iE;
				break;
			case 1:
				i = _energy->it;
				break;
		}

		//save this contribution to the histogram
		switch(_prmt->outp.histo_type){
			case 0:
				if(0 <= _scatt->itheta_histo && _scatt->itheta_histo < _prmt->outp.nAn)
					_histo->simple[i][_scatt->itheta_histo] += F;
				break;
			case 1:
				_histo->element[i][_pos->El] += F;
				break;
			case 2:
				_histo->compound[i][_pos->Cp] += F;
				break;
		}

		if(_prmt->debug == 2)
		cout << i << ' ' << "histo: F " << F << endl;

		return F;

		//cout << _scatt->pos.k << ' ' << _scatt->theta*180/PI << endl;
	}
	return 0;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - TRAJECTORY CREATOR  - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

double elastic_nocollision_probability(const STprmt *_prmt, const STtables *_tables, int iCp, double E){
	int iEl;
	double CS_sum=0, eflp_inv;
	for(iEl=1; iEl<_prmt->nEl+1; iEl++)
		CS_sum += _tables->elem[iEl].CS_angle_cut.get(E) * _prmt->comp[iCp].frac_elem[iEl];

	eflp_inv = _prmt->comp[iCp].dens * CS_sum;

	return exp(-_prmt->simu.dr*eflp_inv);
}

void TRAJECTORY_CREATOR(int iThread, const STprmt *_prmt, const STtables *_tables, STrand *_randgen, STposition *_ipos, STvector *_vec, int iPath, double initial_E, STmatrix *_matrix){
	int		iAn, iE, iw, ncollisions;
	long long int iIon=0;
	double 	delta_E=1e-10,			//delta energy acumulator, start at zero
			delta_W2=0,				//projectile total straggling at any given time
			E=1e-10,				//projectile total energy at any given time
			E1=0,					//projectile total energy before collision
			cos_phi,
			displacement=0,			//total length traveled
			dpath=0,				//distance traveled since last colision
			theta_lab=0,			//scattering angle of the colision
			phi,					//azimuthal angle of the colosion
			Erec=0, 				//recoil energy in a deflection
			Ekin;					//kinectic energy of the atom
	double P, iflp, w;

	STposition pos;
	pos = *_ipos;

	STvector vec, vec_old;
	vec = *_vec;

	STtraj *_traj;		//all trajectories at a given position

	vec.x = -vec.x;
	vec.y = -vec.y;
	vec.z = -vec.z;

	//Azimuthal angle - Rotates the vector based in the matrix rotation
	//rotate only the difference between the current vector phi and the last phi
	rotmat_2D(_tables, pos.phi, &vec, &vec);

	//cout << "nano: " << "m " << pos.m << " i " << pos.i << " j " << pos.j << " k " << pos.k << " Cp "<< pos.Cp << " " << vec.z << endl;

	//move until it hits the surface
	while(pos.Cp == 0){
		pos.x += vec.x * _prmt->simu.dr;
		pos.y += vec.y * _prmt->simu.dr;
		pos.z += vec.z * _prmt->simu.dr;

		//keep the projectile inside the matrix boundaries
		while( pos.x >= _matrix[pos.m].nx*_prmt->layer[pos.l].dL[0] )
			pos.x -= _matrix[pos.m].nx*_prmt->layer[pos.l].dL[0];
		while( pos.x < 0 )
			pos.x += _matrix[pos.m].nx*_prmt->layer[pos.l].dL[0];
		while( pos.y >= _matrix[pos.m].ny*_prmt->layer[pos.l].dL[1] )
			pos.y -= _matrix[pos.m].ny*_prmt->layer[pos.l].dL[1];
		while( pos.y < 0 )
			pos.y += _matrix[pos.m].ny*_prmt->layer[pos.l].dL[1];
		pos.double_to_index(_prmt);			//generates the int counterparts i,j,k of the x,y,z doubles

		if(pos.k < _matrix[pos.m].nz)
			pos.Cp = _matrix[pos.m].Cp[pos.i][pos.j][pos.k];
		else
			pos.Cp = 0;
	}

	//surface plasmon
	if(_prmt->calc.dielec_mode != 0) {
		if(_prmt->comp[pos.Cp].surface_plasmon.A > _randgen->randF() && pos.Cp != 0){
			w = _randgen->randG(_prmt->comp[pos.Cp].surface_plasmon.w0, _prmt->comp[pos.Cp].surface_plasmon.gamma);

			//update energy loss based on this inelastic collision
			if(iPath == 1) delta_E += w;
			if(iPath == 2) delta_E -= w;

			//update energy after collision
			E = initial_E-delta_E;
		}
	}

	do{
		if(pos.k < _matrix[pos.m].nz)
			pos.Cp = _matrix[pos.m].Cp[pos.i][pos.j][pos.k];
		else
			pos.Cp = 0;

		if(_prmt->debug == 2) cout << "nano: " << "m " << pos.m << " i " << pos.i << " j " << pos.j << " k " << pos.k << " Cp "<< pos.Cp << " " << vec.z << ' ' << E << endl;

		//energy loss for this step
		//cout << _randgen->randF() <<  ' ' << sqrt(nIon_storage_frac) << endl;
		if( pos.Cp != 0 ){		//if the composition is != then 0 calculate energy loss and straggling

			E = initial_E - delta_E;

			if(iPath == 1) delta_E += _tables->comp[pos.Cp].dedx_E[0].get(E) * _prmt->simu.dr;
			if(iPath == 2) delta_E -= _tables->comp[pos.Cp].dedx_E[0].get(E) * _prmt->simu.dr;
			delta_W2 += _tables->comp[pos.Cp].dwdx_E[0].get(E)*_prmt->simu.dr;

			//dielectric function effect
			while(_prmt->calc.dielec_mode != 0){

				//test to see if the energy value is valid
				iE = (int)((E-_prmt->outp.Emin)/_prmt->calc.dielec_dE);
				if(iE < 0 || iE >= _tables->comp[pos.Cp].P_E.size) break;

				//test to see if there is a collision
				iflp = sqrt( 2*(E/27.211) / (_prmt->beam.M/0.000548) ) / _tables->comp[pos.Cp].P_E.array[iE] * 0.529;
				//number of collision, evaluated by a poisson distribution
				ncollisions = _randgen->randP(_prmt->simu.dr/iflp);

				//for each collision, calculate the energy transferred
				while(ncollisions > 0){
					ncollisions -= 1;
					//calculate a random pair (w, q)
					P = _randgen->randF()*_tables->comp[pos.Cp].P_E.array[iE];
					//find which w based on the accumulated probability chosen
					iw=0; do{ iw++; } while(P > _tables->comp[pos.Cp].P_E_accu[iE][iw]);
					w = (iw+_randgen->randF()-0.5) * _prmt->calc.dielec_dw * 27.211;

					//update energy loss based on this inelastic collision
					if(iPath == 1) delta_E += w;
					if(iPath == 2) delta_E -= w;
				}
				//update energy after collision
				E = initial_E-delta_E;
				break;
			}

			//change direction - MSCT effect
			while(_prmt->straj.MS == 1){
				//test to see if the element is valid
				choose_element_to_scatter_with_CS_weight(_prmt, _tables, _randgen, E, &pos);
				if(pos.El <= 0 || pos.El > _prmt->nEl) break;

				//get a random azimuthal angle for the scattering event
				phi	= 2*PI*_randgen->randF();

				//get a random scattering angle that follows the scattering cross section distribution
				P = _randgen->randF() * _tables->elem[pos.El].CS_angle_cut.get(E);

				//test to see if the energy value is valid
				iE = (int)(
							(E-_tables->elem[pos.El].CS_angle_cut.initial)
							/_tables->elem[pos.El].CS_angle_cut.delta
							);
				if(iE < 0 || iE >= _tables->elem[pos.El].CS_angle_cut.size) break;

				//get the scattering angle form the cross section accumulation function
				iAn=0; do{ iAn++; } while(P > _tables->elem[pos.El].CS_accumulation[iAn][iE]);
				theta_lab = (iAn+_randgen->randF()-0.5)*_tables->elem[pos.El].CS.delta + _tables->elem[pos.El].CS.initial;
				if(theta_lab < 0) theta_lab = 0;

				//energy before the scattering
				if(iPath == 1) E1 = E;
				if(iPath == 2) E1 = E / _tables->elem[pos.El].K.array[iAn];

				//test to see if a collision happened
				if(_randgen->randF() < elastic_nocollision_probability(_prmt, _tables, pos.Cp, E1)) break;

				//nuclear energy loss
				if(iPath == 1) Erec = E * _tables->elem[pos.El].T.array[iAn];
				if(iPath == 2) Erec = E * ( 1 - 1 / _tables->elem[pos.El].K.array[iAn]);

				delta_E			+= Erec;
				//for electrons, account for the thermal kinectic energy
				if(_prmt->beam.btype == 2){
					Ekin = _prmt->elem[pos.El].Ekin;
					delta_E			= _randgen->randG(delta_E, sqrt(4.0/3.0*fabs(Erec*Ekin)));
				}

				//make the rotation
				vec_old = vec;
				vec.x = sin(theta_lab)*cos(phi);
				vec.y = sin(theta_lab)*sin(phi);
				vec.z = cos(theta_lab);
				rotmat_3D(&vec, vec_old);
				vec.make_mod1();

				dpath = 0;

				//update energy after collision
				E = initial_E-delta_E;
				break;
			}

			//get a trajectory slot to store the particle info
			if(iPath == 1) iIon = (long long int) ((_randgen->randF()+iThread)*_prmt->straj.nIon/_prmt->simu.nThreads);
			if(iPath == 2) iIon = (long long int) ((_randgen->randF()+iThread)*99*_prmt->straj.nIon/_prmt->simu.nThreads + _prmt->straj.nIon);

			//if using direct trajectories (similar to TRBS) the whole traj structure is not necessary
			if(_prmt->straj.direct == 1)
				//pointer to the single traj necessary by the direct trajectory algorithm
				_traj = &_matrix[pos.m].traj[0][0][0][iThread];
			else
				//pointer to all trajectories in this position
				_traj = &_matrix[pos.m].traj[pos.i][pos.j][pos.k][iIon];

			_traj->delta_E = delta_E;
			_traj->delta_W2 = delta_W2;
			_traj->E = E;
			//_traj->CS_norm = time_ratio;

			_traj->theta 	= acos(vec.z);

			cos_phi = vec.x/sin(_traj->theta);
			if(-1 <= cos_phi && cos_phi <= +1) _traj->phi = acos(cos_phi);
			else _traj->phi = 2*PI * _randgen->randF();
			if(vec.y < 0) _traj->phi = 2*PI - _traj->phi;

			if(_prmt->debug == 2)
			cout << "ion " << iIon << " dE " << _traj->delta_E << " E " << _traj->E << " vecz " << vec.z << endl;

		}

		//integration step
		dpath += _prmt->simu.dr;
		displacement += _prmt->simu.dr;
		pos.x += vec.x * _prmt->simu.dr;
		pos.y += vec.y * _prmt->simu.dr;
		pos.z += vec.z * _prmt->simu.dr;

		//keep the projectile inside the matrix boundaries
		while( pos.x >= _matrix[pos.m].nx*_prmt->layer[pos.l].dL[0] )
			pos.x -= _matrix[pos.m].nx*_prmt->layer[pos.l].dL[0];
		while( pos.x < 0 )
			pos.x += _matrix[pos.m].nx*_prmt->layer[pos.l].dL[0];
		while( pos.y >= _matrix[pos.m].ny*_prmt->layer[pos.l].dL[1] )
			pos.y -= _matrix[pos.m].ny*_prmt->layer[pos.l].dL[1];
		while( pos.y < 0 )
			pos.y += _matrix[pos.m].ny*_prmt->layer[pos.l].dL[1];
		pos.double_to_index(_prmt);			//generates the int counterparts i,j,k of the x,y,z doubles

	}while(	displacement <= _prmt->straj.max_distance
			&& 0 <= pos.z
			&& pos.z < _matrix[pos.m].nz*_prmt->layer[pos.l].dL[2]
			//&& E > 0
			&& E < _prmt->beam.E0
			);

	//if(_prmt->debug == 2) cout << "path ended" << endl;

}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - CONNECTOR - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

//return true if there is a valid connection, false if not
bool CONNECTOR(const STprmt *_prmt, STmatrix *_matrix, const STtables *_tables, STrand *_randgen, STpath *_path_IN, STpath *_path_OUT, STscatt *_scatt){
	long long int iIon, jIon;
	double Ediff, delta_E1, delta_E2;
	STtraj 	*_trajs;
	STtraj 	traj_IN;
	STtraj 	traj_OUT;

	_trajs = _matrix[_prmt->straj.iMt].traj[_scatt->pos.i][_scatt->pos.j][_scatt->pos.k];

	iIon = (long long int)(1.0*_randgen->randF()*_prmt->straj.nIon);
	jIon = (long long int)(1.0*_randgen->randF()*99*_prmt->straj.nIon) + _prmt->straj.nIon;

	//trajectories in this position
	traj_IN.delta_E 	= _trajs[iIon].delta_E;
	traj_OUT.delta_E 	= _trajs[jIon].delta_E;
	if(traj_IN.delta_E == 0 || traj_OUT.delta_E == 0) return false;

	traj_IN.delta_W2 	= _matrix[_prmt->straj.iMt].traj[_scatt->pos.i][_scatt->pos.j][_scatt->pos.k][iIon].delta_W2;
	traj_IN.phi 		= _matrix[_prmt->straj.iMt].traj[_scatt->pos.i][_scatt->pos.j][_scatt->pos.k][iIon].phi;
	traj_IN.theta 		= _matrix[_prmt->straj.iMt].traj[_scatt->pos.i][_scatt->pos.j][_scatt->pos.k][iIon].theta;
	traj_IN.E 			= _matrix[_prmt->straj.iMt].traj[_scatt->pos.i][_scatt->pos.j][_scatt->pos.k][iIon].E;
	traj_IN.CS_norm		= _matrix[_prmt->straj.iMt].traj[_scatt->pos.i][_scatt->pos.j][_scatt->pos.k][iIon].CS_norm;
	traj_OUT.delta_W2 	= _matrix[_prmt->straj.iMt].traj[_scatt->pos.i][_scatt->pos.j][_scatt->pos.k][jIon].delta_W2;
	traj_OUT.phi 		= _matrix[_prmt->straj.iMt].traj[_scatt->pos.i][_scatt->pos.j][_scatt->pos.k][jIon].phi;
	traj_OUT.theta 		= _matrix[_prmt->straj.iMt].traj[_scatt->pos.i][_scatt->pos.j][_scatt->pos.k][jIon].theta;
	traj_OUT.E 			= _matrix[_prmt->straj.iMt].traj[_scatt->pos.i][_scatt->pos.j][_scatt->pos.k][jIon].E;
	traj_OUT.CS_norm 	= _matrix[_prmt->straj.iMt].traj[_scatt->pos.i][_scatt->pos.j][_scatt->pos.k][jIon].CS_norm;

	//copy algular values from traj to path
	_path_IN->vec.x = sin(traj_IN.theta)*cos(traj_IN.phi);
	_path_IN->vec.y = sin(traj_IN.theta)*sin(traj_IN.phi);
	_path_IN->vec.z = cos(traj_IN.theta);
	_path_OUT->vec.x = sin(traj_OUT.theta)*cos(traj_OUT.phi);
	_path_OUT->vec.y = sin(traj_OUT.theta)*sin(traj_OUT.phi);
	_path_OUT->vec.z = cos(traj_OUT.theta);

	//continue copying the values from traj to path
	delta_E1 = _randgen->randG(0, sqrt(traj_IN.delta_W2));
	_path_IN->delta_E = traj_IN.delta_E + delta_E1;
	_path_IN->delta_W2 = 0;
	_path_IN->ipos = _scatt->pos;

	delta_E2 = _randgen->randG(0, sqrt(traj_OUT.delta_W2));
	_path_OUT->delta_E = -traj_OUT.delta_E + delta_E2; //CARE WITH THE MINUS SIGNAL!!!
	_path_OUT->delta_W2 = 0;
	_path_OUT->ipos = _scatt->pos;
	_path_OUT->last_Cp = _scatt->pos.Cp;

	//scattering angle - between the vector of this path and the vector of the last path
	_scatt->theta 	= angle_between_vec(_tables, _path_IN->vec, _path_OUT->vec);
	_scatt->theta 	= PI - _scatt->theta;
	_scatt->E_bef 	= traj_IN.E;
	_scatt->E_aft 	= traj_OUT.E;
	_scatt->CS_norm = traj_IN.CS_norm * traj_OUT.CS_norm;

	//difference of energy at the connection
	Ediff = (traj_IN.E-delta_E1)*_tables->elem[_scatt->pos.El].K.get(_scatt->theta) - (traj_OUT.E-delta_E2);

	if(fabs(Ediff) < _prmt->straj.dE)
		return true;
	else
		return false;

}

#endif
