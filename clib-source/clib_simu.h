/*
 * Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef	CLIB_SIMU_H
#define	CLIB_SIMU_H

#include "structs.h"

#include <ctime>
#include <random>
#include <iostream>
#include <fstream>

//random array storage size
//there is little gain for storages bigger than 1e3
#define RAND_STORAGE_SIZE 1000

using namespace std;

//definitions
//const double PI = 4.0 * atan (1.0);

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

struct STrand{			//rand object used to create and call random sequences

	//random sequence and distributions - called in func.cpp's randF() and randG(avrg, sig)
	mt19937 mt19937_sequence;

	//rand index
	int randF_index;
	int randG_index;

	//rand storage for posterior use
	double randF_storage[RAND_STORAGE_SIZE+1];
	double randG_storage[RAND_STORAGE_SIZE+1];

	//get a uniform distributed [0,1[ double
	double randF(){
		randF_index += 1;
		if(randF_index == RAND_STORAGE_SIZE)
			fill_randF_storage();
		return randF_storage[randF_index];
	}

	//get a normal distributed double with average 'avrg' and variance 'sig'
	double randG(double avrg, double sig){
		randG_index += 1;
		if(randG_index == RAND_STORAGE_SIZE)
			fill_randG_storage();
		return avrg + sig*randG_storage[randG_index];
	}

	//get a poisson distributed double with average 'avrg'
	int randP(double avrg){
		poisson_distribution <int> poisson_rand(avrg);
		return poisson_rand(mt19937_sequence);
	}

	//fill the randF storage array
	void fill_randF_storage(){
		int i;
		randF_index = 0;
		uniform_real_distribution <double> uniform_rand(0,1);
		for(i=0; i<=RAND_STORAGE_SIZE; i++)
			randF_storage[i] = uniform_rand(mt19937_sequence);
	}

	//fill the randG storage array
	void fill_randG_storage(){
		int i;
		randG_index = 0;
		normal_distribution <double> normal_rand(0,1);
		for(i=0; i<=RAND_STORAGE_SIZE; i++)
			randG_storage[i] = normal_rand(mt19937_sequence);
	}

};

struct STposition{					//Position object used in simulations
	int i;
	int j;
	int k;
	int l;
	int m;
	int lay_type;
	int Cp;
	int El;
	double x;
	double y;
	double z;
	double phi;

	void index_to_double(const STprmt *_prmt){		//generates the double counterparts x,y,z of the i,j,k index
		x = i * _prmt->layer[l].dL[0];
		y = j * _prmt->layer[l].dL[1];
		z = k * _prmt->layer[l].dL[2];
	}

	void double_to_index(const STprmt *_prmt){		//generates the int counterparts i,j,k of the x,y,z doubles
		i = floor(x / _prmt->layer[l].dL[0]);
		j = floor(y / _prmt->layer[l].dL[1]);
		k = floor(z / _prmt->layer[l].dL[2]);
	}

	void clean(){
		i 	= 0;
		j 	= 0;
		k 	= 0;
		l 	= 0;
		m 	= 0;
		lay_type = 0;
		Cp 	= 0;
		El 	= 0;
		x 	= 0;
		y 	= 0;
		z 	= 0;
		phi = 0;
	}
};

struct STprojectile{
	int		El_col;		// =0 is beam element; !=0 is the corresponding element as difned by prmt.elem[]
	int		Z;
	double	M;

	void clean(){
		El_col = 0;
		Z = 0;
		M = 0;
	}
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct STpath{						//Path object, stores integration values
	STposition	ipos;		//position of the scattering - initial position
	STposition	fpos;		//position of the next scattering - final position
	STvector	vec;		//vector connecting ipos and fpos
	double		vec_mod;	//squared module of vec

	double	delta_E;		//medium energy loss during the total_distance path
	double	delta_W2;		//straggling energy loss during the total_distance path
	double	eRBS_dP;
	double	total_prob;
	double	displacement;
	double	sig_cexpl;

	short	*history;		//array storing all the iCp values after each integration step
	short	history_size=0;	//size of the history array

	short	is_inward_path=false;	//boolean value to tag as inward or outward path
	short last_Cp; //last compound the ion passed

	STprojectile projectile;

	void clean(){
		ipos.clean();
		fpos.clean();
		vec.clean();
		vec_mod	= 0;

		delta_E 			= 0;
		delta_W2 			= 0;
		eRBS_dP 			= 0;
		total_prob		= 0;
		displacement	= 0;
		sig_cexpl			= 0;
		last_Cp				= 0;

		projectile.clean();

		int i;
		for(i=0; i<history_size; i++)history[i] = 0;
	}
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct STscatt{			//Scattering object, stores information for each collision
	STposition	pos;		//scattering position
	STposition	rpos;		//scattering position - relative to last collision: _scatt[itheta-1].pos

	double		theta;			//scattering angle_lab for the collision at fpos
	double		phi;			//recoil angle_lab for the collision at fpos
	int			itheta_histo;	//scattering angle_lab - histogram index counterpart
	double		theta_cm;		//scattering angle_cm for the collision at fpos
	int			itheta_cm;		//scattering angle_cm - tables.cross.cm index counterpart
	double		E_bef;			//energy immediately before scattering
	double		E_aft;			//energy immediately after scattering
	double		W2_bef;			//straggling immediately before scattering
	double		W2_aft;			//straggling immediately after scattering

	double		CS_norm=1;

	void theta_to_itheta_histo(const STprmt *_prmt, STrand *_randgen){
		itheta_histo = floor((theta - _prmt->dete.theta_min)/_prmt->outp.dAngle + _randgen->randF() );
	}
	void phi_to_itheta_histo(const STprmt *_prmt, STrand *_randgen){
		itheta_histo = floor((phi - _prmt->dete.theta_min)/_prmt->outp.dAngle + _randgen->randF() );
	}

	void clean(){
		pos.clean();
		theta 		= 0;
		E_bef 		= 0;
		E_aft 		= 0;
		W2_bef 		= 0;
		W2_aft 		= 0;
	}
};

struct STpsctInfo{	//Scattering object, stores information for each collision
	double	nI;			//
	int		npos_total;		//total number of scattering positions chosen

	void clean(){
		nI 			= 0;
		npos_total 	= 0;
	}
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct STenergy{		//Final energy object, stores information about final energy of the whole event
	double		E_out;		//average energy of the ion when it reach the surface
	double		W2_out;		//straggling of the ion when it reach the surface
	double		Eg;			//gaussian distribution of E_out by the straggling adquired in the path
	double		Es;			//??
	double		E;			//final energy - double
	int			iE;			//final energy - index
	double		t;			//final time - double
	int			it;			//final time - index
	double 	neutr;
	double		tof_jacob;
	bool		is_inside_spectrum=true;	//flag to determine if the energy/time is inside the spectrum window

	void E_to_iE(const STprmt *_prmt, STrand *_randgen){
		//MUST be floor() because int() makes the following error:
		//int(-0.1) = int(0.1) = 0, doubling the 0 index contribution
		iE = floor((E - _prmt->outp.Emin)/_prmt->outp.dEnergy + _randgen->randF()-0.5 );
		if(iE < 0 || _prmt->outp.nEn <= iE) is_inside_spectrum = false;
	}
	void t_to_it(const STprmt *_prmt, STrand *_randgen){
		it = floor((t - _prmt->outp.Tmin)/_prmt->outp.dTime + _randgen->randF()-0.5 );
		if(it < 0 || _prmt->outp.nTm <= it) is_inside_spectrum = false;
	}

	void clean(){
		E_out 	= 0;
		W2_out 	= 0;
		Eg 		= 0;
		Es 		= 0;
		E 		= 0;
		iE 		= 0;
		neutr 	= 1;
		tof_jacob = 1;
		is_inside_spectrum = true;
	}
};

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

//must be after the definitions of these structs
#include "func.cpp"
#include "ssct.cpp"
#include "msct.cpp"

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

//external functions
extern "C"{
	int TRAJ_DIRECT(int, long int*, const STprmt*, STmatrix*, const STtables*, SThisto*);
	int TRAJ_FILL(int, long int*, const STprmt*, STmatrix*, const STtables*);
	int TRAJ_CONNECT(int, long int*, const STprmt*, STmatrix*, const STtables*, SThisto*);
	int SIMU_SSCT(int, long int*, const STprmt*, const STmatrix*, const STtables*, SThisto*);
	int SIMU_ERDA(int, long int*, const STprmt*, const STmatrix*, const STtables*, SThisto*);
	int SIMU_NRP(int, long int*, const STprmt*, const STmatrix*, const STtables*, SThisto*);
}

#endif
