/*
 * Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef	FUNC_H
#define	FUNC_H

#include "structs.h"

//#include <iostream>
//#include <fstream>
//#include <cmath>
//#include <ctime>
//#include <random>

using namespace std;

//definitions
const double PI = 4.0 * atan (1.0);

//functions
void GET_VECTOR_IN(const STprmt*, STvector*);
void GET_VECTOR_OUT(const STprmt*, STrand*, STvector*);
void GET_VECTOR_BETWEEN(const STprmt*, STscatt*, STscatt*, STpath*);

void SET_PROJECTILE_MEIS(const STprmt*, STpath*);
void SET_PROJECTILE_ERDA(const STprmt*, STscatt*, STpath*);
void SET_NRP_VARS(const STprmt*, STtables*, STscatt*, STpath*, STpath*, STrand*, STenergy*);

void SET_IPOS_FPOS(const STprmt*, STscatt*, STpath*);
void SET_IPOS_FPOS_CUDA(const STprmt*, STscatt*, STpath*);

//void PRINT_DISPLACEMENT(STpath*, STpath*, STscatt*, ofstream);

#endif
