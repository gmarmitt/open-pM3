/*
 * Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef STRUCTS_H
#define	STRUCTS_H

#include <cmath>

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - -  PRMTS  - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

struct STvector{
	double x;
	double y;
	double z;

	void clean(){
		x = 0;
		y = 0;
		z = 0;
	}

	void make_mod1(){
		double mod;
		mod = 1/sqrt(x*x + y*y + z*z);
		x *= mod;
		y *= mod;
		z *= mod;
	}

};

struct STarray{
	double 	*array;
	double 	initial;
	double 	delta;
	int		size;

	/*
	double get(STrand *_randgen, double value){
		int i;
		i = floor((value - initial)/delta + _randgen->randF());
		return array[i];
	}
	*/
	double get(double value) const{
		int i;
		i = (int)((value - initial)/delta);
		if(i>=0 && i<size) return array[i];
		else return 0;
	}
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct	STplas{		//plasmon parameters
	double	A;
	double	w0;
	double	gamma;
};

struct	STbstate{		//bound state parameters
	int 	mode;
	int 	n;
	int 	l;
	double 	Ne;
	double	Ib;
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct	STelem{		//elements parameters
	char	*name;
	char	*CS_file;
	int		Z;
	double	M;
	double	sig;
	double	Ekin;
	double	Er_mean;	//ressonance energy mean
	double	Er_width;	//ressonance energy std deviation
	STbstate	*boundstate;
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct	STcomp{		//compositions parameters
	char	*name;
	double	dens;
	int		nElem;
	double	eRBS_dPdx;
	double	*dedx;
	double	*dwdx;
	double	dwdx_corr;
	double	dEdx_corr;
	double	*frac_elem;
	STplas	surface_plasmon;
	STplas	*plasmon;
	char 		*VEGAS_file;
	int			nCrystals;
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct	STlayer{	//layer description
	int		lay_type;		//layer type: 'nano=1' or 'film=0'
	double	thick;			//layer total thickness
	double	*dL;			//dL's for this layer
	int		period_cont;	//periodic contourn: 'yes=1' or 'no=0'
	int		period_posi;	//periodic matrix position: 'yes=1' or 'no=0'
	int		Cp;				//default composition
	int		nMt;			//number of matrix - INDEXED PER LAYER
	double	*Mt_weight;		//statisticas weight of each matrix - INDEXED GLOBALY
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct	STsimu{		//simulation parameters
	int		algorithm;			//simulation algorithm: '0' std, '1' traj
	int		surf_apprx;			//surface approximation: '0' off, 1 'on'
	int		autofit;			//autofit switch: 'on=1' or 'off=0'
	int		nThreads;			//number of threads
	long long int	nI;					//number of interactions
	long long int	nI_per_thread;		//number of interactions per thread
	int		line_shape;			//line shape: 'EMG=1' or 'gauss=0'
	int		cross_section;		//cross section: 'Ziegler=1' or 'Rutherford=0'
	double	dL;					//length of each little cube used
	double 	dr;					//length used in the path integration
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct STcexpl{		//coulomb explosion parameters
	double	stopping_ratio;		//energy loss ratio for molecules
	double	gamma;				//straggling for molecules
	double	alpha;				//correction variable for deep gammas
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct	STcalc{		//calculation parameters
	int		stopp_mode;
	int		strag_mode;
	int		neutr_mode;
	int		dielec_mode;
	double	stopp_dE;
	double	neutr_dE;
	double	dielec_dE;
	double	dielec_dw;
	double	dielec_dq;
	double	dielec_wmax;
	double	dielec_qmax;
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct	SToutp{		//output parameters
	double	Emin;				//initial value
	double	Emax;				//final value
	double	Tmin;				//initial value
	double	Tmax;				//final value
	double	dEnergy;			//value step
	double	dAngle;				//angle step
	double	dTime;				//time step
	int		nEn;				//number of steps in output
	int		nAn;				//number of steps in output
	int		nTm;				//number of steps in output
	int		tof;				//output in time (ns) scale
	int		histo_type;			//histogram output options
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct	STbeam{		//beam parameters
	int		btype;				//beam type: '0' ion, '1' molecule, '2' electron
	int		Z;					//atomic number
	double	M;					//atomic mass
	double	Q;					//charge
	double	E0;					//energy
	double	prtcl_sr;			//total charge deposited
	double	T1;					//incident angle
	STvector vec;				//vector pointing along the beam direction
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct	STdete{		//detector parameters
	double	sig_exp2;			//sigma^2 of the experimental resolution
	double	sig_exp2_TOF;		//sigma^2 of the experimental TOF resolution
	int		det_type;			//detector type: 'eletrostatic=0' or 'TOF=1'
	double	LX;					//delta 'out of plane'
	double	X0;					//horizontal central position
	double	dX;					//angle 'out of plane' step
	double	LY;					//delta 'on plane'
	double	Y0;					//vertical central position
	double	dY;					//angle 'on plane' step
	double	r;					//circular detector radius
	double	d_tof;				//tof distance
	STvector vec;				//vector pointing to the center of the detector (X0, Y0)
	int 	nthetax;			//number of steps 'out of plane'
	int 	nthetay;			//number of steps 'on plane'
	double 	theta_min;			//min angle 'on plane'
	double 	theta_max;			//max angle 'on plane'
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct	STssct{		//single scattering parameters
	double	NS_cut;			//cross section calculation's angle cut
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct	STstraj{		//trajectory simulation parameters
	int				MS;
	int				direct;
	int				memory;
	int				iMt;
	int				CS_cut_algo;
	long long int	nI;
	long long int	nI_per_thread;
	long long int 	nIon;
	double 			max_distance;
	double 			dE;
	double			angle_min;
	double			angle_max;
	double			CS_frac;
	double			eflp;
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct	STsnrp{			//NRP simulation parameters
	double	Ei;			//initial scan energy
	double	Ef;			//final scan energy
};

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

struct STprmt{
	int		debug;
	int		log;
	int		nEl;
	int		nCp;
	int		nLa;
	int		nMt;
	int		nPl;
	int		nBS;
	double	thick;
	STsimu	simu;
	STcexpl	cexpl;
	STcalc	calc;
	SToutp	outp;
	STbeam	beam;
	STdete	dete;
	STssct	ssct;
	STstraj	straj;
	STsnrp	snrp;
	STelem	*elem;
	STcomp	*comp;
	STlayer	*layer;
};

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

struct STtraj{
	double theta;
	double phi;
	double delta_E;
	double delta_W2;
	double E;
	double CS_norm;
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct STmatrix{
	char	*FileName;
	char	*FileData;
	char	*mtx_type;
	char 	*matgen_name; //matrix generator name
	char 	*matgen_attr; //matrix generator attributes
	int		nx;
	int		ny;
	int		nz;
	double	phi;
	double	dCp;
	double	Cp_ratio;
	int		rand_phi;
	unsigned short ***Cp;
	double	***theta_avrg;
	double	***theta_devi;
	STtraj	****traj;
};

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

struct STcomptab{
	double **P_wq;		//inelastic energy loss probability (without delta)
	STarray P_E;		//integral of P_wq for all dq and dw (with delta)
	double **P_E_accu;	//accumulation function of P_E with respect to w
	STarray	*dedx_E;
	STarray	*dwdx_E;
	STarray	neutr;			//charge neutralization fraction
	STarray *visibility;  //crystaline visibility, by angle and depth
};

struct STelemtab{
	STarray K;					//kinemectic factor
	STarray T;					//transmission factor (T = 1-K)
	STarray CS;					//scattering cross section
	STarray CS_total;			//total cross section for colisions in this element
	STarray CS_angle_cut;		//total cross section in (angle_cut, 180)
	double **CS_accumulation;	//total cross section accumulation
	double **CS_correc;		//scattering cross section
};

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

struct STtables{
	STcomptab *comp;		//compounds vector
	STelemtab *elem;		//elements vector
};

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

struct SThisto{
	double	norm_ssct;
	double	**simple;
	double	**element;
	double	**compound;
};
#endif
