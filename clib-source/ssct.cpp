/*
 * Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef	SSCT
#define	SSCT

#include "ssct.h"

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - GET POSITION  - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

int GET_FIRST_POSITION(const STprmt *_prmt, const STmatrix *_matrix, STrand *_randgen, STscatt *_scatt){
	STposition pos;
	double z_rand;

	z_rand = _randgen->randF() * _prmt->thick;		//chooses one z inside the sample
	pos.l = which_layer(_prmt, z_rand);				//get the layer from this z
	if(pos.l <= 0 || pos.l > _prmt->nLa) return 100;	//range check

	switch(_prmt->layer[pos.l].lay_type){
	case 0:									//if layer is a film
		pos.m = 0;
		pos.phi = 0;

		pos.x = 0;
		pos.y = 0;
		pos.z = _randgen->randF()*_prmt->layer[pos.l].thick;		//z is randomized again

		pos.double_to_index(_prmt);		//generates the index counterparts i,j,k of the x,y,z double

		pos.lay_type = 0;		//defines the layer type: film

		pos.Cp = _prmt->layer[pos.l].Cp;		//Cp is equal to the layer Cp
		break;

	case 1:									//if layer is a NP
		choose_matrix_in_layer(_prmt, _randgen, &pos);		//chooses one matrix inside the layer
		if(pos.m <= 0 || pos.m > _prmt->nMt) return 101;	//range check

		//current phi rotation
		pos.phi = _matrix[pos.m].phi;

		pos.x = _randgen->randF()*_matrix[pos.m].nx*_prmt->layer[pos.l].dL[0];		//randomize in x
		pos.y = _randgen->randF()*_matrix[pos.m].ny*_prmt->layer[pos.l].dL[1];		//randomize in y
		//pos.z = _randgen->randF()*_matrix[pos.m].nz*_prmt->layer[pos.l].dL[2];		//randomize in z

		pos.z = _randgen->randF()*_prmt->layer[pos.l].thick;		//z is randomized again

		//cout << pos.z << ' ' << _prmt->layer[pos.l].thick << ' ' << _matrix[pos.m].nz*_prmt->layer[pos.l].dL[2] << endl;
		//cout << "pos: x " << pos.x << " y " << pos.y << " z " << pos.z << " l " << pos.l << " m " << pos.m << " Cp " << pos.Cp << " El " << pos.El << endl;
		if(pos.z < _matrix[pos.m].nz*_prmt->layer[pos.l].dL[2]){
			pos.double_to_index(_prmt);		//generates the index counterparts i,j,k of the x,y,z double
			if(pos.i < 0 || pos.i >= _matrix[pos.m].nx) return 102;	//range check
			if(pos.j < 0 || pos.j >= _matrix[pos.m].ny) return 103;	//range check
			if(pos.k < 0 || pos.k >= _matrix[pos.m].nz) return 104;	//range check

			pos.lay_type = 1;		//defines the layer type: NP
			pos.Cp = _matrix[pos.m].Cp[pos.i][pos.j][pos.k];		//Cp got from matrix position
		}
		else{
			pos.Cp = 0;
		}

		break;
	}

	if(pos.Cp > 0 && pos.Cp <= _prmt->nCp){
		choose_element_to_scatter(_prmt, _randgen, &pos);
		if(pos.El <= 0 || pos.El > _prmt->nEl) return 105;	//range check
	}

		//goes to the next collision
	_scatt->pos = pos;

	if(_prmt->debug == 2)
	cout << "pos: i " << pos.i << " j " << pos.j << " k " << pos.k << " l " << pos.l << " m " << pos.m << " Cp " << pos.Cp << " El " << pos.El << endl;
	//cout << "pos: x " << pos.x << " y " << pos.y << " z " << pos.z << " l " << pos.l << " m " << pos.m << " Cp " << pos.Cp << " El " << pos.El << endl;
	return 0;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - -  SCATTERING INFO  - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

void SCATT_INFO(const STprmt *_prmt, const STtables *_tables, STrand *_randgen, STpath *_path_IN, STpath *_path_OUT, STscatt *_scatt){
	double angle;

	//scattering angle - between the vector of this path and the vector of the last path.
	//There are two possibilities:
	//-- final position from 1 equals the initial position of 2:  |---1--->X|---2--->
	//-- initial position from 1 equal the initial position of 2: <---1---|X|---2--->
	//In the second case we need to change the scattering angle accordingly.
	angle = angle_between_vec(_tables, _path_IN->vec, _path_OUT->vec);
	if(	_path_IN->ipos.i == _path_OUT->ipos.i &&
		_path_IN->ipos.j == _path_OUT->ipos.j &&
		_path_IN->ipos.k == _path_OUT->ipos.k &&
		_path_IN->ipos.l == _path_OUT->ipos.l &&
		_path_IN->ipos.m == _path_OUT->ipos.m)
			angle 	= PI - angle;

	//in the case of ERDA simulations the angle is phi, not theta. Here we calculate theta_lab from phi_lab
	if(_prmt->simu.algorithm == 2){
		double theta_cm;
		theta_cm = PI - 2*angle;
		_scatt->phi 	= angle;
		_scatt->theta 	= theta_cm_to_lab(_tables, _prmt->beam.M, _prmt->elem[_scatt->pos.El].M, theta_cm);
		}
	//in all other cases angle is theta
	else
		_scatt->theta 	= angle;

	//energy loss due to the collision
	if(_prmt->simu.algorithm == 2){
		_scatt->E_aft	= _scatt->E_bef * _tables->elem[_scatt->pos.El].K.get(_scatt->phi);
		_scatt->W2_aft 	= pow(_tables->elem[_scatt->pos.El].T.get(_scatt->theta), 2) * _scatt->W2_bef;
	}
	else{
		_scatt->E_aft	= _scatt->E_bef * _tables->elem[_scatt->pos.El].K.get(_scatt->theta);
		_scatt->W2_aft 	= pow(_tables->elem[_scatt->pos.El].K.get(_scatt->theta), 2) * _scatt->W2_bef;
	}

	if(_prmt->debug == 2)
	//cout << _scatt->phi*180/PI << ' ' << _scatt->pos.El << ' ' << _tables->elem[_scatt->pos.El].K.get(_scatt->theta) << endl;
	cout << "Ebef: " << _scatt->E_bef << " Wbef: " << _scatt->W2_bef << " Eaft: " << _scatt->E_aft << " Waft: " << _scatt->W2_aft << endl;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

void PATH_INFO(const STprmt *_prmt, STrand *_randgen, STpath *_path, STscatt *_scatt, double E_old, double W2_old){
	//if using molecular energy loss and straggling
	if(_prmt->beam.btype == 1){
		//difference in stopping power
		_path->delta_E *= _prmt->cexpl.stopping_ratio;
		//coulomb explosion: sigc(x) = gamma*x/(1+alpha*x) and the dE is given by
		//dE_cexpl = {-sqrt(3)*sigc(x) ... 0 ... +sqrt(3)*sigc(x)} uniform distribution
		_path->delta_E += 2*(_randgen->randF() - 0.5) * sqrt(3) * _prmt->cexpl.gamma * _path->displacement/(1 + _prmt->cexpl.alpha*_path->displacement);
	}

	//energy loss in the segment - before the collision
	_scatt->E_bef 	= E_old 	- _path->delta_E;
	_scatt->W2_bef	= W2_old	+ _path->delta_W2;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

void PATH_INFO_LAST(const STprmt *_prmt, STpath *_path_OUT, STscatt *_scatt, STenergy *_energy){
	//the last segment of energy loss needs to be acconted + detector resolution
	_energy->E_out		= _scatt->E_aft 	- _path_OUT->delta_E;
	_energy->W2_out		= _scatt->W2_aft 	+ _path_OUT->delta_W2 + _prmt->dete.sig_exp2;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - EVENT FINAL ENERGY  - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

void EVENT_ENERGY(const STprmt *_prmt, const STtables *_tables, STrand *_randgen, STpath *_path_OUT, STscatt *_scatt, STenergy *_energy){
	//gaussian distribution of energy loss, based on the energy straggling adquired in the path + detector resolution
	_energy->Eg = _randgen->randG(_energy->E_out, sqrt(_energy->W2_out));

	//line shape contribution
//	if(_prmt->simu.line_shape == 1 && 1/_prmt->elem[_scatt->pos.El].sig * sqrt(_energy->W2_out) < 5)  //only important for analytical EMG function

	if(_prmt->simu.line_shape == 1)
		_energy->Es = -log(1-_randgen->randF()) / (1/_prmt->elem[_scatt->pos.El].sig) - _prmt->elem[_scatt->pos.El].sig;
	else
		_energy->Es = 0;

	_energy->E = _energy->Eg - _energy->Es;

	//type of detector: 0 - eletrostatic; 1 - TOF;
	switch(_prmt->dete.det_type){
	case 0:
		break;
	case 1:
		//time resolution evaluation
		if(_energy->E >= 0){
			//correspondent time (ns) to this energy
			_energy->t = _prmt->dete.d_tof * sqrt((_path_OUT->projectile.M/1e3/6.02e23) / (2*_energy->E*1.6e-19)) * 1e9;

			//time resolution of the detector
			_energy->t = _randgen->randG(_energy->t, sqrt(_prmt->dete.sig_exp2_TOF));

			//correspondent energy (eV) to this time (ns) --> [t/1e9] = sec
			_energy->E = pow(_prmt->dete.d_tof/(_energy->t/1e9), 2) * (_path_OUT->projectile.M/1e3/6.02e23) / 2 / 1.6e-19;
		}
		else 		//guarantees E<0 ions will be discarted
			_energy->t = -1;
		break;
	}

	//energy output - default
	_energy->E_to_iE(_prmt, _randgen);

	//time of flight output
	if(_prmt->outp.tof == 1){
		_energy->t_to_it(_prmt, _randgen);
		if(_energy->t > 0){
			_energy->tof_jacob = (_path_OUT->projectile.M/1e3/6.02e23) * pow(_prmt->dete.d_tof, 2) / pow(_energy->t*1e-9, 3);
		}
		else{
			_energy->tof_jacob = 0;
		}
	}

	_energy->neutr = _tables->comp[_path_OUT->last_Cp].neutr.get(_energy->E);

	if(_prmt->debug == 2)
		cout << "energy: E_out " << _energy->E_out << " W2_out " << _energy->W2_out << " E " << _energy->E << " iE " << _energy->iE << " it " << _energy->it << endl;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - EVENT HISTOGRAM - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

void HISTOGRAM_SSCT(const STprmt *_prmt, const STmatrix *_matrix, const STtables *_tables, STrand *_randgen, STscatt *_scatt, STenergy *_energy, SThisto *_histo){
	int i=0, iE, iAn;
	double F=1, CS_correc;
	STposition *_pos;
	_pos = &_scatt->pos;

	iE = (int)(_scatt->E_bef/_tables->comp[_prmt->nCp].neutr.delta);
	iAn = (int)((_scatt->theta-_tables->elem[_pos->El].CS.initial)/_tables->elem[_pos->El].CS.delta);
	CS_correc = _tables->elem[_pos->El].CS_correc[iAn][iE];

	F *= 	_histo->norm_ssct;						//histogram normalization

	F *= 	_prmt->comp[_pos->Cp].dens				//density of the component _pos->Cp
			* _energy->neutr;		//charge neutralization

	F *= _energy->tof_jacob;						//tof jacobian term; always 1 in energy spectra

	//cross section
	F *=	_tables->elem[_pos->El].CS.get(_scatt->theta) / 	//(Rutherford)*E^2 of the collision
			pow(_scatt->E_bef, 2)*					//be divided by E^2 (to get the correct cross section).
			CS_correc;								//screening correction

	//calculates the index for the angle histogram
	if(_prmt->simu.algorithm == 2) _scatt->phi_to_itheta_histo(_prmt, _randgen);
	else _scatt->theta_to_itheta_histo(_prmt, _randgen);

	//energy or tof output
	switch(_prmt->outp.tof){
		case 0:
			i = _energy->iE;
			break;
		case 1:
			i = _energy->it;
			break;
	}

	//save this contribution to the histogram
	switch(_prmt->outp.histo_type){
		case 0:
			if(0 <= _scatt->itheta_histo && _scatt->itheta_histo < _prmt->outp.nAn)
				_histo->simple[i][_scatt->itheta_histo] += F;
			break;
		case 1:
			_histo->element[i][_pos->El] += F;
			break;
		case 2:
			_histo->compound[i][_pos->Cp] += F;
			break;
	}

	if(_prmt->debug == 2) cout << "histo: F " << F << endl;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

void HISTOGRAM_NRP(const STprmt *_prmt, const STmatrix *_matrix, const STtables *_tables, STrand *_randgen, STscatt *_scatt, STenergy *_beamEnergy, SThisto *_histo){
	double F=1, E_out;

	STposition 	*_pos;
	_pos = &_scatt->pos;

	//gaussian distribution of energy loss, based on the energy straggling adquired in the path
	E_out = _randgen->randG(_scatt->E_bef, sqrt(_scatt->W2_bef + _prmt->dete.sig_exp2));

	//if outside of the selected spectrum window: exit histogram
	if(_beamEnergy->iE >= 0 && _beamEnergy->iE <= _prmt->outp.nEn-1){	//range check

		F *= 	_histo->norm_ssct;						//histogram normalization

		F *= 	_prmt->comp[_pos->Cp].dens;				//density of the component _pos->Cp

		//cross section
		F *= _prmt->elem[_pos->El].Er_width / PI
				/	(
					_prmt->elem[_pos->El].Er_width*_prmt->elem[_pos->El].Er_width +
						(E_out-_prmt->elem[_pos->El].Er_mean)*(E_out-_prmt->elem[_pos->El].Er_mean)
					);

		//calculates the index for the angle histogram
		_scatt->theta_to_itheta_histo(_prmt, _randgen);

		//save this contribution to the histogram
		switch(_prmt->outp.histo_type){
			case 0:
				if(0 <= _scatt->itheta_histo && _scatt->itheta_histo < _prmt->outp.nAn)
					_histo->simple[_beamEnergy->iE][_scatt->itheta_histo] += F;
				break;
			case 1:
				_histo->element[_beamEnergy->iE][_pos->El] += F;
				break;
			case 2:
				_histo->compound[_beamEnergy->iE][_pos->Cp] += F;
				break;
		}

		if(_prmt->debug == 2) cout << "histo: F " << F << endl;
	}
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - -  SINGLE SCATTERING  - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

void PATH_STRAIGHT_DISTANCE(const STprmt *_prmt, const STmatrix *_matrix, const STtables *_tables, STrand *_randgen, int energy_loss_index, STpath *_path){

	int			Cp_old;
	double 		dx, final_pos_z, last_layer_phi=0, sum_of_distance=0;

	STvector	vec;			//vector modified along the integration
	vec = _path->vec;

	STposition 	pos;			//position modified along the integration
	pos = _path->ipos;

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

	do{		//loop for layers

		//randomize the phi if rand_phi option is used
		if(_matrix[pos.m].rand_phi == 1)
			pos.phi = _randgen->randF() * 2 * PI;

		//Azimuthal angle - Rotates the vector based in the matrix rotation
		//rotate only the difference between the current vector phi and the last phi
		rotmat_2D(_tables, pos.phi - last_layer_phi, &vec, &vec);
		//store this layer phi rotation, to correctly change the rotation in the beginning of the next layer
		last_layer_phi = pos.phi;

		//this section calculates the thickness of the next segment
		if(pos.l == _path->fpos.l)	//if in the last layer
			final_pos_z = _path->fpos.z;				//the final k is equal to the FinalPos k
		else				//if not, the final k is:
			if(vec.z > 0)
				final_pos_z = _prmt->layer[pos.l].thick;			//if in film, the final k is equal to the thickness
			else
				final_pos_z = 0;

		if(_prmt->debug == 2) cout << "ipos.x " << _path->ipos.x << " ipos.y " << _path->ipos.y << " ipos.z " << _path->ipos.z << " ipos.l " << _path->ipos.l << endl;
		if(_prmt->debug == 2) cout << "fpos.x " << _path->fpos.x << " fpos.y " << _path->fpos.y << " fpos.z " << _path->fpos.z << " fpos.l " << _path->fpos.l << endl;
		if(_prmt->debug == 2) cout << "fposz " << final_pos_z << " pos.z " << pos.z << " vec.z " << vec.z << " sum " << sum_of_distance << " dx " << dx << endl;

		do{		//loop for k's

			switch(pos.lay_type){

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

			case 0:			//if the pos is in a film
				if(vec.z != 0)
					//calculates the total distance
					dx = fabs((final_pos_z - 1.0*pos.z)/vec.z);	//the lenght traveled is equal to the whole thickness
																//minus the starting point divided by cos(theta)
				else							//if vec.z == 0, both scattering points MUST be in the same layer
					dx = _path->vec_mod;		//in this case we can use the total distance vec_mod between scatterings (plural scattering)
				sum_of_distance += dx;

				pos.Cp = _prmt->layer[pos.l].Cp;

				if(_prmt->debug == 2) cout << "film: " << "dx " << dx << " vkf " << vec.z << " z " << pos.z << " Cp " << pos.Cp << endl;

				if(pos.Cp != 0){			//if the composition is != then 0 calculate energy loss and straggling
					_path->delta_E		+= _prmt->comp[pos.Cp].dedx[energy_loss_index] * dx;
					_path->delta_W2		+= _prmt->comp[pos.Cp].dwdx[energy_loss_index] * dx;
				}

				//goes to the end of the film
				if(pos.Cp != 0)	_path->displacement += dx;
				pos.z += vec.z * dx;

				//save the last Cp, used to know the last compound passed before leaving the sample
				if(pos.Cp != 0) _path->last_Cp = pos.Cp;

				break;

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

			case 1:			//if the pos is in a NP
				Cp_old = pos.Cp;

				if(pos.i < _matrix[pos.m].nx && pos.j < _matrix[pos.m].ny && pos.k < _matrix[pos.m].nz) //range check
					pos.Cp = _matrix[pos.m].Cp[pos.i][pos.j][pos.k];
				else
					pos.Cp = 0;

				//save the last Cp, used to know the last compound passed before leaving the sample
				if(pos.Cp != 0) _path->last_Cp = pos.Cp;

				if(_prmt->debug == 2) cout << "nano: " << "m " << pos.m << " i " << pos.i << " j " << pos.j << " k " << pos.k << " Cp "<< pos.Cp << " " << vec.z << endl;

				//correction to the last step:
				//if there is a change of composition during the last dr step, take the avarage energy loss between both steps
				if(pos.Cp != Cp_old){
					if(pos.Cp != 0){
						_path->delta_E	+= _prmt->comp[pos.Cp].dedx[energy_loss_index] * _prmt->simu.dr/2;
						_path->delta_W2	+= _prmt->comp[pos.Cp].dwdx[energy_loss_index] * _prmt->simu.dr/2;
					}
					if(Cp_old != 0){
						_path->delta_E	-= _prmt->comp[Cp_old].dedx[energy_loss_index] * _prmt->simu.dr/2;
						_path->delta_W2	-= _prmt->comp[Cp_old].dwdx[energy_loss_index] * _prmt->simu.dr/2;
					}
				}
				//energy loss for this step
				if(pos.Cp != 0){			//if the composition is != then 0 calculate energy loss and straggling
					_path->delta_E		+= _prmt->comp[pos.Cp].dedx[energy_loss_index] * _prmt->simu.dr;
					_path->delta_W2		+= _prmt->comp[pos.Cp].dwdx[energy_loss_index] * _prmt->simu.dr;
				}

				//integration step
				if(pos.Cp != 0)	_path->displacement += _prmt->simu.dr;
				pos.x += vec.x * _prmt->simu.dr;
				pos.y += vec.y * _prmt->simu.dr;
				pos.z += vec.z * _prmt->simu.dr;

				pos.double_to_index(_prmt);			//generates the int counterparts i,j,k of the x,y,z doubles

				if(pos.i >= _matrix[pos.m].nx || pos.i < 0		//if the ion goes out of the matrix boundaries
					|| pos.j >= _matrix[pos.m].ny || pos.j < 0)
						which_point_in_neightbour_matrix(_prmt, _matrix, _randgen, &pos);	//get the point inside a neightbour matrix

				break;
			}

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //
		}while(pos.z < final_pos_z && _path->delta_E < _prmt->beam.E0);
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

		if(vec.z > 0){
			//goes to the first step (k = 0) of the next layer (l += 1)
			pos.l 	+= 1;
			pos.z 	= 0;
		}
		else{
			//goes to the first step (k = layer.thick) of the next layer (l -= 1)
			pos.l 	-= 1;
			pos.z 	= _prmt->layer[pos.l].thick;
		}

		pos.lay_type	= _prmt->layer[pos.l].lay_type;
		pos.phi = 0;

		if(_prmt->debug == 2) cout << "next layer: " "l " << pos.l << " lf " <<  _path->fpos.l << endl;

		if(pos.l <= _prmt->nLa && 		//if the layer is lower then the total
			pos.lay_type == 1)			//AND the layer is a NP
				choose_point_in_matrix(_prmt, _tables, _matrix, _randgen, &pos, vec, sum_of_distance, last_layer_phi);	//choose a point inside the matrix

	}while(abs(_path->ipos.l - pos.l) <= abs(_path->ipos.l - _path->fpos.l) && _path->delta_E < _prmt->beam.E0);		//while it is not out of the sample

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

	if(_prmt->debug == 2) cout << "path ended" << endl;

}

void PATH_STRAIGHT_DISTANCE_HISTORY(const STprmt *_prmt, const STmatrix *_matrix, const STtables *_tables, STrand *_randgen, double initial_E, STpath *_path){

	int			nSteps=0;
	double 		dx, final_pos_z, last_layer_phi=0, sum_of_distance=0;

	STvector	vec;			//vector modified along the integration
	vec = _path->vec;

	STposition 	pos;			//position modified along the integration
	pos = _path->ipos;

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

	do{		//loop for layers

		//randomize the phi if rand_phi option is used
		if(_matrix[pos.m].rand_phi == 1)
			pos.phi = _randgen->randF() * 2 * PI;

		//Azimuthal angle - Rotates the vector based in the matrix rotation
		//rotate only the difference between the current vector phi and the last phi
		rotmat_2D(_tables, pos.phi - last_layer_phi, &vec, &vec);
		//store this layer phi rotation, to correctly change the rotation in the beginning of the next layer
		last_layer_phi = pos.phi;

		//this section calculates the thickness of the next segment
		if(pos.l == _path->fpos.l)	//if in the last layer
			final_pos_z = _path->fpos.z;				//the final k is equal to the FinalPos k
		else				//if not, the final k is:
			if(vec.z > 0)
				final_pos_z = _prmt->layer[pos.l].thick;			//if in film, the final k is equal to the thickness
			else
				final_pos_z = 0;

		if(_prmt->debug == 2) cout << "ipos.x " << _path->ipos.x << " ipos.y " << _path->ipos.y << " ipos.z " << _path->ipos.z << " ipos.l " << _path->ipos.l << endl;
		if(_prmt->debug == 2) cout << "fpos.x " << _path->fpos.x << " fpos.y " << _path->fpos.y << " fpos.z " << _path->fpos.z << " fpos.l " << _path->fpos.l << endl;
		if(_prmt->debug == 2) cout << "fposz " << final_pos_z << " pos.z " << pos.z << " vec.z " << vec.z << " sum " << sum_of_distance << " dx " << dx << endl;

		do{		//loop for k's

			switch(pos.lay_type){
				case 0:			//if the pos is in a film
					pos.Cp = _prmt->layer[pos.l].Cp;
					break;
				case 1:			//if the pos is in a NP
					if(pos.i >= _matrix[pos.m].nx || pos.i < 0		//if the ion goes out of the matrix boundaries
						|| pos.j >= _matrix[pos.m].ny || pos.j < 0)
							which_point_in_neightbour_matrix(_prmt, _matrix, _randgen, &pos);	//get the point inside a neightbour matrix

					if(pos.i < _matrix[pos.m].nx && pos.j < _matrix[pos.m].ny && pos.k < _matrix[pos.m].nz) //range check
						pos.Cp = _matrix[pos.m].Cp[pos.i][pos.j][pos.k];
					else
						pos.Cp = 0;
					break;
				}
			//save the last Cp, used to know the last compound passed before leaving the sample
			if(pos.Cp != 0) _path->last_Cp = pos.Cp;

			//stores the step compound index in the history array
			if(nSteps < _path->history_size){
				_path->history[nSteps] = pos.Cp;
				nSteps += 1;
			}

			if(_prmt->debug == 2) cout << "nano: " << "m " << pos.m << " i " << pos.i << " j " << pos.j << " k " << pos.k << " Cp "<< pos.Cp << " " << vec.z << endl;

			//integration step
			if(pos.Cp != 0)	_path->displacement += _prmt->simu.dr;
			pos.x += vec.x * _prmt->simu.dr;
			pos.y += vec.y * _prmt->simu.dr;
			pos.z += vec.z * _prmt->simu.dr;

			pos.double_to_index(_prmt);			//generates the int counterparts i,j,k of the x,y,z doubles

		}while(pos.z < final_pos_z);
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

		//goes to the first step (k = 0) of the next layer (l += 1)
		pos.z 	-= final_pos_z;
		pos.l 	+= 1;

		pos.lay_type	= _prmt->layer[pos.l].lay_type;
		pos.phi = 0;

		if(_prmt->debug == 2) cout << "next layer: " "l " << pos.l << " lf " <<  _path->fpos.l << endl;

		if(pos.l <= _prmt->nLa && 		//if the layer is lower then the total
			pos.lay_type == 1)			//AND the layer is a NP
				choose_point_in_matrix(_prmt, _tables, _matrix, _randgen, &pos, vec, sum_of_distance, last_layer_phi);	//choose a point inside the matrix

	}while(abs(_path->ipos.l - pos.l) <= abs(_path->ipos.l - _path->fpos.l));		//while it is not out of the sample

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

	if(_prmt->debug == 2) cout << "path ended" << endl;

	int iStep, Cp_old, iE=0;
	//if initial energy is the beam's energy, we are on the incoming path, which must be reversed
	if(_path->is_inward_path == true){
		Cp_old = 0;
		for(iStep=nSteps-1; iStep>=0; iStep--){
			//energy in this step
			iE = max(0, (int)( (initial_E-_path->delta_E)/_prmt->calc.stopp_dE ));

			//correction to the last step:
			//if there is a change of composition during the last dr step, take the avarage energy loss between both steps
			//cout << iStep << ' ' << _path->history[iStep] << ' ' << Cp_old << endl;
			if(_path->history[iStep] != Cp_old){
				if(_path->history[iStep] > 0){
					_path->delta_E	+= _tables->comp[_path->history[iStep]].dedx_E[0].array[iE] * _prmt->simu.dr/2;
					_path->delta_W2	+= _tables->comp[_path->history[iStep]].dwdx_E[0].array[iE] * _prmt->simu.dr/2;
				}
				if(Cp_old > 0){
					_path->delta_E	-= _tables->comp[Cp_old].dedx_E[0].array[iE] * _prmt->simu.dr/2;
					_path->delta_W2	-= _tables->comp[Cp_old].dwdx_E[0].array[iE] * _prmt->simu.dr/2;
				}
			}
			//energy loss for this step
			if(_path->history[iStep] > 0){			//if the composition is != then 0 calculate energy loss and straggling
				_path->delta_E		+= _tables->comp[_path->history[iStep]].dedx_E[0].array[iE] * _prmt->simu.dr;
				_path->delta_W2		+= _tables->comp[_path->history[iStep]].dwdx_E[0].array[iE] * _prmt->simu.dr;
			}
			Cp_old = _path->history[iStep];
		}
	}
	//if initial energy != from beam, we are on the outward path, which already is integrated on the correct direction
	else{
		Cp_old = _path->history[0];
		_path->delta_E		+= _tables->comp[Cp_old].dedx_E[_path->projectile.El_col].array[iE] * _prmt->simu.dr;
		_path->delta_W2		+= _tables->comp[Cp_old].dwdx_E[_path->projectile.El_col].array[iE] * _prmt->simu.dr;
		for(iStep=1; iStep<nSteps; iStep++){
			//energy in this step
			iE = max(0, (int)( (initial_E-_path->delta_E)/_prmt->calc.stopp_dE ));

			//correction to the last step:
			//if there is a change of composition during the last dr step, take the avarage energy loss between both steps
			if(_path->history[iStep] != Cp_old){
				if(_path->history[iStep] > 0){
					_path->delta_E	+= _tables->comp[_path->history[iStep]].dedx_E[_path->projectile.El_col].array[iE] * _prmt->simu.dr/2;
					_path->delta_W2	+= _tables->comp[_path->history[iStep]].dwdx_E[_path->projectile.El_col].array[iE] * _prmt->simu.dr/2;
				}
				if(Cp_old > 0){
					_path->delta_E	-= _tables->comp[Cp_old].dedx_E[_path->projectile.El_col].array[iE] * _prmt->simu.dr/2;
					_path->delta_W2	-= _tables->comp[Cp_old].dwdx_E[_path->projectile.El_col].array[iE] * _prmt->simu.dr/2;
				}
			}
			//energy loss for this step
			if(_path->history[iStep] > 0){			//if the composition is != then 0 calculate energy loss and straggling
				_path->delta_E		+= _tables->comp[_path->history[iStep]].dedx_E[_path->projectile.El_col].array[iE] * _prmt->simu.dr;
				_path->delta_W2		+= _tables->comp[_path->history[iStep]].dwdx_E[_path->projectile.El_col].array[iE] * _prmt->simu.dr;
			}
			Cp_old = _path->history[iStep];
		}
	}
}

#endif
