/*
 * Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "clib_dielectric.h"

dcomp g(dcomp z){
	dcomp zplus1, zminus1;
	long double outreal, outimag, dummy1, dummy2;
	zplus1 = z + dcomp(1.0, 0);
	zminus1 = z - dcomp(1.0, 0);

	dummy1 = log(abs(zplus1) / abs(zminus1));
	dummy2 = arg(zplus1) - arg(zminus1);

	outreal = z.real() + 0.5*(1.0 - (z.real()*z.real() - z.imag()*z.imag()))*dummy1 + z.real()*z.imag()*dummy2;
	outimag = z.imag() + 0.5*(1.0 - (z.real()*z.real() - z.imag()*z.imag()))*dummy2 - z.real()*z.imag()*dummy1;

	return (dcomp(outreal, outimag));
}

dcomp Lindhard(double q, double  omega, double  gamma, double  omega0){
	long double n_dens, E_f, v_f;
	long double z, chi, epsreal, epsimag;
	//complex dummy1,dummy2;
	dcomp d1, d2, z1, z2;

	n_dens = omega0*omega0 / (4.0*PI);
	E_f = 0.5*pow((3 * PI*PI*n_dens), (2.0 / 3.0));
	v_f = pow(2 * E_f, 0.5);   // v_f=k_f in atomic units


	z = q / (2 * v_f);
	chi = sqrt(1.0 / (PI*v_f));

	z1 = dcomp(omega / (q*v_f) + z, gamma / (q*v_f));
	d1 = g(z1);
	z2 = dcomp(omega / (q*v_f) - z, gamma / (q*v_f));
	d2 = g(z2);

	//printf("%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n", z1.real(), z1.imag(), z2.real(), z2.imag());

	epsreal = 1.0 + chi*chi / (4 * z*z*z)*(d1.real() - d2.real());
	epsimag = chi*chi / (4 * z*z*z)*(d1.imag() - d2.imag());
	return (dcomp(epsreal, epsimag));
}

dcomp Mermin(double q, double  omega, double gamma, double  omega0){
	dcomp z1, z2, z3, top, bottom, z2overz3;
	long double g_over_w;

	g_over_w = gamma / omega;
	z1 = dcomp(1.0, g_over_w);// omega should be unequal 0
	z2 = Lindhard(q, omega, gamma, omega0) - dcomp(1.0, 0.0);
	z3 = Lindhard(q, 0.0, 0.0, omega0) - dcomp(1.0, 0.0);
	z2overz3 = z2 / z3;

	top = z1*z2;
	bottom = dcomp(1, 0) + dcomp(0, g_over_w)*z2 / z3;
	z1 = dcomp(1.0, 0) + top / bottom;

	return (z1);
}

double dielectric_launcher(int mode, double q, double omega, double gamma, double omega0){
	dcomp eps;
	long double eps_norm2;

	if(mode == 1)
		eps = Lindhard(q, omega, gamma, omega0);
	else if(mode == 2)
		eps = Mermin(q, omega, gamma, omega0);
	else
		eps = dcomp(1,0);

	eps_norm2 = eps.real()*eps.real()+eps.imag()*eps.imag();
	return eps.imag()/eps_norm2;
	//return eps.imag();
}

double gos_hyd_function(double q, double omega, double Ib){
	bool include_bound_states = true;
	double Ql, kH, bl, kH2, Zs;
	double gos_hyd;

	if(omega > Ib){
		Zs = sqrt(Ib/0.5);
		Ql = (q/Zs)*(q/Zs);
		kH = sqrt(omega/(Ib) -1);
		kH2 = (kH)*(kH);
		bl = atan(2*kH/(Ql-kH2+1));
		if(bl < 0)
			bl = bl + PI;
		gos_hyd = 128*omega*(Ql+kH2/3+1/3)*exp(-2*bl/kH)/((Ib*Ib)*pow( (Ql-kH2+1)*(Ql-kH2+1) + 4*kH2,3)*(1-exp(-2*PI/kH)));
	}
	else{
		if(include_bound_states == true and omega > 0.75*Ib){
			//n = sqrt(1/(1-omega/Ib));
			Zs = sqrt(Ib/0.5);
			Ql = (q/Zs)*(q/Zs);
			kH2 = (omega/(Ib) -1);
			bl = -1/sqrt(-kH2)*log((Ql+1-kH2+2*sqrt(-kH2))/(Ql+1-kH2-2*sqrt(-kH2)));
			gos_hyd = 2*128*omega*(Ql+kH2/3+1/3)*exp(bl)/((Ib*Ib)*pow( (Ql-kH2+1)*(Ql-kH2+1) + 4*kH2,3));
		}
		else{
			gos_hyd = 0;
		}
	}
	return gos_hyd;
}

double  GOSx(int mode, int n, int  l, int Z, double  Ib, double q, double  w){
	double Zs, ne, Wl, Ql, Ql2, Ql3, A, bl, dummy, kH, kH2;
	double c[10]; double tmp;
	int j, jmax;
	Zs = 0.0;

	// Ib > 0, uses Ib as binding energy
	if (mode == 0)
		Zs = n*sqrt(Ib / 0.5);
	if (mode == 1){
		if (n == 1)  Zs = 1.0*Z - 0.3;
		if (n == 2)  Zs = 1.0*Z - 4.15;
		if (n == 3){
			if ((l == 0) || (l == 1))  Zs = 1.0*Z - 11.25;
			if (l == 2) Zs = 1.0*Z - 21.15;
		}
	}

	if (Zs == 0.0) return 0.0;

	if (
		w < Ib ||
		w < 0.5*pow(Zs,2) * (1/pow(n, 2) - 1/pow(n+1, 2))
		)
		return 0.0;

	ne = 2 * (2 * l + 1);

	Ql = (q / Zs)*(q / Zs);
	Wl = w / (0.5*Zs*Zs);
	kH2 = Wl - 1.0 / (n*n);

	if (kH2 > 0.0)
	{
		kH = sqrt(kH2);
		bl = atan(2.0 * kH / n / (Ql - Wl + 2.0 / (n*n)));
		if (bl < 0)  bl = bl + PI;
		A = 16.0 * pow(2.0 / n, 3.0)*exp(-2.0 / kH*bl) / (1.0 - exp(-2.0 * PI / kH)) / pow((Ql - Wl)*(Ql - Wl) + 4.0 / (n*n)*Ql, 2.0 * n + 1.0);
	}
	else
	{
		kH = sqrt(-kH2);
		tmp = (Ql - Wl + 2.0 / (n*n) + 2.0 * kH / n) / (Ql - Wl + 2.0 / (n*n) - 2.0 * kH / n);
		bl = -1.0 / kH*log(tmp);
		A = 16.0 * pow(2.0 / n, 3.0)*exp(bl) / pow((Ql - Wl)*(Ql - Wl) + 4.0 / (n* n)*Ql, 2.0 * n + 1.0);
	}

	dummy = 0.0;
	jmax = -1;

	if (n == 1)
	{
		c[0] = (Ql + Wl / 3);
		jmax = 0;
	}
	if ((n == 2) && (l == 0))
	{
		Ql2 = Ql*Ql;
		c[0] = (19.0 / 60.0 + 8.0 / 15.0 * Ql)*Ql2;
		c[1] = 1.0 / 15.0 * (Ql + 1)*Ql;
		c[2] = 1.0 / 30.0 * (1.0 - 40.0 * Ql)*Ql;
		c[3] = 2.0 / 3.0 * Ql;
		c[4] = 1.0 / 4.0 + 4.0 / 3.0 * Ql;
		c[5] = 1.0 / 3.0;
		jmax = 5;
	}


	if ((n == 2) && (l == 1))
	{
		Ql2 = Ql*Ql;
		c[0] = (17.0 / 20.0 + 4.0 / 5.0 * Ql)*Ql2;
		c[1] = (1.0 / 10.0 + 34.0 / 15.0 * Ql)*Ql;
		c[2] = 1.0 / 30.0 * (49.0 + 120.0 * Ql)*Ql;
		c[3] = 1.0 / 6.0 + 2.0 * Ql;
		c[4] = 1.0 / 4.0;
		jmax = 4;
	}

	if ((n == 3) && (l == 0))
	{
		Ql2 = Ql*Ql;
		Ql3 = Ql2*Ql;
		c[0] = (528384.0 / 502211745.0 + 561152.0 / 55801305.0 * Ql + 68608.0 / 6200145.0 * Ql2)*Ql3;
		c[1] = (32768.0 / 167403915.0 + 2048.0 / 413343.0 * Ql - 7424.0 / 6200145.0 * Ql2)*Ql2;
		c[2] = (8192.0 / 6200145.0 - 63488.0 / 1240029.0 * Ql - 17408.0 / 98415.0 * Ql2)*Ql2;
		c[3] = (256768.0 / 6200145.0 + 256.0 / 729.0 * Ql)*Ql2;
		c[4] = (11008.0 / 885735.0 + 30976.0 / 98415.0 * Ql + 15744.0 / 10935.0 * Ql2)*Ql;
		c[5] = (2816.0 / 32805.0 - 10912.0 / 10935.0 * Ql)*Ql;
		c[6] = (128.0 / 19683.0 + 1024.0 / 10935.0 * Ql - 64.0 / 27.0 * Ql2);
		c[7] = (208.0 / 2187.0 + 80.0 / 81.0 * Ql);
		c[8] = (32.0 / 81.0 + 4.0 / 3.0 * Ql);
		c[9] = 1.0 / 3.0;
		jmax = 9;
	}


	if ((n == 3) && (l == 1))
	{
		Ql2 = Ql*Ql;
		Ql3 = Ql2*Ql;
		c[0] = (495616.0 / 167403915.0 + 443392.0 / 18600435.0 * Ql + 8192.0 / 413343.0 * Ql2)*Ql3;
		c[1] = (65536.0 / 167403915.0 + 546304.0 / 18600435.0 * Ql + 38912.0 / 413343.0 * Ql2)*Ql2;
		c[2] = (274944.0 / 18600435.0 + 135424.0 / 2066715.0 * Ql + 2048.0 / 6561.0 * Ql2)*Ql2;
		c[3] = (4096.0 / 2657205.0 + 15872.0 / 137781.0 * Ql - 22528.0 / 32805.0 * Ql2)*Ql;
		c[4] = (512.0 / 32805.0 + 2368.0 / 10935.0 * Ql - 512.0 / 243.0 * Ql2)*Ql;
		c[5] = (8992.0 / 32805.0 + 1664.0 / 729.0 * Ql)*Ql;
		c[6] = (224.0 / 6561.0 + 560.0 / 243.0 * Ql + 128.0 / 27.0 * Ql2);
		c[7] = (208.0 / 729.0 + 64.0 / 27.0 * Ql);
		c[8] = 8.0 / 27.0;
		jmax = 8;
	}


	if ((n == 3) && (l == 2))
	{
		Ql2 = Ql*Ql;
		Ql3 = Ql2*Ql;
		c[0] = (253952.0 / 55801305.0 + 904192.0 / 55801305.0 * Ql + 131072.0 / 6200145.0 * Ql2)*Ql3;
		c[1] = (45056.0 / 167403915.0 + 1440256.0 / 18600435.0 * Ql + 149504.0 / 6200145.0 * Ql2)*Ql2;
		c[2] = (140800.0 / 3720087.0 + 4211968.0 / 6200145.0 * Ql + 32768.0 / 98415.0 * Ql2)*Ql2;
		c[3] = (2048.0 / 885735.0 + 2657792.0 / 6200145.0 * Ql + 48128.0 / 32805.0 * Ql2)*Ql;
		c[4] = (75008.0 / 885735.0 + 193472.0 / 98415.0 * Ql + 8192.0 / 3645.0 * Ql2)*Ql;
		c[5] = (256.0 / 59049.0 + 6304.0 / 10935.0 * Ql + 22912.0 / 10935.0 * Ql2);
		c[6] = (736.0 / 19683.0 + 6416.0 / 10935.0 * Ql);
		c[7] = 80.0 / 2187.0;
		jmax = 7;
	}

	for (j = 0; j < (jmax+1); j++)
	{
		dummy = dummy + c[j] * pow(Wl - Ql, j);
	}

	//returns GOS PER ELECTRON
	return A*dummy * 2.0 * w / (0.5*(Zs*Zs)*(0.5*(Zs*Zs))) / ne;
}

int dielectric_inside_C(const STprmt *_prmt, STtables *_tables, int nw, int nq){
	int iE, iw, iq, iq_ini, iq_fin, iCp, iPl, iEl;
	double elf, q, q0_1, E=0, w, v=0, M_beam, P_Ew, P_Ew0_1, rel_cor_factor;

	int iBS;
	double elfgos;

	double ffq=1, ffw=1;

	M_beam = _prmt->beam.M/0.000548579909;

	double dedx=0, dw2dx=0, dPdx=0;

	for(iCp=1; iCp<=_prmt->nCp; iCp++){
		for(iw=1; iw<nw; iw++){
			w = iw*_prmt->calc.dielec_dw;
			for(iq=1; iq<nq; iq++){
				q = iq*iq*_prmt->calc.dielec_dq;
				//ELF part -- add all plasmons
				elf = 0;
				for(iPl=1; iPl<=_prmt->nPl; iPl++) if(_prmt->comp[iCp].plasmon[iPl].A > 0){
					elf += dielectric_launcher(_prmt->calc.dielec_mode, q, w,
									_prmt->comp[iCp].plasmon[iPl].gamma, _prmt->comp[iCp].plasmon[iPl].w0)
								* _prmt->comp[iCp].plasmon[iPl].A;
				}
				//GOS part -- add all bound states
				elfgos = 0;
				for(iEl=1; iEl<=_prmt->nEl; iEl++) if(_prmt->comp[iCp].frac_elem[iEl] > 0)
					for(iBS=1; iBS<=_prmt->nBS; iBS++){
						elfgos += GOSx(	_prmt->elem[iEl].boundstate[iBS].mode,
										_prmt->elem[iEl].boundstate[iBS].n,
										_prmt->elem[iEl].boundstate[iBS].l,
										_prmt->elem[iEl].Z,
										_prmt->elem[iEl].boundstate[iBS].Ib,
										q, w)
									* _prmt->elem[iEl].boundstate[iBS].Ne
									* _prmt->comp[iCp].frac_elem[iEl]*(_prmt->comp[iCp].dens*pow(0.529, 3))
									* 2*PI*PI/w;
					}
				_tables->comp[iCp].P_wq[iw][iq] += 16*PI*PI * _prmt->beam.Z*_prmt->beam.Z /(q*q)
							* (elf + elfgos);
			}
		}

		for(iE=0; iE<_tables->comp[iCp].P_E.size; iE++){
			E = (iE*_tables->comp[iCp].P_E.delta + _tables->comp[iCp].P_E.initial)/27.211;
			if(E <= 0) E = 1e-3;
			v = sqrt(2*E/M_beam);
			dedx = 0; dw2dx = 0; dPdx = 0;
			for(iw=1; iw<nw; iw++){
				w = iw*_prmt->calc.dielec_dw;

				if(M_beam >1.1)  //electrons should be  mass 11   //(MV)
				{
					rel_cor_factor= 1.0;  // (MV) of course there are relativistic corrections for ions too, but can be neglected for MEIS energies

					//here we avoid issues with sqrt() of negative number
					if(E-w > 0)
						q = sqrt(2*E*M_beam) - sqrt(2*(E-w)*M_beam);
					else
						q = sqrt(2*E*M_beam);
				}
				else  // electrons use relativistic corrections, at least for lower limit
				{
					rel_cor_factor = (1.0 + E / (C*C))*(1.0 + E / (C*C)) / (1.0 + E / (2.0*C*C));

					//here we avoid issues with sqrt() of negative number
					if(E-w > 0)
						q = sqrt(E*(2.0 + E /(C*C))) - sqrt((E - w)*(2.0 + (E - w) / (C*C)));   //  (MV) relativistic integration limit, see Shinotsuka SIA 47 871
					else
						q = sqrt(E*(2.0 + E /(C*C)));
				}                                                  //(MV)

				//iq_ini = int(q/_prmt->calc.dielec_dq);
				iq_ini = int(sqrt(q/_prmt->calc.dielec_dq));
				//q0_1 = 1.0*q/_prmt->calc.dielec_dq - 1.0*iq_ini;
				q0_1 = (q - iq_ini*iq_ini*_prmt->calc.dielec_dq)/(_prmt->calc.dielec_dq*((iq_ini+1)*(iq_ini+1)-iq_ini*iq_ini));

				P_Ew0_1 = (_tables->comp[iCp].P_wq[iw][iq_ini] * (1.0 - q0_1)
						+ _tables->comp[iCp].P_wq[iw][iq_ini+1] * q0_1)
						//* q * (2*PI) / v * _prmt->calc.dielec_dq*(1.0 - q0_1) /(2*PI)/(2*PI)/(2*PI);
						* q * (2*PI) / v * _prmt->calc.dielec_dq*((iq_ini+1)*(iq_ini+1)-iq_ini*iq_ini)*(1.0 - q0_1) /(2*PI)/(2*PI)/(2*PI);

				//printf("%d\t%d\t%lf\t%lf\t%lf\n", iw, iq_ini, q0_1, _tables->comp[iCp].P_wq[iw][iq_ini], _tables->comp[iCp].P_wq[iw][iq_ini+1]);
				//iq_fin = int((sqrt(2*E*M_beam) + sqrt(2*(E-w)*M_beam))/_prmt->calc.dielec_dq);
				iq_fin = int(sqrt((sqrt(2*E*M_beam) + sqrt(2*(E-w)*M_beam))/_prmt->calc.dielec_dq));

				P_Ew = 0;
				for(iq=iq_ini+1; iq<nq && iq<iq_fin; iq++){
					q = iq*iq*_prmt->calc.dielec_dq;
					P_Ew += _tables->comp[iCp].P_wq[iw][iq] * q * ((iq+1)*(iq+1)-iq*iq);
				}
				P_Ew *= (2*PI) / v * _prmt->calc.dielec_dq/(2*PI)/(2*PI)/(2*PI);
				P_Ew += P_Ew0_1;

				dedx += w * P_Ew * rel_cor_factor * _prmt->calc.dielec_dw/(2*PI);   //(MV)
				dPdx += P_Ew * rel_cor_factor * _prmt->calc.dielec_dw/(2*PI);       //(MV)
				dw2dx += w*w * P_Ew *rel_cor_factor * _prmt->calc.dielec_dw/(2*PI); //(MV)
				_tables->comp[iCp].P_E.array[iE] += P_Ew * _prmt->calc.dielec_dw/(2*PI);
				_tables->comp[iCp].P_E_accu[iE][iw] = _tables->comp[iCp].P_E.array[iE];
			}
		//printf("%lf\t%lf\n", E * 27.211, _tables->comp[iCp].P_E.array[iE]);
		}
		printf("iCp:%d\tE(eV):%.2lf\tdE/dx(eV/A):%.5lf\tdw2/dx(eV2/A):%.3lf\tflp(A):%.1lf\n", iCp, E * 27.211, dedx / v * 27.211 / 0.529, dw2dx / v * 27.211*27.211 / 0.529, 1/(dPdx / v / 0.529));
	}

	return 0;
}
