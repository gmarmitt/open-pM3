/*
 * Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef	FUNC
#define	FUNC

#include "func.h"

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - -  FUNCTIONS  - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

void theta_lab_to_cm(const STtables *_tables, double ro, double theta_lab, double *theta_cm){
	double gamma, beta_sqr;
	gamma = ro*ro - 1;
	beta_sqr = tan(theta_lab)*tan(theta_lab);

	if(ro <= 1){
		theta_cm[0] = theta_lab +  asin(sin(theta_lab)*ro);
	}
	else{
		if(beta_sqr <= 1/gamma){
			theta_cm[0] = theta_lab +  asin(sin(theta_lab)*ro);
			theta_cm[1] = theta_lab + PI -  asin(sin(theta_lab)*ro);
		}
	}

	if(theta_cm[0] > PI) theta_cm[0] = 2*PI - theta_cm[0];
	if(theta_cm[1] > PI) theta_cm[1] = 2*PI - theta_cm[1];
}

double theta_cm_to_lab(const STtables *_tables, double m1, double m2, double theta_cm){
	double ro, theta_lab;
	ro = m1/m2;

	theta_lab = atan(sin(theta_cm)/(cos(theta_cm)+ro));

	//to avoid problems with the sign of atan()
	if(theta_lab < 0)
		theta_lab += PI;

	return theta_lab;
}

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

//simple 2D rotation by 'theta' radians
void rotmat_2D(const STtables *_tables, double theta, STvector *r_in, STvector *r_out){
	if(theta != 0){
		STvector r_sup;

		r_sup.x = cos(theta)*r_in->x - sin(theta)*r_in->y;
		r_sup.y = sin(theta)*r_in->x + cos(theta)*r_in->y;
		r_sup.z = r_in->z;

		*r_out = r_sup;
	}
	else
		*r_out = *r_in;
}

//3D rotation where the vector '_s' goes in the direction of 'u'
void rotmat_3D(STvector *_s, STvector u){
	STvector s_sup;
	double cos_theta, sin_theta, cos_phi, sin_phi, var;

	cos_theta = u.z;
	sin_theta = sqrt(1 - cos_theta*cos_theta);

	if(sin_theta == 0){
		cos_phi = 1;
		sin_phi = 0;
	}
	else{
		sin_phi = u.x/sin_theta;
		cos_phi = u.y/sin_theta;
	}

	s_sup = *_s;

	var = s_sup.y*cos_theta + s_sup.z*sin_theta;

	_s->x = var*sin_phi + s_sup.x*cos_phi;
	_s->y = var*cos_phi - s_sup.x*sin_phi;
	_s->z = -s_sup.y*sin_theta + s_sup.z*cos_theta;

}

//angle between vectors 'a' and 'b'
double angle_between_vec(const STtables *_tables, STvector a, STvector b){
	double cos;

	cos = a.x*b.x + a.y*b.y + a.z*b.z;

	//sometimes 'angle' may be a tiny bit (~1e-8) higher than +1, or lower then -1, causing a segfault error
	if(cos > +1) cos = +1;
	if(cos < -1) cos = -1;

//	return acos(cos);
	return acos(cos);
}

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

//get a random element to scatter
void choose_element_to_scatter(const STprmt *_prmt, STrand *_randgen, STposition *_pos){
	int iEl;
	double elem_rand;
	elem_rand = _randgen->randF();
	for(iEl=1; iEl<=_prmt->nEl; iEl++){
		if(elem_rand <= _prmt->comp[_pos->Cp].frac_elem[iEl])break;
		else elem_rand -= _prmt->comp[_pos->Cp].frac_elem[iEl];
	}
	_pos->El = iEl;
}

//get a random element to scatter weighted by the total scattering cross section
void choose_element_to_scatter_with_CS_weight(const STprmt *_prmt, const STtables *_tables, STrand *_randgen, double E, STposition *_pos){
	int iEl;
	double elem_rand, CS_total_sum=0;

	for(iEl=1; iEl<=_prmt->nEl; iEl++)
		CS_total_sum += _prmt->comp[_pos->Cp].frac_elem[iEl] * _tables->elem[iEl].CS_angle_cut.get(E);

	elem_rand = _randgen->randF() * CS_total_sum;
	for(iEl=1; iEl<=_prmt->nEl; iEl++){
		if(elem_rand < _prmt->comp[_pos->Cp].frac_elem[iEl] * _tables->elem[iEl].CS_angle_cut.get(E))break;
		else elem_rand -= _prmt->comp[_pos->Cp].frac_elem[iEl] * _tables->elem[iEl].CS_angle_cut.get(E);
	}
	_pos->El = iEl;
}

//get a random matrix in the layer
void choose_matrix_in_layer(const STprmt *_prmt, STrand *_randgen, STposition *_pos){
	int iMt;
	double mtx_rand;
	mtx_rand = _randgen->randF();
	for(iMt=1; iMt<=_prmt->nMt; iMt++){
		if(mtx_rand < _prmt->layer[_pos->l].Mt_weight[iMt])break;
		else mtx_rand -= _prmt->layer[_pos->l].Mt_weight[iMt];
	}
	_pos->m = iMt;
}

//chooses a point inside a given NP layer
void choose_point_in_matrix(const STprmt *_prmt, const STtables *_tables, const STmatrix *_matrix, STrand *_randgen, STposition *_pos, STvector vec, double sum_of_distance, double last_layer_phi){
	STvector rot_vec;

	choose_matrix_in_layer(_prmt, _randgen, _pos);		//chooses one matrix inside the layer

	//current phi rotation
	_pos->phi = _matrix[_pos->m].phi;

	switch(_prmt->layer[_pos->l].period_posi){
	case 0:								//if there is no periodic positioning
		_pos->x = _randgen->randF()*_matrix[_pos->m].nx*_prmt->layer[_pos->l].dL[0];		//randomize in x
		_pos->y = _randgen->randF()*_matrix[_pos->m].ny*_prmt->layer[_pos->l].dL[1];		//randomize in y

		_pos->double_to_index(_prmt);		//generates the index counterparts i,j,k of the x,y,z double

		//_pos->i = int(_randgen->randF() * _matrix[_pos->m].nx);		//randomize in x
		//_pos->j = int(_randgen->randF() * _matrix[_pos->m].ny);		//randomize in y
		break;

	case 1:								//if there is periodic positioning
		//create the vector from the scattering position to the current position
		vec.x *= sum_of_distance; vec.y *= sum_of_distance;	vec.z *= sum_of_distance;

		//rotate the position by the difference between the phi angle in this layer and the
		//last layer phi rotation
		if(_pos->lay_type == 1)rotmat_2D(_tables, _pos->phi - last_layer_phi, &vec, &rot_vec);
		//cout << vec.x << ' ' << rot_vec.x << ' ' << vec.y << ' ' << rot_vec.y << ' ' << vec.z << ' ' << rot_vec.z << endl;

		//then, displace the current position by the rotation
		//_pos->i += floor((rot_vec.x - vec.x)/_prmt->layer[_pos->l].dL[0]);
		//_pos->j += floor((rot_vec.y - vec.y)/_prmt->layer[_pos->l].dL[1]);
		_pos->x += rot_vec.x - vec.x;
		_pos->y += rot_vec.y - vec.y;

		_pos->double_to_index(_prmt);		//generates the index counterparts i,j,k of the x,y,z double

		//transpose the point if outside of the matrix
		while(_pos->i < 0) 						_pos->i += _matrix[_pos->m].nx;
		while(_pos->i > _matrix[_pos->m].nx-1)	_pos->i -= _matrix[_pos->m].nx;

		while(_pos->j < 0) 						_pos->j += _matrix[_pos->m].ny;
		while(_pos->j > _matrix[_pos->m].ny-1)	_pos->j -= _matrix[_pos->m].ny;

		_pos->index_to_double(_prmt);		//generates the float counterparts x,y,z of the i,j,k index
		break;
	}
}

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

//in which layer a given 'z' is located
int which_layer(const STprmt *_prmt, double z){
	int iLa;
	double z_tot=0;

	for(iLa=1; iLa<=_prmt->nLa; iLa++){
		z_tot += _prmt->layer[iLa].thick;
		if(double(z_tot) >= double(z)) return iLa;
		//there is a bug if one uses: if(z_tot >= z) instead. It is related to a rounding number problem in
		//some very high decimal case, because both z and z_tot are doubles. Cutting at double length fix it.
	}
	return 1;
}

//choses a point inside a neightbour matrix
void which_point_in_neightbour_matrix(const STprmt *_prmt, const STmatrix *_matrix, STrand *_randgen, STposition *_pos){
	switch(_prmt->layer[_pos->l].period_cont){
	case 0:			//if the layer does not have periodic contourn there is NOT another matrix in the side
					//then choose i=j=0 and use the Cp referent to the layer NOT the matrix.Cp
		_pos->i = 0;
		_pos->j = 0;
		_pos->lay_type = 0;
		break;

	case 1:			//the layer has periodic contourn
		choose_matrix_in_layer(_prmt, _randgen, _pos);		//chooses one matrix inside the layer


		if(_prmt->layer[_pos->l].period_posi == 0){			//if there is no periodic positioning
			if(_pos->i > _matrix[_pos->m].nx-1 || _pos->i < 0)
				//_pos->j = int(_randgen->randF() * _matrix[_pos->m].ny);		//randomize in y
				_pos->y = _randgen->randF()*_matrix[_pos->m].ny*_prmt->layer[_pos->l].dL[1];		//randomize in y

			if(_pos->j > _matrix[_pos->m].ny-1 || _pos->j < 0)
				//_pos->i = int(_randgen->randF() * _matrix[_pos->m].nx);		//randomize in x
				_pos->x = _randgen->randF()*_matrix[_pos->m].nx*_prmt->layer[_pos->l].dL[0];		//randomize in x
		}

		//transpose the point if outside of the matrix
		while(_pos->x < 0) 													_pos->x += _matrix[_pos->m].nx*_prmt->layer[_pos->l].dL[0];
		while(_pos->x > _matrix[_pos->m].nx*_prmt->layer[_pos->l].dL[0])	_pos->x -= _matrix[_pos->m].nx*_prmt->layer[_pos->l].dL[0];

		while(_pos->y < 0) 													_pos->y += _matrix[_pos->m].ny*_prmt->layer[_pos->l].dL[1];
		while(_pos->y > _matrix[_pos->m].ny*_prmt->layer[_pos->l].dL[1])	_pos->y -= _matrix[_pos->m].ny*_prmt->layer[_pos->l].dL[1];

		_pos->double_to_index(_prmt);		//generates the index counterparts i,j,k of the x,y,z double

		break;
	}

	//OBS: do NOT use "_pos->index_to_double()" here, because "z = k * dL" will destroy further steps in certain situations
	//_pos->x = _pos->i * _prmt->layer[_pos->l].dL[0];
	//_pos->y = _pos->j * _prmt->layer[_pos->l].dL[1];
}

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

//select uniformaly distributed random points inside a sphere os radius r
void rand_sphere(STrand *_randgen, double r, double *x, double *y, double *z){
	double mod2;

	do{
		*x = 2*_randgen->randF() - 1;
		*y = 2*_randgen->randF() - 1;
		*z = 2*_randgen->randF() - 1;
		mod2 = (*x)*(*x) + (*y)*(*y) + (*z)*(*z);
	}while(mod2 > 1);

	*x *= r;
	*y *= r;
	*z *= r;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - -  GET VECTOR - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

//gets a vector with direction towards the beam, as defined in prmt
void GET_VECTOR_IN(const STprmt *_prmt, STvector *_vec){
	//vector components
	*_vec = _prmt->beam.vec;
	//mod2 for vec_IN is 1 to avoid problems in PS histogram function
	//_path_IN->vec_mod = 1;

	if(_prmt->debug == 2) cout << "vec: x " << _vec->x << " y " << _vec->y << " z " << _vec->z << endl;
}

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

//gets a vector with direction towards the detector, as defined in prmt
void GET_VECTOR_OUT(const STprmt *_prmt, STrand *_randgen, STvector *_vec){
	STvector s;
	double thetax, thetay;

	if(_prmt->dete.r == 0.0){			//if there is no radius in the detector, choose inside a square
		thetax = _prmt->dete.LX*(_randgen->randF() - 0.5);
		thetay = _prmt->dete.LY*(_randgen->randF() - 0.5);
	}
	else do{						//if there is a radius, choose inside a circle with radius r
		thetax = _prmt->dete.LX*(_randgen->randF() - 0.5);
		thetay = _prmt->dete.LY*(_randgen->randF() - 0.5);
	}while(thetax*thetax + thetay*thetay > _prmt->dete.r*_prmt->dete.r);

	s.x = sin(thetax);
	s.y = sin(thetay);
	s.z = sqrt(1 - (s.x * s.x + s.y * s.y));

	rotmat_3D(&s, _prmt->dete.vec);

	//vector components
	*_vec = s;

	if(_prmt->debug == 2)
	cout << "vec: x " << _vec->x << " y " << _vec->y << " z " << _vec->z << endl;
}

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

void SET_PROJECTILE_MEIS(const STprmt *_prmt, STpath *_path){
	_path->projectile.Z = _prmt->beam.Z;
	_path->projectile.M = _prmt->beam.M;
}

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

void SET_PROJECTILE_ERDA(const STprmt *_prmt, STscatt *_scatt, STpath *_path){
	_path->projectile.El_col = _scatt->pos.El;
	_path->projectile.Z = _prmt->elem[_scatt->pos.El].Z;
	_path->projectile.M = _prmt->elem[_scatt->pos.El].M;
}

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

void SET_NRP_VARS(const STprmt *_prmt, const STtables *_tables, STscatt *_scatt, STpath *_path_IN, STpath *_path_OUT, STrand *_randgen, STenergy *_beamEnergy){
	// beam energy
	_beamEnergy->E = _randgen->randF() * (_prmt->outp.Emax - _prmt->outp.Emin - _prmt->outp.dEnergy) + _prmt->outp.Emin;
	_beamEnergy->E_to_iE(_prmt, _randgen);

	// scattering angle
	_scatt->theta 	= angle_between_vec(_tables, _path_IN->vec, _path_OUT->vec);
	_scatt->theta 	= PI - _scatt->theta;
}

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

void SET_IPOS_FPOS(const STprmt *_prmt, STscatt *_scatt, STpath *_path){
	//initial path position
	_path->ipos = _scatt->pos;

	//final path position
	_path->fpos.l = _prmt->nLa;
	_path->fpos.z = _prmt->layer[_path->fpos.l].thick;
	_path->fpos.double_to_index(_prmt);
}

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

void PRINT_DISPLACEMENT(STpath *_path_IN, STpath *_path_OUT, STscatt *_scatt, std::ofstream& displacement_file){
	double total_displacement;
	total_displacement = _path_IN->displacement + _path_OUT->displacement;

	STposition *_pos;
	_pos = &_scatt->pos;

	displacement_file << _pos->x << ' ' << _pos->y << ' ' << _pos->z << ' ' << total_displacement << endl;
}

#endif
