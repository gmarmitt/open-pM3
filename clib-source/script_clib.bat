
if %PROCESSOR_ARCHITECTURE%==x86 (
	g++ -O3 -std=c++11 -c clib_misc.cpp -lm -static-libgcc -static-libstdc++ -static
	g++ -O3 -std=c++11 -c clib_simu.cpp -lm -static-libgcc -static-libstdc++ -static
	g++ -O3 -shared -o ../clib/pM3_clib-win32.dll clib_misc.o clib_simu.o -lm -static-libgcc -static-libstdc++ -static

	g++ -O3 -std=c++11 -fPIC -c clib_dielectric.cpp -lm -Wall -static-libgcc -static-libstdc++ -static
	g++ -O3 -shared -o ../clib/dielectric_clib-win32.dll clib_dielectric.o -lm -Wall -static-libgcc -static-libstdc++ -static
) else (
	g++ -O3 -std=c++11 -c clib_misc.cpp -lm -static-libgcc -static-libstdc++ -static
	g++ -O3 -std=c++11 -c clib_simu.cpp -lm -static-libgcc -static-libstdc++ -static
	g++ -O3 -shared -o ../clib/pM3_clib-win64.dll clib_misc.o clib_simu.o -lm -static-libgcc -static-libstdc++ -static

	g++ -O3 -std=c++11 -fPIC -c clib_dielectric.cpp -lm -Wall -static-libgcc -static-libstdc++ -static
	g++ -O3 -shared -o ../clib/dielectric_clib-win64.dll clib_dielectric.o -lm -Wall -static-libgcc -static-libstdc++ -static
)

del clib_misc.o
del clib_simu.o
del clib_dielectric.o
