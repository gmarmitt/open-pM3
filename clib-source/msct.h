/*
 * Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef	MSCT_H
#define	MSCT_H

#include "structs.h"

using namespace std;

//definitions
//const double PI = 4.0 * atan (1.0);

void GET_FIRST_POSITION_TRAJ(const STprmt*, STmatrix*, STrand*, STposition*);

void SCATT_INFO_TRAJ(const STprmt*, const STtables*, STrand*, STscatt*);

double HISTOGRAM_TRAJ(const STprmt*, const STmatrix*, const STtables*, STrand*, STpath*, STpath*, STscatt*, STenergy*, SThisto*);
double HISTOGRAM_TRAJ_DIRECT(int, const STprmt*, const STmatrix*, const STtables*, STrand*, SThisto*);
void TRAJECTORY_CREATOR(int, const STprmt*, const STtables*, STrand*, STposition*, STvector*, int, double, STmatrix*);
bool CONNECTOR(const STprmt*, STmatrix*, const STtables*, STrand*, STpath*, STpath*, STscatt*);

#endif
