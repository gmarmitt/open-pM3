/*
 * Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef	CLIB_MISC_H
#define	CLIB_MISC_H

#include "structs.h"

#include <iostream>
#include <cmath>
#include <cstdio>
#include <string.h>

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

const double PI = 4.0 * atan (1.0);

using namespace std;

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

extern "C"{
	void VARIABLES_TEST(const STprmt*, const STmatrix*);
	int GENERATE_PM3_MATRIX(STmatrix*);
	int GENERATE_TRI3DYN_MATRIX(STprmt*, STmatrix*);
}

#endif
