/*
 * Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "clib_simu.h"

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - -  DIRECT TRAJECTORY  - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

int TRAJ_DIRECT(int iThread, long int *progcount, const STprmt *_prmt, STmatrix *_matrix,
				const STtables *_tables, SThisto *_histoALL){

	STvector 	*_vec_IN	= new STvector;
	STposition 	*_ipos 		= new STposition;
	STenergy 	*_energy	= new STenergy;
	STrand 		*_randgen	= new STrand;

	//sets the seed for the random sequence used in this thread, then fill the storages
	_randgen->mt19937_sequence.seed(time(NULL) + iThread*10);
	_randgen->fill_randF_storage();
	_randgen->fill_randG_storage();

	//pointer to the histogram of this thread in particular
	SThisto		*_histo;
	_histo 		= &_histoALL[iThread];

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //
	for(progcount[iThread]=0; progcount[iThread]<_prmt->simu.nI_per_thread;){
		//clean previous variables
		_vec_IN->clean();
		_ipos->clean();

		GET_FIRST_POSITION_TRAJ(_prmt, _matrix, _randgen, _ipos);

		//get path vectors
		GET_VECTOR_IN(_prmt, _vec_IN);

		//inward trajectory
		TRAJECTORY_CREATOR(iThread, _prmt, _tables, _randgen, _ipos, _vec_IN, 1, _prmt->beam.E0, _matrix);

		//prints this event
		if(HISTOGRAM_TRAJ_DIRECT(iThread, _prmt, _matrix, _tables, _randgen, _histo) == 1){
			progcount[iThread]++;
		}
	}
// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

	delete _vec_IN;
	delete _ipos;
	delete _energy;
	delete _randgen;

	return 0;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - -  TRAJECTORY CONNECTIONS - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

int TRAJ_FILL(int iThread, long int *progcount, const STprmt *_prmt, STmatrix *_matrix,
				const STtables *_tables){

	double Ed;
	int i;
	STvector *_vec_IN	= new STvector;
	STvector *_vec_OUT	= new STvector;
	STposition *_ipos 	= new STposition;
	STrand 	*_randgen	= new STrand;

	//sets the seed for the random sequence used in this thread, then fill the storages
	_randgen->mt19937_sequence.seed(time(NULL) + iThread*10);
	_randgen->fill_randF_storage();
	_randgen->fill_randG_storage();

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //
	for(progcount[iThread]=0; progcount[iThread]<_prmt->straj.nI_per_thread; progcount[iThread]++){
		if(_prmt->debug == 2)
		cout << "thread " << iThread << " i " << progcount[iThread] << endl;

		//clean previous variables
		_vec_IN->clean();
		_ipos->clean();

		GET_FIRST_POSITION_TRAJ(_prmt, _matrix, _randgen, _ipos);

		//get path vectors
		GET_VECTOR_IN(_prmt, _vec_IN);

		//inward trajectory
		TRAJECTORY_CREATOR(iThread, _prmt, _tables, _randgen, _ipos, _vec_IN, 1, _prmt->beam.E0, _matrix);

		//outward trajectory
		for(i=1; i<100; i++){
			//clean previous variables
			_vec_OUT->clean();
			_ipos->clean();

			GET_FIRST_POSITION_TRAJ(_prmt, _matrix, _randgen, _ipos);
			GET_VECTOR_OUT(_prmt, _randgen, _vec_OUT);
			Ed = _randgen->randF()*(_prmt->outp.Emax - _prmt->outp.Emin)+_prmt->outp.Emin;
			TRAJECTORY_CREATOR(iThread, _prmt, _tables, _randgen, _ipos, _vec_OUT, 2, Ed, _matrix);
		}
	}
// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

	delete _vec_IN;
	delete _vec_OUT;
	delete _ipos;
	delete _randgen;

	return 0;
}

int TRAJ_CONNECT(int iThread, long int *progcount, const STprmt *_prmt, STmatrix *_matrix,
				const STtables *_tables, SThisto *_histoALL){
	bool valid_connection;
	int err;

	STrand 		*_randgen	= new STrand;
	STenergy 	*_energy	= new STenergy;
	STscatt 	*_scatt 	= new STscatt;
	STpath		*_path_IN	= new STpath;
	STpath		*_path_OUT	= new STpath;

	//tag the path as inward, if thats the case
	_path_IN->is_inward_path = true;

	//pointer to the histogram of this thread in particular
	SThisto		*_histo;
	_histo 		= &_histoALL[iThread];

	//sets the seed for the random sequence used in this thread, then fill the storages
	_randgen->mt19937_sequence.seed(time(NULL) + iThread*10);
	_randgen->fill_randF_storage();
	_randgen->fill_randG_storage();

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //
	for(progcount[iThread]=0; progcount[iThread]<_prmt->simu.nI; progcount[iThread]++){
		if(_prmt->debug == 2) cout << "thread " << iThread << " i " << progcount[iThread] << endl;
		//clean previous variables
		_path_IN->clean();
		_path_OUT->clean();
		_scatt->clean();
		_energy->clean();

		//get projectile properties
		SET_PROJECTILE_MEIS(_prmt, _path_IN);
		SET_PROJECTILE_MEIS(_prmt, _path_OUT);

		//if Ediff between in and out trajs is smaller than dE, there is a valid connection
		while(_scatt->pos.Cp == 0) err = GET_FIRST_POSITION(_prmt, _matrix, _randgen, _scatt);
		if( err != 0 ) return err;
		do {
			valid_connection = CONNECTOR(_prmt, _matrix, _tables, _randgen, _path_IN, _path_OUT, _scatt);
		}while(valid_connection == false);

		//inward path
		PATH_INFO(_prmt, _randgen, _path_IN, _scatt, _prmt->beam.E0, 0);

		//first collision
		SCATT_INFO_TRAJ(_prmt, _tables, _randgen, _scatt);

		//outward path
		PATH_INFO_LAST(_prmt, _path_OUT, _scatt, _energy);

		//evaluates the final energy of the event
		EVENT_ENERGY(_prmt, _tables, _randgen, _path_OUT, _scatt, _energy);

		//generates the histogram contribution for this event
		if(_energy->is_inside_spectrum) HISTOGRAM_TRAJ(_prmt, _matrix, _tables, _randgen, _path_IN, _path_OUT, _scatt, _energy, _histo);
	}

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

	delete _scatt;
	delete _path_IN;
	delete _path_OUT;
	delete _energy;
	delete _randgen;

	return 0;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - -  SINGLE SCATTERING SIMULATION - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

int SIMU_SSCT(int iThread, long int *progcount, const STprmt *_prmt, const STmatrix *_matrix,
				const STtables *_tables, SThisto *_histoALL){

	int err;
	STscatt 	*_scatt 	= new STscatt;
	STpath		*_path_IN	= new STpath;
	STpath		*_path_OUT	= new STpath;
	STenergy 	*_energy	= new STenergy;
	STrand 		*_randgen	= new STrand;

	//tag the path as inward, if thats the case
	_path_IN->is_inward_path = true;

	//maximum number of steps to cross the whole sample
	_path_IN->history_size = abs((int)(_prmt->thick/cos(_prmt->beam.T1)/_prmt->simu.dr)) + 1; 		//inward path
	_path_OUT->history_size = abs((int)(_prmt->thick/cos(PI-_prmt->beam.T1-_prmt->dete.theta_min)/_prmt->simu.dr)) + 1;	//outward path

	_path_IN->history	= new short[_path_IN->history_size];
	_path_OUT->history	= new short[_path_OUT->history_size];

	//pointer to the histogram of this thread in particular
	SThisto		*_histo;
	_histo 		= &_histoALL[iThread];

	//pointers to elements of the _path array, used to change information inside _path
	STvector 	*_vec_IN;
	STvector 	*_vec_OUT;
	_vec_IN 	= &_path_IN->vec;
	_vec_OUT 	= &_path_OUT->vec;

	//sets the seed for the random sequence used in this thread, then fill the storages
	_randgen->mt19937_sequence.seed(time(NULL) + iThread*10);
	_randgen->fill_randF_storage();
	_randgen->fill_randG_storage();

	//opens the output file for the total displacement
	ofstream displacement_file;
	if(_prmt->debug == 3){
		displacement_file.open("displacement.dat");
	}

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //
	for(progcount[iThread]=0; progcount[iThread]<_prmt->simu.nI_per_thread; progcount[iThread]++){
		if(_prmt->debug == 2) cout << "thread " << iThread << " i " << progcount[iThread] << endl;

		//clean previous variables
		_path_IN->clean();
		_path_OUT->clean();
		_scatt->clean();
		_energy->clean();

	// - - - - - - - GET SCATTERING POSITIONS  - - - - - - - - //
		err = GET_FIRST_POSITION(_prmt, _matrix, _randgen, _scatt);
		if( err != 0 ) return err;

		if(_scatt->pos.Cp != 0){
			//get path vectors
			GET_VECTOR_IN(_prmt, _vec_IN);
			GET_VECTOR_OUT(_prmt, _randgen, _vec_OUT);

			SET_IPOS_FPOS(_prmt, _scatt, _path_IN);
			SET_IPOS_FPOS(_prmt, _scatt, _path_OUT);

			//get projectile properties
			SET_PROJECTILE_MEIS(_prmt, _path_IN);
			SET_PROJECTILE_MEIS(_prmt, _path_OUT);

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - //

			//inward path
			if(_prmt->simu.surf_apprx == 1)PATH_STRAIGHT_DISTANCE(_prmt, _matrix, _tables, _randgen, 0, _path_IN);
			if(_prmt->simu.surf_apprx == 0)PATH_STRAIGHT_DISTANCE_HISTORY(_prmt, _matrix, _tables, _randgen, _prmt->beam.E0, _path_IN);
			PATH_INFO(_prmt, _randgen, _path_IN, _scatt, _prmt->beam.E0, 0);

			//first collision
			SCATT_INFO(_prmt, _tables, _randgen, _path_IN, _path_OUT, _scatt);

			//outward path
			if(_prmt->simu.surf_apprx == 1)PATH_STRAIGHT_DISTANCE(_prmt, _matrix, _tables, _randgen, _path_OUT->ipos.El, _path_OUT);
			if(_prmt->simu.surf_apprx == 0)PATH_STRAIGHT_DISTANCE_HISTORY(_prmt, _matrix, _tables, _randgen, _scatt->E_aft, _path_OUT);
			PATH_INFO_LAST(_prmt, _path_OUT, _scatt, _energy);

			//evaluates the final energy of the event
			EVENT_ENERGY(_prmt, _tables, _randgen, _path_OUT, _scatt, _energy);

			//generates the histogram contribution for this event
			if(_energy->is_inside_spectrum) HISTOGRAM_SSCT(_prmt, _matrix, _tables, _randgen, _scatt, _energy, _histo);

			//prints the total displacement inside the sample
			if(_prmt->debug == 3) PRINT_DISPLACEMENT(_path_IN, _path_OUT, _scatt, displacement_file);
		}
	}
// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

	delete _scatt;
//	delete[] _path_IN->history;
	delete _path_IN;
//	delete[] _path_OUT->history;
	delete _path_OUT;
	delete _energy;
	delete _randgen;

	//closes the output file for the total displacement
	if(_prmt->debug == 3) displacement_file.close();

	return 0;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - -  ERDA SCATTERING SIMULATION - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

int SIMU_ERDA(int iThread, long int *progcount, const STprmt *_prmt, const STmatrix *_matrix,
				const STtables *_tables, SThisto *_histoALL){

	int err;

	STscatt 	*_scatt 	= new STscatt;
	STpath		*_path_IN	= new STpath;
	STpath		*_path_OUT	= new STpath;
	STenergy 	*_energy	= new STenergy;
	STrand 		*_randgen	= new STrand;

	//tag the path as inward, if thats the case
	_path_IN->is_inward_path = true;

	//maximum number of steps to cross the whole sample
	_path_IN->history_size = abs((int)(_prmt->thick/cos(_prmt->beam.T1)/_prmt->simu.dr)) + 1; 		//inward path
	_path_OUT->history_size = abs((int)(_prmt->thick/cos(PI-_prmt->beam.T1-_prmt->dete.theta_min)/_prmt->simu.dr)) + 1;	//outward path

	_path_IN->history	= new short[_path_IN->history_size];
	_path_OUT->history	= new short[_path_OUT->history_size];

	//pointer to the histogram of this thread in particular
	SThisto		*_histo;
	_histo 		= &_histoALL[iThread];

	//pointers to elements of the _path array, used to change information inside _path
	STvector 	*_vec_IN;
	STvector 	*_vec_OUT;
	_vec_IN 	= &_path_IN->vec;
	_vec_OUT 	= &_path_OUT->vec;

	//sets the seed for the random sequence used in this thread
	_randgen->mt19937_sequence.seed(time(NULL) + iThread*10);
	_randgen->fill_randF_storage();
	_randgen->fill_randG_storage();

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

	for(progcount[iThread]=0; progcount[iThread]<_prmt->simu.nI_per_thread; progcount[iThread]++){
		if(_prmt->debug == 2) cout << "thread " << iThread << " i " << progcount[iThread] << endl;

		//clean previous variables
		_path_IN->clean();
		_path_OUT->clean();
		_scatt->clean();
		_energy->clean();

	// - - - - - - - GET SCATTERING POSITIONS  - - - - - - - - //
		err = GET_FIRST_POSITION(_prmt, _matrix, _randgen, _scatt);
		if( err != 0 ) return err;

		if(_scatt->pos.Cp != 0){
			//get path vectors
			GET_VECTOR_IN(_prmt, _vec_IN);
			GET_VECTOR_OUT(_prmt, _randgen, _vec_OUT);

			SET_IPOS_FPOS(_prmt, _scatt, _path_IN);
			SET_IPOS_FPOS(_prmt, _scatt, _path_OUT);

			//get projectile properties
			SET_PROJECTILE_MEIS(_prmt, _path_IN);
			SET_PROJECTILE_ERDA(_prmt, _scatt, _path_OUT);

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - //

			//inward path
			if(_prmt->simu.surf_apprx == 1)PATH_STRAIGHT_DISTANCE(_prmt, _matrix, _tables, _randgen, 0, _path_IN);
			if(_prmt->simu.surf_apprx == 0)PATH_STRAIGHT_DISTANCE_HISTORY(_prmt, _matrix, _tables, _randgen, _prmt->beam.E0, _path_IN);
			PATH_INFO(_prmt, _randgen, _path_IN, _scatt, _prmt->beam.E0, 0);

			//first collision
			SCATT_INFO(_prmt, _tables, _randgen, _path_IN, _path_OUT, _scatt);

			//outward path
			if(_prmt->simu.surf_apprx == 1)PATH_STRAIGHT_DISTANCE(_prmt, _matrix, _tables, _randgen, _path_OUT->ipos.El, _path_OUT);
			if(_prmt->simu.surf_apprx == 0)PATH_STRAIGHT_DISTANCE_HISTORY(_prmt, _matrix, _tables, _randgen, _scatt->E_aft, _path_OUT);
			PATH_INFO_LAST(_prmt, _path_OUT, _scatt, _energy);

			//evaluates the final energy of the event
			EVENT_ENERGY(_prmt, _tables, _randgen, _path_OUT, _scatt, _energy);

			//generates the histogram contribution for this event
			if(_energy->is_inside_spectrum) HISTOGRAM_SSCT(_prmt, _matrix, _tables, _randgen, _scatt, _energy, _histo);
		}

	}

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

	delete _scatt;
	delete _path_IN;
	delete _path_OUT;
	delete _energy;

	return 0;
}

/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - NRP SIMULATION  - - - - - - - - - - - - - - - - - - - -///
/// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -///

int SIMU_NRP(int iThread, long int *progcount, const STprmt *_prmt, const STmatrix *_matrix,
				const STtables *_tables, SThisto *_histoALL){

	int err;
	STscatt 	*_scatt 	= new STscatt;
	STpath		*_path_IN	= new STpath;
	STpath		*_path_OUT	= new STpath;
	STenergy	*_beamEnergy	= new STenergy;
	STrand 		*_randgen	= new STrand;

	//tag the path as inward, if thats the case
	_path_IN->is_inward_path = true;

	//maximum number of steps to cross the whole sample
	_path_IN->history_size = abs((int)(_prmt->thick/cos(_prmt->beam.T1)/_prmt->simu.dr)) + 1; 		//inward path
	_path_OUT->history_size = abs((int)(_prmt->thick/cos(PI-_prmt->beam.T1-_prmt->dete.theta_min)/_prmt->simu.dr)) + 1;	//outward path

	_path_IN->history	= new short[_path_IN->history_size];
	_path_OUT->history	= new short[_path_OUT->history_size];

	//pointer to the histogram of this thread in particular
	SThisto		*_histo;
	_histo 		= &_histoALL[iThread];

	//pointers to elements of the _path array, used to change information inside _path
	STvector 	*_vec_IN;
	STvector 	*_vec_OUT;
	_vec_IN 	= &_path_IN->vec;
	_vec_OUT 	= &_path_OUT->vec;

	//sets the seed for the random sequence used in this thread, then fill the storages
	_randgen->mt19937_sequence.seed(time(NULL) + iThread*10);
	_randgen->fill_randF_storage();
	_randgen->fill_randG_storage();

// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //
	for(progcount[iThread]=0; progcount[iThread]<_prmt->simu.nI_per_thread; progcount[iThread]++){
		if(_prmt->debug == 2) cout << "thread " << iThread << " i " << progcount[iThread] << endl;

		//clean previous variables
		_path_IN->clean();
		_path_OUT->clean();
		_scatt->clean();
		_beamEnergy->clean();

	// - - - - - - - GET SCATTERING POSITIONS  - - - - - - - - //
		err = GET_FIRST_POSITION(_prmt, _matrix, _randgen, _scatt);
		if( err != 0 ) return err;

		if(_scatt->pos.Cp != 0){
			//get path vectors
			GET_VECTOR_IN(_prmt, _vec_IN);
			GET_VECTOR_OUT(_prmt, _randgen, _vec_OUT);

			SET_IPOS_FPOS(_prmt, _scatt, _path_IN);

			//get projectile properties
			SET_PROJECTILE_MEIS(_prmt, _path_IN);

			//set energy and scattering angle
			SET_NRP_VARS(_prmt, _tables, _scatt, _path_IN, _path_OUT, _randgen, _beamEnergy);

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - //

			//inward path
			if(_prmt->simu.surf_apprx == 1)PATH_STRAIGHT_DISTANCE(_prmt, _matrix, _tables, _randgen, 0, _path_IN);
			if(_prmt->simu.surf_apprx == 0)PATH_STRAIGHT_DISTANCE_HISTORY(_prmt, _matrix, _tables, _randgen, _beamEnergy->E, _path_IN);
			PATH_INFO(_prmt, _randgen, _path_IN, _scatt, _beamEnergy->E, 0);

			//generates the histogram contribution for this event
			HISTOGRAM_NRP(_prmt, _matrix, _tables, _randgen, _scatt, _beamEnergy, _histo);

		}
	}
// -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -   -  //

	delete _scatt;
//	delete[] _path_IN->history;
	delete _path_IN;
//	delete[] _path_OUT->history;
	delete _path_OUT;
	delete _beamEnergy;
	delete _randgen;


	return 0;
}
