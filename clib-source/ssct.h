/*
 * Copyright 2018 Gabriel G. Marmitt (gabriel.marmitt7@gmail.com)

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef	SSCT_H
#define	SSCT_H

#include "structs.h"

using namespace std;

int GET_FIRST_POSITION(const STprmt*, const STmatrix*,  STrand*, STscatt*);

void PATH_INFO(const STprmt*, STrand*, STpath*, STscatt*, double, double);
void PATH_INFO_LAST(const STprmt*, STpath*, STscatt*, STenergy*);
void SCATT_INFO(const STprmt*, const STtables*, STrand*, STpath*, STpath*, STscatt*);

void EVENT_ENERGY(const STprmt*, const STtables*, STrand*, STpath*, STscatt*, STenergy*);

void HISTOGRAM_SSCT(const STprmt*, const STmatrix*, const STtables*, STrand*, STscatt*, STenergy*, SThisto*);
void HISTOGRAM_NRP(const STprmt*, const STmatrix*, const STtables*, STrand*, STscatt*, STenergy*, SThisto*);

void PATH_STRAIGHT_DISTANCE(const STprmt*, const STmatrix*, const STtables*, STrand*, int, STpath*);
void PATH_STRAIGHT_DISTANCE_HISTORY(const STprmt*, const STmatrix*, const STtables*, STrand*, double, STpath*);

#endif
