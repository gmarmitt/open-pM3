#!/bin/bash

clear

# get the pc architecture
MACHINE_TYPE=`uname -m`

key="$1"

if [ ${MACHINE_TYPE} == 'x86_64' ]; then
	# 64-bit compilation
	g++ -O3 -std=c++11 -fPIC -c clib_misc.cpp -lm -Wall -static-libgcc -static-libstdc++
	g++ -O3 -std=c++11 -fPIC -c clib_simu.cpp -lm -Wall -static-libgcc -static-libstdc++
	g++ -O3 -shared -o ../clib/pM3_clib-lin64.so clib_misc.o clib_simu.o -lm -Wall -static-libgcc -static-libstdc++

	g++ -O3 -std=c++11 -fPIC -c clib_dielectric.cpp -lm -Wall -static-libgcc -static-libstdc++
	g++ -O3 -shared -o ../clib/dielectric_clib-lin64.so clib_dielectric.o -lm -Wall -static-libgcc -static-libstdc++
else
	g++ -O3 -std=c++11 -fPIC -c clib_misc.cpp -lm -Wall -static-libgcc -static-libstdc++
	g++ -O3 -std=c++11 -fPIC -c clib_simu.cpp -lm -Wall -static-libgcc -static-libstdc++
	g++ -O3 -shared -o ../clib/pM3_clib-lin32.so clib_misc.o clib_simu.o -lm -Wall -static-libgcc -static-libstdc++

	g++ -O3 -std=c++11 -fPIC -c clib_dielectric.cpp -lm -Wall -static-libgcc -static-libstdc++
	g++ -O3 -shared -o ../clib/dielectric_clib-lin32.so clib_dielectric.o -lm -Wall -static-libgcc -static-libstdc++
fi

rm clib_misc.o
rm clib_simu.o
rm clib_dielectric.o
